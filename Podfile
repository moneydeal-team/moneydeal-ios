platform :ios, '11.0'
inhibit_all_warnings!

def rxSwift
    pod 'RxSwift'
    pod 'RxCocoa'
    pod 'RxGesture'
end

def swiftyUserDefaults
    pod 'SwiftyUserDefaults'
end

def keychainSwift
    pod 'KeychainSwift'
end

def rxKingfisher
    pod 'RxKingfisher', '0.3.1'
end

def punycode
    pod 'Punycode-Cocoa'
end

def firebase
    pod 'Firebase/Core'
    pod 'Firebase/Messaging'
    pod 'Firebase/Auth'
end

target 'Moneydeal' do
  use_frameworks!
  
  # Sugar
  rxSwift
  swiftyUserDefaults
  pod 'RxDataSources'
  pod 'RxKeyboard'
  pod 'RxViewModel'
  pod 'Kingfisher', '~> 4.0'
  rxKingfisher
  
  # UI
  pod 'FlexLayout'
  pod 'PinLayout'
  pod 'AppRouter'
  pod 'RxAppState'
  pod 'InputMask'

  # Utilities
  keychainSwift
  firebase
  pod 'SwiftLint'
end

target 'Domain' do
    use_frameworks!
    
    rxSwift
    pod 'RxRealm'
end

target 'Network' do
    use_frameworks!
    
    # Sugar
    rxSwift
    swiftyUserDefaults
    pod 'Moya/RxSwift'
    punycode
    rxKingfisher
end

target 'Storage' do
    use_frameworks!
    
    rxSwift
    pod 'RxRealm'
end

target 'Utils' do
    use_frameworks!
    
    rxSwift
    punycode
    pod 'RxRealm'
end

post_install do |installer|
    installer.pods_project.targets.each do |target|
        if target.name == 'KeychainSwift'
            target.build_configurations.each do |config|
                config.build_settings['SWIFT_VERSION'] = '4.2'
            end
        end
    end
end
