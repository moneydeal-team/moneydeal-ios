<p align="center">
  <img height="160" src="http://images.cdn.moneydeal.kkapp.ru/project/gitlogo.png" />
</p>

# Moneydeal
## Introduction
Moneydeal is an expense tracking application. Our mission is to simplify your expenses control. The Main feature is the control of group expenses; a person can add a purchase to the account of all group members.

We have many features, here are some of them:
- Income and expenses tracking
- Group purchase
- Add purchase with QR-Code
- One-step authorization
- Financial goal (e.g. a deposit)

## Stack
### iOS
**MVVM** - architecture well with reactive programming 🏆

**RxSwift** - we like reactive programming style ❤️

**FlexLayout** - a nice swift interface to create great views 🦋

**Moya** - network layer 🌐

**Realm** - fastest database 🚀

**Account kit** - one-step authorization via facebook 🌪

**SwiftLint** - we appreciate a clean code 💫


### Backend
**Go** - we don’t always need words to describe programming language by Google 💠

**Casandra** - high performance no-sql database ☄️


### Infrastructure
Our servers (cdn, api, e.t.c.) are hosted by MSC (mail.ru cloud solution)

## Deisgn
Moneydeal is a progressive application, we use world design standards. You can find the application design in [figma](https://www.figma.com/file/Zyd4cOEcUQ8kgNd6HwWFJ8/Kostya_ios_app "figma")

## Analytics
[http://images.cdn.moneydeal.kkapp.ru/analytics.pdf](http://images.cdn.moneydeal.kkapp.ru/analytics.pdf)

## Team
K. Kulakov - [vk](https://vk.com/kulakovkostya),  [git](https://github.com/KostyaKulakov/)

A. Zorin - [vk](https://vk.com/id60770850),  [git](https://github.com/ZorinArsenij)

N. Chuzhikov - [vk](https://vk.com/nikitachuzhikov),  [git](https://github.com/nikitachuzhikov)