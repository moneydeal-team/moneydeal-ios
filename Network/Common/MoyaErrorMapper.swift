//
//  MoyaErrorMapper.swift
//  Networking
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Moya
import Domain
import RxSwift
import RxKingfisher
import Utils

public enum MoyaErrorMapper {

    /// Преобразовывает пойманную ошибку в ошибку типа `Domain.Error`.
    public static func handleError<T>(error: Swift.Error) -> Single<T> {
        switch error {
        case is ServerStatusResponse:
            return Single.error(error)
        case MoyaError.encodableMapping, MoyaError.objectMapping, MoyaError.jsonMapping, MoyaError.stringMapping:
            return Single.error(Domain.Error.invalidResponse(message: "TTL_ERROR_INVALID_RESPONSE".localized))
        case MoyaError.underlying(let error, nil):
            return handleNSError(error as NSError)
        default:
            return handleNSError(error as NSError)
        }
    }

    static func handleNSError<T>(_ nsError: NSError) -> Single<T> {
        if nsError.code == 401 {
            return Single.error(Domain.Error.authError(message: "TTL_ERROR_AUTHORIZATION".localized))
        }

        if nsError.code == NSURLErrorCannotFindHost {
            return Single.error(Domain.Error.appError(message: "TTL_ERROR_CONNECTION".localized))
        }

        if nsError.code == NSURLErrorTimedOut {
            return Single.error(Domain.Error.serverError(message: "TTL_ERROR_CONNECTION".localized))
        }

        if nsError.code == NSURLErrorNetworkConnectionLost {
            return Single.error(Domain.Error.networkError(message: "TTL_ERROR_NO_INTERNET".localized))
        }

        if nsError.code == NSURLErrorNotConnectedToInternet {
            return Single.error(Domain.Error.networkError(message: "TTL_ERROR_NO_INTERNET".localized))
        }

        if nsError.domain == NSURLErrorDomain {
            return Single.error(Domain.Error.systemError(message: "TTL_ERROR_COMMON".localized))
        }

        // Abort request
        if nsError.domain == "kCFErrorDomainCFNetwork", nsError.code == 303 {
            return Single.error(Domain.Error.invalidResponse(message: "TTL_ERROR_INVALID_RESPONSE".localized))
        }

        return Single.error(Domain.Error.systemError(message: "TTL_ERROR_COMMON".localized))
    }
}
