//
//  MoyaRequest.swift
//  Networking
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Moya
import RxSwift
import Domain
import Utils

extension Reactive where Base: MoyaProviderType {

    func request(_ token: Base.Target) -> Single<Response> {
        return request(token, callbackQueue: nil)
            .filterSuccessfulStatusCodes()
            .flatMap({ (response) in
//                let JSONData = try response.mapJSON()
//                guard let JSON = JSONData as? [String: Any], !JSON.isEmpty else {
//                    return PrimitiveSequence.error(Domain.Error.invalidResponse(message:
//                "TTL_ERROR_COMMON".localized))
//                }

                if let error = try? JSONDecoder().decode(ServerStatusResponse.self, from: response.data),
                        error.status != .success {
                    return PrimitiveSequence.error(error)
                }

                return PrimitiveSequence.just(response)
            })
    }
}
//
//extension Reactive where Base: MoyaProviderType, Base.Target == FileProvider {
//
//    func request(_ token: Base.Target) -> Single<PRFile> {
//        if FileManager.default.fileExists(atPath: token.localURL.path) {
//            return Single.just(PRFile(localURL: token.localURL))
//        }
//        return request(token, callbackQueue: nil)
//            .map({ _ in PRFile(localURL: token.localURL) })
//    }
//}
