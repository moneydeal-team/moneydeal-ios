//
//  Constants.swift
//  Networking
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

enum Constants {

    enum ProviderConfiguration {
        static let Verbose = true
        static let cURL = true
    }

    enum API {
        enum URL {
            static let BaseHost = "https://balancer.moneydeal.kkapp.ru/"
        }

        enum User {
            static let Authorization = "login"
            static let Profile = "account"
        }

        enum Goal {
            static let List = "goal"
            static let Create = "goal"
            static let Upload = "file/upload"
        }
    }
}
