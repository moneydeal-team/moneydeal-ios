//
//  GoalUseCase.swift
//  Network
//
//  Created by Konstantin Kulakov on 06/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Domain
import Utils
import RxSwift
import Moya

class GoalUseCase: SessionUseCase<GoalProvider>, Domain.GoalUseCase {
    func goals(from date: Date, count: Int) -> Single<[Goal]> {
        return provider.rx.request(.goals(date: date.asString(format: Utils.Constants.defaultDateFormat),
                                          count: count))
            .map(GoalList.self, atKeyPath: "list")
            .catchError(MoyaErrorMapper.handleError)
    }

    func create(goal: Goal) -> Single<Void> {
        return provider.rx.request(.create(goal: goal))
            .map({ _ in return Single.just(()) })
            .catchError(MoyaErrorMapper.handleError)
    }

    func upload(file: OutgoingFile) -> Single<Domain.ImageInfo> {
        return provider.rx.request(.upload(file: file))
            .map(ImageInfo.self)
            .catchError(MoyaErrorMapper.handleError)
    }
}
