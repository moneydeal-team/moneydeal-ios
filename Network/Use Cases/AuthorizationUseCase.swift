//
//  AuthorizationUseCase.swift
//  Networking
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Domain
import RxSwift
import Moya

class AuthorizationUseCase: BaseUseCase, Domain.AuthorizationUseCase {

    private let provider = AuthorizationProvider.moyaProvider()

    func login(phone: String, token: String, googleToken: String, code: String) -> Single<Login> {
        return provider.rx.request(.login(phone: phone, token: token, googleToken: googleToken, code: code))
            .map(Login.self)
            .catchError(MoyaErrorMapper.handleError)
    }
}
