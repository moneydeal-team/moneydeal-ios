//
//  SessionUseCase.swift
//  Networking
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Moya
import Result

class SessionUseCase<T: BaseProvider & TargetType>: BaseUseCase {

    let provider: MoyaProvider<T>

    private override init() {
        provider = T.moyaProvider()

        super.init()
    }

    init(token: String) {
        provider = MoyaProvider<T>(requestClosure: { (endpoint, result) in
            // swiftlint:disable:next force_try
            var request = try! endpoint.urlRequest()
            request.addValue(token, forHTTPHeaderField: "X-Auth")
            request.addValue(Bundle.localization.serverLocal, forHTTPHeaderField: "X-Lang")

            result(.success(request))
        }, plugins: T.defaultPlugins)
    }
}
