//
//  SettingsUseCase.swift
//  Network
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Domain
import Utils
import RxSwift
import Moya

class SettingsUseCase: SessionUseCase<SettingsProvider>, Domain.SettingsUseCase {
    func obtainInfo() -> Single<UserInfo> {
        return provider.rx.request(.obtainInfo)
            .map(UserInfo.self)
            .catchError(MoyaErrorMapper.handleError)
    }

    func uploadAvatar(file: OutgoingFile) -> Single<Domain.ImageInfo> {
        return provider.rx.request(.upload(file: file))
            .map(ImageInfo.self)
            .catchError(MoyaErrorMapper.handleError)
    }

    func upadte(info: UserInfo) -> Single<Void> {
        return provider.rx.request(.update(info: info))
            .map({ _ in return Single.just(()) })
            .catchError(MoyaErrorMapper.handleError)
    }
}
