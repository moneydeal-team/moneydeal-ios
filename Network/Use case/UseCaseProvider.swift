//
//  UseCaseProvider.swift
//  Networking
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Domain
import Moya

public class UseCaseProvider: Domain.NetworkingUseCaseProvider {
    public init() {}

    public func makeAuthorizationUseCase() -> Domain.AuthorizationUseCase {
        return AuthorizationUseCase()
    }

    public func makeGoalUseCase(token: String?) -> Domain.GoalUseCase {
        return GoalUseCase(token: token ?? "")
    }

    public func makeSettingsUseCase(token: String?) -> Domain.SettingsUseCase {
        return SettingsUseCase(token: token ?? "")
    }
}
