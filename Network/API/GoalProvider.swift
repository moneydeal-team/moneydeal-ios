//
//  GoalProvider.swift
//  Network
//
//  Created by Konstantin Kulakov on 06/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Moya
import Domain

enum GoalProvider: BaseProvider {
    case goals(date: String, count: Int)
    case create(goal: Domain.Goal)
    case upload(file: OutgoingFile)
}

extension GoalProvider: TargetType {

    var path: String {
        switch self {
        case .goals: return "api/v1/\(Constants.API.Goal.List)"
        case .create: return "api/v1/\(Constants.API.Goal.List)"
        case .upload: return "api/v1/\(Constants.API.Goal.Upload)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .goals: return .get
        case .create: return .post
        case .upload: return .post
        }
    }

    var task: Task {
        switch self {
        case .goals:
            return .requestParameters(parameters: parameters, encoding: URLEncoding())
        case .create(let goal):
            return .requestJSONEncodable(goal)
        case let .upload(file):
            let fileData = MultipartFormData(provider: .data(file.data),
                                             name: "uploadfile",
                                             fileName: file.nameWithExt,
                                             mimeType: file.mimeType)
            let multipartData = [fileData]

            return .uploadMultipart(multipartData)
        }
    }

    var parameters: [String: Any] {
        switch self {
        case .goals(let date, let count):
            return [
                "from": date,
                "count": count
            ]
        case .create, .upload:
            return [:]
        }
    }
}
