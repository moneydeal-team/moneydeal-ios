//
//  BaseProvider.swift
//  Networking
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Moya
import SwiftyUserDefaults
import Utils

protocol BaseProvider {

}

extension BaseProvider {

    var baseURL: URL {
        let url = URL(string: Constants.API.URL.BaseHost)!

        return url
    }

    var path: String {
        return ""
    }

    var method: Moya.Method {
        return .post
    }

    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }

    var sampleData: Data {
        return "Not used?".data(using: .utf8)!
    }

    var task: Task {
        return .requestPlain
    }
}

extension BaseProvider where Self: TargetType {

    static var defaultPlugins: [PluginType] {
        return [
            NetworkLoggerPlugin(verbose: Constants.ProviderConfiguration.Verbose,
                                cURL: Constants.ProviderConfiguration.cURL)
        ]
    }

    static func moyaProvider() -> MoyaProvider<Self> {
        let provider = MoyaProvider<Self>(plugins: defaultPlugins)

        return provider
    }

    var headers: [String: String]? {
        return [:]
    }
}
