//
//  SettingsProvider.swift
//  Network
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Moya
import Domain

enum SettingsProvider: BaseProvider {
    case obtainInfo
    case upload(file: OutgoingFile)
    case update(info: UserInfo)
}

extension SettingsProvider: TargetType {

    var path: String {
        switch self {
        case .obtainInfo: return "api/v1/\(Constants.API.User.Profile)"
        case .upload: return "api/v1/\(Constants.API.Goal.Upload)"
        case .update: return "api/v1/\(Constants.API.User.Profile)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .obtainInfo: return .get
        case .upload: return .post
        case .update: return .put
        }
    }

    var task: Task {
        switch self {
        case .obtainInfo:
            return .requestParameters(parameters: parameters, encoding: URLEncoding())
        case .update(let userInfo):
            return .requestJSONEncodable(userInfo)
        case let .upload(file):
            let fileData = MultipartFormData(provider: .data(file.data),
                                             name: "uploadfile",
                                             fileName: file.nameWithExt,
                                             mimeType: file.mimeType)
            let multipartData = [fileData]

            return .uploadMultipart(multipartData)
        }
    }

    var parameters: [String: Any] {
        switch self {
        case .obtainInfo, .upload, .update:
            return [:]
        }
    }
}
