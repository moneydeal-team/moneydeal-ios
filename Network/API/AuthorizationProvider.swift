//
//  AuthorizationProvider.swift
//  Networking
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Moya

enum AuthorizationProvider: BaseProvider {
    case login(phone: String, token: String, googleToken: String, code: String)
}

extension AuthorizationProvider: TargetType {

    var path: String {
        switch self {
        case .login: return "api/v1/\(Constants.API.User.Authorization)"
        }
    }

    var method: Moya.Method {
        return .post
    }

    var task: Task {

        switch self {
        case .login:
            return .requestParameters(parameters: parameters, encoding: parameterEncoding)
        }
    }

    var parameters: [String: Any] {
        switch self {
        case .login(let phone, let token, let googleToken, let code):
            return [
                "phone": phone,
                "token": token,
                "google_token": googleToken,
                "code": code
            ]
        }
    }
}
