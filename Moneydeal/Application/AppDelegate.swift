//
//  AppDelegate.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 18/10/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import AppRouter
import Firebase

enum AppDelegateRouter {
    static let toAuthorization = InitialNavigationTransition(destination: AuthorizationViewController.self)
    static let toMain = InitialNavigationTransition(destination: MainTabBarViewController.self)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self

        setupTabBar()
        setupInitialViewController()

        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.badge, .alert, .sound]) { (_, _) in
            // Enable or disable features based on authorization.
        }
        application.registerForRemoteNotifications()

        return true
    }

    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }

    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        print("[DEBUG] Did recive remote notification: \(userInfo)")
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }
}

extension AppDelegate {

    private func setupInitialViewController() {
        if AuthorizationService.shared.userInfo != nil {
            AppDelegateRouter.toMain.go()
        } else {
            AppDelegateRouter.toAuthorization.go()
        }
    }
}

extension AppDelegate {
    private func setupTabBar() {
        UITabBar.appearance().tintColor = UIColor(red: 0.353, green: 0.245, blue: 0.775, alpha: 1)
    }
}

extension AppDelegate: MessagingDelegate {

    // MARK: - MessagingDelegate
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("FCM Token: \(fcmToken)")

        AuthorizationService.shared.fcmToken = fcmToken

        let dataDict: [String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
}
