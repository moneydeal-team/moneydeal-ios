//
//  AuthorizationViewModel.swift
//  Moneydeal
//
//  Created by Arsenij Zorin on 18/11/2019.
//  Copyright © 2019 Arsenij Zorin. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Domain

class AuthorizationViewModel: ViewModel, ActivityTrackable, ErrorTrackable {
    let activityTracker = ActivityTracker()
    let errorTracker = ErrorTracker()

    func checkPhone(for phone: String) -> Bool {
        let maxLendth: Int = Constants.Phone.russianPhoneLendth

        return phone.count >= maxLendth
    }
}

extension AuthorizationViewModel: ViewModelType {
    struct Input {
        let phoneNumber: Driver<String>
        let verifyAction: Driver<()>
    }

    struct Output {
        let result: Driver<SMSAuthorizationInfo>
        let isReady: Driver<Bool>
    }

    func transform(input: AuthorizationViewModel.Input) -> AuthorizationViewModel.Output {
        let result = PublishSubject<SMSAuthorizationInfo>()

        input.verifyAction
        .withLatestFrom(input.phoneNumber)
        .flatMapLatest({ [unowned self] phone -> Driver<SMSAuthorizationInfo> in
            SMSAuthorizationService.send(for: phone)
            .trackActivity(self.activityTracker)
            .trackError(self.errorTracker)
            .asDriverOnErrorJustComplete()
        })
        .drive(result)
        .disposed(by: bag)

        let isReady = PublishSubject<Bool>()

        input.phoneNumber
        .map({ [unowned self] in
            self.checkPhone(for: $0)
        })
        .drive(isReady)
        .disposed(by: bag)

        return .init(result: result.asDriverOnErrorJustComplete(),
                     isReady: isReady.asDriverOnErrorJustComplete())
    }
}
