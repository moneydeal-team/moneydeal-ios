//
//  AuthorizationViewController.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 18/10/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxCocoa
import RxSwift
import Domain
import InputMask

enum AuthorizationRouter {
    static let toVerification = PushTransition(destination: SMSVerificationViewController.self)
}

class AuthorizationViewController: ViewController, ViewConfigurator, ViewModelHolder {

    var indicatorView: ActivityIndicatorView! = ActivityIndicatorView()

    var viewModel: AuthorizationViewModel? = AuthorizationViewModel()

    let phoneMaskListner = MaskedTextFieldDelegate()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Show the navigation bar on other view controllers
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupPhoneMaskListner()
        hideKeyboardWhenTappedAround()
    }

    func configure(with view: AuthorizationView) {
        view.configure()

        view.updateAuthButton(status: false)

        adjustBottomWithKeyboard(for: view.contentView)

        view.phoneTextField.delegate = phoneMaskListner

        guard let viewModel = viewModel else { return }

        let verifyAction = view.authButton.rx.tap
                           .throttleTapInMainScheduler()
                           .asDriverOnErrorJustComplete()

        let phoneNumber = view.phoneTextField.rx.textChange.filterNil().asDriverOnErrorJustComplete()

        let output = viewModel.transform(input: .init(phoneNumber: phoneNumber,
                                                      verifyAction: verifyAction))

        output.result
        .asTransitionable()
        .transit(to: AuthorizationRouter.toVerification)
        .disposed(by: bag)

        output.isReady.drive(onNext: { [unowned view] isReady in
            view.updateAuthButton(status: isReady)
        })
        .disposed(by: bag)
    }

    func setupPhoneMaskListner() {
        phoneMaskListner.primaryMaskFormat = Constants.Pattern.phoneMask
    }
}

extension AuthorizationViewController: TransitionPresenter, Transitionable {}
extension AuthorizationViewController: KeyboardAdjustable {}
extension AuthorizationViewController: ErrorPresentable {}
extension AuthorizationViewController: ActivityIndicatorContainer {}
extension AuthorizationViewController: AutoBinder {}
