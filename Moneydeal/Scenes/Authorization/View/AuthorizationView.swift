//
//  AuthorizationView.swift
//  Moneydeal
//
//  Created by Arsenij Zorin on 11/11/2019.
//  Copyright © 2019 Arsenij Zorin. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Utils

class AuthorizationView: View {
    let contentView = UIScrollView()
    fileprivate let rootFlexContainer = UIView()

    let appLogoImageView = UIImageView()
    let appNameLabel = UILabel()
    let enterPhoneLabel = UILabel()
    let phoneTextField = PlaceholderTextField()
    let termTextView = UITextView()
    let authButton = UIButton()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Behavior for layout
    override func layoutSubviews() {
        super.layoutSubviews()

        // Layout the contentView & rootFlexContainer using PinLayout
        contentView.pin.all(pin.safeArea)
        rootFlexContainer.pin.top().horizontally()

        // Let the flexbox container layout itself and adjust the height
        rootFlexContainer.flex.layout(mode: .adjustHeight)

        // Adjust the scrollview contentSize
        contentView.contentSize = rootFlexContainer.frame.size
    }
}

extension AuthorizationView {
    /// Setup all styles for UI elements
    func setup() {
        setupAppLogoImageView()
        setupAppNameLabel()
        setupEnterPhoneLabel()
        setupPhoneTextField()
        setupTermTextView()
        setupAuthButton()

        backgroundColor = .white
    }

    /// Setup styles for app logo image view
    func setupAppLogoImageView() {
        appLogoImageView.image = UIImage(named: "md_app_logo")

        // Setup shadow
        appLogoImageView.layer.shadowColor = UIColor(red: 0.369, green: 0.867, blue: 0.718, alpha: 0.21).cgColor
        appLogoImageView.layer.shadowOpacity = 1
        appLogoImageView.layer.shadowOffset = CGSize(width: 15.0, height: 20.0)
        appLogoImageView.layer.shadowRadius = 8

        appLogoImageView.layer.shouldRasterize = true

        appLogoImageView.layer.rasterizationScale = UIScreen.main.scale
    }

    /// Setup styles for app name label
    func setupAppNameLabel() {
        appNameLabel.textColor = UIColor(red: 0.231, green: 0.255, blue: 0.294, alpha: 1)

        appNameLabel.font = UIFont(name: "GothamPro-Bold", size: 18)

        appNameLabel.text = "TTL_AUTHORIZATION_APP_NAME_TITLE".localized
    }

    /// Setup styles for enter phone label
    func setupEnterPhoneLabel() {
        enterPhoneLabel.textColor = .black

        enterPhoneLabel.font = UIFont(name: "GothamPro-Bold", size: 18)

        enterPhoneLabel.text = "TTL_AUTHORIZATION_ENTERPHONE_LABEL_TITLE".localized
    }

    /// Setup styles for phone text field
    func setupPhoneTextField() {
        phoneTextField.font = UIFont(name: "GothamPro", size: 17)

        //phoneTextField.placeholderFontScale = 1

        phoneTextField.textColor = UIColor(red: 0.09, green: 0.114, blue: 0.2, alpha: 1)
        phoneTextField.placeholderColor = UIColor(red: 0.651, green: 0.667, blue: 0.706, alpha: 1)

        phoneTextField.activeBorderColor = UIColor(red: 0.38, green: 0.243, blue: 0.918, alpha: 1)
        phoneTextField.inactiveBorderColor = UIColor(red: 0.651, green: 0.667, blue: 0.706, alpha: 1)

        phoneTextField.keyboardType = .phonePad
        phoneTextField.autocorrectionType = .no
        phoneTextField.autocapitalizationType = .none

        phoneTextField.backgroundColor = .white

        phoneTextField.cornerRadius = 6.0
        phoneTextField.borderSize = 1.0

        phoneTextField.placeholder = "TTL_AUTHORIZATION_TEXTFIELD_PHONE_PLACEHOLDER".localized
    }

    /// Setup styles for term label
    func setupTermTextView() {
        termTextView.backgroundColor = .clear

        termTextView.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)

        termTextView.font = UIFont(name: "GothamPro", size: 12.0)

        termTextView.textAlignment = .center

        termTextView.isEditable = false

        let termText = String(format: "TTL_AUTHORIZATION_TEXTVIEW_TERM_TEXT".localized,
                              "TTL_AUTHORIZATION_SIGNIN_BUTTON_TITLE".localized)

        let attribetedString = NSMutableAttributedString(string: termText)

        if let privacyURL = URL(string: Constants.Links.privacyPolicy) {
            attribetedString.setLink(for: "TTL_AUTHORIZATION_TEXTVIEW_TERM_TEXT_LINK_CONF".localized,
                                     with: privacyURL.absoluteString)
        }

        if let rulesURL = URL(string: Constants.Links.term) {
            attribetedString.setLink(for: "TTL_AUTHORIZATION_TEXTVIEW_TERM_TEXT_LINK_RULES".localized,
                                     with: rulesURL.absoluteString)
        }

        termTextView.attributedText = attribetedString

        termTextView.linkTextAttributes = [.foregroundColor: UIColor(red: 0.353, green: 0.245, blue: 0.775, alpha: 1)]
    }

    /// Setup styles for auth button
    func setupAuthButton() {
        authButton.setTitle("TTL_AUTHORIZATION_SIGNIN_BUTTON_TITLE".localized,
                            for: .normal)

        authButton.isEnabled = false

        authButton.titleLabel?.font = UIFont(name: "GothamPro-Bold", size: 16.0)

        authButton.layer.cornerRadius = 6.0

        authButton.backgroundColor = UIColor(red: 0.353, green: 0.245, blue: 0.775, alpha: 1)
        authButton.tintColor = .white

        authButton.setTitleColor(.lightGray, for: .highlighted)

        // Setup shadow
        authButton.layer.shadowColor = UIColor(red: 0.38, green: 0.243, blue: 0.918, alpha: 0.32).cgColor
        authButton.layer.shadowOpacity = 1
        authButton.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        authButton.layer.shadowRadius = 8

        authButton.layer.shouldRasterize = true

        authButton.layer.rasterizationScale = UIScreen.main.scale
    }
}

extension AuthorizationView {
    func updateAuthButton(status isReady: Bool) {
        authButton.isEnabled = isReady

        UIView.animate(withDuration: 0.4) { [weak self] in
            self?.authButton.layer.shadowOpacity = isReady ? 1.0 : 0
            self?.authButton.alpha = isReady ? 1.0 : 0.5
        }
    }
}

extension AuthorizationView: BaseView {
    /// Configure layout for view
    func configure() {

        // Configure layout for root container
        rootFlexContainer
        .flex
        .direction(.column)
        .alignItems(.center)
        .define { flex in
            // Configure layout for each UI element
            layoutAppLogoImageView(for: flex)
            layoutAppNameLabel(for: flex)
            layoutEnterPhoneLabel(for: flex)
            layoutPhoneTextField(for: flex)
            layoutTermTextView(for: flex)
            layoutAuthButton(for: flex)
        }

        contentView.addSubview(rootFlexContainer)

        addSubview(contentView)
    }

    /// Layout for app logo image view
    func layoutAppLogoImageView(for flex: Flex) {
        let marginTop: CGFloat = 92.0

        flex.addItem(appLogoImageView).marginTop(marginTop)
    }

    /// Layout for app name label
    func layoutAppNameLabel(for flex: Flex) {
        let marginTop: CGFloat = 24.0

        flex.addItem(appNameLabel).marginTop(marginTop)
    }

    /// Layout for enter phone label
    func layoutEnterPhoneLabel(for flex: Flex) {
        let marginTop: CGFloat = 67.0

        flex.addItem(enterPhoneLabel).marginTop(marginTop)
    }

    /// Layout for phone text field
    func layoutPhoneTextField(for flex: Flex) {
        let height: CGFloat = 66.0

        let marginTop: CGFloat = 33.0

        let marginHorizontal: CGFloat = 16.0

        // Add other container for margin spaces
        flex.addItem().marginTop(marginTop).width(100%).define { flex in
            flex.addItem(phoneTextField).minHeight(height).marginHorizontal(marginHorizontal)
        }
    }

    /// Layout for term label
    func layoutTermTextView(for flex: Flex) {
        let marginTop: CGFloat = 37.0

        let marginHorizontal: CGFloat = 24.0

        flex.addItem(termTextView).marginTop(marginTop).marginHorizontal(marginHorizontal)
    }

    /// Layout for auth button
    func layoutAuthButton(for flex: Flex) {
        let height: CGFloat = 56.0

        let marginTop: CGFloat = 47.0

        let marginHorizontal: CGFloat = 16.0

        let marginBottom: CGFloat = 20.0

        // Add other container for margin spaces
        flex.addItem()
        .marginTop(marginTop)
        .marginBottom(marginBottom)
        .width(100%).define { flex in
            flex.addItem(authButton).height(height).marginHorizontal(marginHorizontal)
        }
    }
}
