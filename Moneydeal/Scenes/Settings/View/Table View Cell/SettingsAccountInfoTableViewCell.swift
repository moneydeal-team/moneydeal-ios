//
//  SettingsAccountInfoTableViewCell.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class SettingsAccountInfoTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {

    var viewModel: SettingsAccountInfoCellViewModel? {
        didSet {
            setupBindings()
        }
    }

    let upperTitleLabel = UILabel()
    let valueTextField = UITextField()

    let separatorView = UIView()

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    override func setupLayout() {
        let cellMargin = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16)

        let titleMargin = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        contentView.flex.define { flex in
            flex.addItem()
                .margin(cellMargin)
                .direction(.column).define { flex in
                    flex.addItem(upperTitleLabel).margin(titleMargin).grow(1).shrink(1)
                    flex.addItem(valueTextField).grow(1).shrink(1)
            }

            flex.addItem(separatorView)
                .height(0.5)
                .position(.absolute)
                .bottom(0)
                .left(cellMargin.left)
                .right(0)
        }
    }

    override func setup() {
        setupValueTextField()
        setupUpperTitleLabel()
        setupSeparatorView()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    func setupValueTextField() {
        valueTextField.font = UIFont(name: "GothamPro", size: 17)
        valueTextField.textColor = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
    }

    func setupUpperTitleLabel() {
        upperTitleLabel.font = UIFont(name: "GothamPro", size: 12)
        upperTitleLabel.textColor = UIColor(red: 0.702, green: 0.702, blue: 0.702, alpha: 1)

        upperTitleLabel.numberOfLines = 1
        upperTitleLabel.minimumScaleFactor = 1
        upperTitleLabel.adjustsFontSizeToFitWidth = true
        upperTitleLabel.lineBreakMode = .byTruncatingTail
    }

    func setupSeparatorView() {
        separatorView.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        backgroundColor = .clear
        valueTextField.text = nil
        upperTitleLabel.text = nil
    }

    func setupBindings() {
        guard let vm = viewModel else { return }

        upperTitleLabel.text = vm.upperTitle
        valueTextField.text = vm.valueTitle

        valueTextField.isSecureTextEntry = vm.isSecure
        valueTextField.isEnabled = vm.isEnabled

        upperTitleLabel.flex.markDirty()
        valueTextField.flex.markDirty()

        valueTextField.rx.controlEvent([.editingDidEnd, .editingDidEndOnExit])
        .map({ _ in self.valueTextField.text })
        .filterNil()
        .asDriverOnErrorJustComplete()
        .drive(vm.subject)
        .disposed(by: bag)

        setNeedsLayout()
    }
}
