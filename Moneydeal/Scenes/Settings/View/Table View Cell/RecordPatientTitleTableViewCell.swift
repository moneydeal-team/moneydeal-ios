//
//  RecordPatientTitleTableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import UIKit

class RecordPatientTitleTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    let titleLabel = UILabel()

    var viewModel: RecordPatientTitleCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)

        isOpaque = true
        alpha = 1
    }

    func configure() {
        titleLabel.text = viewModel?.title

        titleLabel.flex.markDirty()

        layoutSubviews()
    }

    override func setup() {
        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = UIColor(red: 0.94, green: 0.94, blue: 0.96, alpha: 1)

        contentView.backgroundColor = UIColor(red: 0.94, green: 0.94, blue: 0.96, alpha: 1)

        titleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)

        titleLabel.font = UIFont(name: "GothamPro", size: 13)

        contentView.isOpaque = true
        contentView.alpha = 1.0

        isOpaque = true
        alpha = 1
    }

    override func setupLayout() {
        let cellMargin = UIEdgeInsets(top: 10, left: 16, bottom: 4, right: 16)

        contentView.flex.define { flex in
            flex.addItem().margin(cellMargin).define { flex in
                flex.addItem(titleLabel)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = nil
    }
}
