//
//  SettingsAccountExitTableViewCell.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class SettingsAccountExitTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {

    var viewModel: SettingsAccountExitCellViewModel?

    let titleLabel = UILabel()

    let logoutImageView = UIImageView()

    let separatorView = UIView()

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    override func setupLayout() {
        let cellMargin = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16)

        let titleMargin = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)

        contentView.flex.define { flex in
            flex.addItem().margin(cellMargin)
                .alignItems(.center)
                .direction(.row).define { flex in
                    flex.addItem(logoutImageView).minWidth(24)
                    flex.addItem(titleLabel).margin(titleMargin)
            }

            flex.addItem(separatorView)
                .height(0.5)
                .position(.absolute)
                .bottom(0)
                .left(cellMargin.left)
                .right(0)
        }
    }

    override func setup() {
        setupTitleLabel()
        setupLogoutImageView()
        setupSeparatorView()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    func setupTitleLabel() {
        titleLabel.font = UIFont(name: "SFProDisplay-Regular", size: 17)
        titleLabel.textColor = UIColor(red: 0.631, green: 0.631, blue: 0.631, alpha: 1)

        titleLabel.text = "TTL_SETTINGS_SECTION_ACCOUNT_EXIT_TITLE".localized
    }

    func setupLogoutImageView() {
        logoutImageView.image = UIImage(named: "is_settings_logout")
    }

    func setupSeparatorView() {
        separatorView.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
