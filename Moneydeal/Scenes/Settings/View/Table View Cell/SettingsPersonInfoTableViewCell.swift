//
//  SettingsPersonInfoTableViewCell.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class SettingsPersonInfoTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {

    var viewModel: SettingsPersonInfoCellViewModel? {
        didSet {
            setupBindings()
        }
    }

    let avatarImageView = UIImageView()

    let nameTitleLabel = UILabel()

    let nextImageView = UIImageView()

    let activityIndicator = UIActivityIndicatorView()

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
        activityIndicator.pin.center()
    }

    override func setupLayout() {
        let cellMargin = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16)

        let avatarMargin = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 11)

        contentView.flex.backgroundColor(.white).define { flex in
            flex.addItem().backgroundColor(.white).margin(cellMargin)
                .alignItems(.center)
                .direction(.row).define { flex in
                    flex.addItem().margin(avatarMargin).width(60).height(60).define { flex in
                        flex.addItem(avatarImageView)
                        flex.addItem(activityIndicator).position(.absolute)
                    }
                    flex.addItem(nameTitleLabel).grow(1).shrink(1)

                    flex.addItem().direction(.row).justifyContent(.end).grow(1).define { flex in
                        flex.addItem(nextImageView).minWidth(10)
                    }
            }
        }
    }

    override func setup() {
        setupNameTitleLabel()
        setupNextImageView()
        setupAvatarImageView()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .white
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        backgroundColor = .clear
        nameTitleLabel.text = nil

        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }

    func setupBindings() {
        guard let vm = viewModel else { return }

        nameTitleLabel.text = vm.fullName
        nameTitleLabel.flex.markDirty()

        setupAvatarImage(from: vm.avatarURL)

        setNeedsLayout()
    }

    func setupAvatarImage(from url: URL?) {
        guard let imageURL = url else { return }

        activityIndicator.isHidden = false
        activityIndicator.startAnimating()

        avatarImageView.kf.setImage(with: imageURL,
                                    placeholder: UIImage(named: "is_settings_avatar_default"),
                                    options: [.backgroundDecode],
            completionHandler: ({ [weak self] (_, _, _, _) in
                                        self?.activityIndicator.stopAnimating()
                                        self?.activityIndicator.isHidden = true
        }), auth: true)

    }
}

extension SettingsPersonInfoTableViewCell {
    func setupNameTitleLabel() {
        nameTitleLabel.font = UIFont(name: "SFProDisplay-Regular", size: 22)
        nameTitleLabel.textColor = .black

        nameTitleLabel.numberOfLines = 2
        nameTitleLabel.minimumScaleFactor = 0.7
        nameTitleLabel.adjustsFontSizeToFitWidth = true
        nameTitleLabel.lineBreakMode = .byTruncatingTail
    }

    func setupNextImageView() {
        nextImageView.image = UIImage(named: "is_services_next")
        nextImageView.contentMode = .right
    }

    func setupAvatarImageView() {
        avatarImageView.image = UIImage(named: "is_settings_avatar_default")
        avatarImageView.layer.masksToBounds = true
        avatarImageView.layer.cornerRadius = 30.0
        avatarImageView.contentMode = .scaleAspectFill
    }
}
