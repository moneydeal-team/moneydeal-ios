//
//  SettingsSelectActionTableViewCell.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class SettingsSelectActionTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {

    var viewModel: SettingsSelectActionCellViewModel? {
        didSet {
            setupBindings()
        }
    }

    let nameTitleLabel = UILabel()

    let nextImageView = UIImageView()

    let separatorView = UIView()

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    override func setupLayout() {
        let cellMargin = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16)

        let titleMargin = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        contentView.flex.define { flex in
            flex.addItem().margin(cellMargin)
                .alignItems(.center)
                .justifyContent(.spaceBetween)
                .direction(.row).define { flex in
                    flex.addItem(nameTitleLabel).margin(titleMargin)
                    flex.addItem(nextImageView).minWidth(10)
            }

            flex.addItem(separatorView)
                .height(0.5)
                .position(.absolute)
                .bottom(0)
                .left(cellMargin.left)
                .right(0)
        }
    }

    override func setup() {
        setupNameTitleLabel()
        setupNextImageView()
        setupSeparatorView()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    func setupNameTitleLabel() {
        nameTitleLabel.font = UIFont(name: "SFProDisplay-Regular", size: 17)
        nameTitleLabel.textColor = .black

        nameTitleLabel.numberOfLines = 1
        nameTitleLabel.minimumScaleFactor = 1
        nameTitleLabel.adjustsFontSizeToFitWidth = true
        nameTitleLabel.lineBreakMode = .byTruncatingTail
    }

    func setupNextImageView() {
        nextImageView.image = UIImage(named: "is_services_next")
        nextImageView.contentMode = .right
    }

    func setupSeparatorView() {
        separatorView.backgroundColor = UIColor(red: 0.78, green: 0.78, blue: 0.8, alpha: 1)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        backgroundColor = .clear
        nameTitleLabel.text = nil
    }

    func setupBindings() {
        guard let vm = viewModel else { return }

        nameTitleLabel.text = vm.defaultTitle
        nameTitleLabel.flex.markDirty()

        vm.titleSubject.filterNil()
        .subscribe(onNext: { [unowned self] text in
            self.nameTitleLabel.text = text

            self.nameTitleLabel.flex.markDirty()
            self.setNeedsLayout()
        })
        .disposed(by: reuseBag)

        if vm.isHiddenSeparator {
            separatorView.flex.left(0)
            separatorView.flex.markDirty()
        }

        setNeedsLayout()
    }
}
