//
//  SettingsTopTableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class SettingsTopTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    let label = UILabel()
    let closeButton = UIButton()

    var viewModel: SettingsTopCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        closeButton.rx.tap.throttleTapInMainScheduler()
        .asDriverOnErrorJustComplete()
        .drive(vm.closeSubject)
        .disposed(by: reuseBag)
    }

    override func setup() {
        setupCellStyles()
        setupLabel()
        setupCloseButton()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.direction(.row).define { flex in
            layoutLabel(for: flex)
            flex.addItem().position(.absolute).right(14.0).define { flex in
                layoutCloseButton(for: flex)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension SettingsTopTableViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width

        contentView.flex.minWidth(headerWidth)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }

    /// Setup styles for label
    func setupLabel() {
        label.textColor = .black

        label.font = UIFont(name: "GothamPro-Bold", size: 24)
        label.text = "TTL_TABBAR_SETTINGS_TITLE".localized

        label.numberOfLines = 0
        label.textAlignment = .left
    }

    /// Setup styles for close button
    func setupCloseButton() {
        closeButton.setImage(UIImage(named: "Close"), for: .normal)
    }
}

extension SettingsTopTableViewCell {
    /// Layout for label
    func layoutLabel(for flex: Flex) {
        flex.addItem(label).marginTop(32.0).marginLeft(14.0)
    }

    /// Layout fot close button
    func layoutCloseButton(for flex: Flex) {
        flex.addItem(closeButton).size(64.0)
    }
}
