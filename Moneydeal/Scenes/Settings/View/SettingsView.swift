//
//  SettingsView.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class SettingsView: View {
    private let rootFlexContainer = UIView()

    let tableView = UITableView()

    let refreshControl = UIRefreshControl()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        tableView.separatorStyle = .none
        tableView.refreshControl = refreshControl
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        rootFlexContainer.pin.top(pin.safeArea).horizontally().bottom()

        rootFlexContainer.flex.layout(mode: .fitContainer)
    }

}

extension SettingsView: BaseView {
    func configure() {
        rootFlexContainer.flex.define { flex in
            flex.addItem(tableView).grow(1)
        }

        addSubview(rootFlexContainer)
    }
}
