//
//  SettingsViewController.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import Kingfisher
import Utils
import MobileCoreServices
import Photos

enum SettingsRouter {
    static let toAuthorization = InitialNavigationTransition(destination: AuthorizationViewController.self)
    static let toMain = InitialNavigationTransition(destination: MainTabBarViewController.self)
}

class SettingsViewController: TableViewController, ViewModelHolder, ViewConfigurator {
    var viewModel: SettingsViewModel? = SettingsViewModel()

    var indicatorView: ActivityIndicatorView! = ActivityIndicatorView()

    var dataSource: TableViewDataSource<Section<CellViewModel>>?

    var saveButton: UIBarButtonItem?

    lazy var picker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.mediaTypes = [String(kUTTypeImage)]
        return picker
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "TTL_TABBAR_SETTINGS_TITLE".localized

        setupSaveButton()

        hideKeyboardWhenTappedAround()
    }

    func setupSaveButton() {
        saveButton = UIBarButtonItem(title: "TTL_ADDRECORD_SAVE_BUTTON".localized,
                                    style: .done,
                                    target: nil,
                                    action: nil)

        saveButton?.tintColor = .white
        //navigationItem.rightBarButtonItem = saveButton
    }

    func configure(with view: SettingsView) {
        view.configure()

        adjustBottomWithKeyboard(for: view.tableView)

        dataSource = TableViewDataSource(cellMap: cellMap)

        guard let vm = viewModel,
              let saveButton = saveButton else { return }

        let refresh = view.refreshControl.rx.controlEvent(.valueChanged)
                        .flatMapLatest({ [unowned self] in
                            return self.mainView.tableView.rx.didEndDragging.take(1).mapToVoid()
                        })
                        .asDriverOnErrorJustComplete()

        vm.refreshTracker.drive(view.refreshControl.rx.isRefreshing).disposed(by: bag)

        let avatar = picker.fileSubject.asDriverOnErrorJustComplete()

        picker.fileSubject.subscribe(onNext: { _ in
            KingfisherManager.shared.cache.clearMemoryCache()
            KingfisherManager.shared.cache.clearDiskCache()
            KingfisherManager.shared.cache.cleanExpiredDiskCache()
        }).disposed(by: bag)

        let output = vm.transform(input: .init(saveButton: saveButton.rx
                                                        .tap
                                                        .throttleTapInMainScheduler()
                                                        .asDriverOnErrorJustComplete(),
                                               avatar: avatar,
                                               refresh: refresh))

        output.sections.drive(view.tableView.rx.items(dataSource: dataSource!)).disposed(by: bag)

        output.actionSelection.filter({ $0 == .exit })
        .drive(onNext: { [weak self] _ in
            guard let self = self else { return }

            let alertVC = UIAlertController.init(title: "TTL_SETTINGS_EXIT_ALERT_TITLE".localized,
                                                 message: "TTL_SETTINGS_EXIT_ALERT_MSG".localized,
                                                 style: .alert)

            let cancelAction = UIAlertAction(title: "TTL_SETTINGS_EXIT_ALERT_CANCEL".localized,
                                             style: .cancel)

            let clearAction = UIAlertAction(title: "TTL_SETTINGS_EXIT_ALERT_OK".localized,
                                            style: .destructive) { [weak self] _ in
                self?.viewModel?.logoutAccount()
                SettingsRouter.toAuthorization.go()
            }

            alertVC.addAction(cancelAction)
            alertVC.addAction(clearAction)

            self.present(alertVC, animated: true)
        }).disposed(by: bag)

        output.closeSubject.drive(onNext: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)

            self?.dismiss(animated: true, completion: nil)
        })
        .disposed(by: bag)

        output.actionSelection.filter({ $0 == .lang })
        .drive(onNext: { [weak self] _ in
            guard let self = self else { return }

            let languages: [Language] = [.ru, .en]

            let options = AlertController(title: "TTL_SETTINGS_SELECT_LANGUAGE_ACTION_TITLE".localized,
                                          style: .actionSheet)

            options.addAction(UIAlertAction.cancel())

            options.rx.withActions(languages)
            .subscribe(onNext: { (selectedLang) in
                Bundle.localization = selectedLang
                SettingsRouter.toMain.go()
            })
            .disposed(by: self.bag)

            self.present(options, animated: true)
        })
        .disposed(by: bag)

        output.actionSelection.filter({ $0 == .avatar })
        .drive(onNext: { [weak self] _ in
            guard let self = self else { return }

            let camera = SettingsCameraAction { [unowned self] in
                self.onCameraTap()
            }
            let library = SettingsLibraryAction { [unowned self] in
                self.onGalleryTap()
            }
            let cancel = CancelAlertAction()

            let sheet = AlertController(style: .actionSheet).withActions([camera, library, cancel])
            self.present(sheet, animated: true, completion: nil)

        }).disposed(by: bag)

        view.tableView.rx.itemSelected
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [dataSource] index in
                guard let selectable = try? dataSource?.model(at: index) as? Selectable else {
                    return
                }

                Vibration.selection.vibrate()
                selectable.onSelect()
            })
            .disposed(by: bag)
    }

    override func configureCellMap() -> CellMap {
        return [
            SettingsSelectActionCellViewModel.className(): SettingsSelectActionTableViewCell.self,
            SettingsPersonInfoCellViewModel.className(): SettingsPersonInfoTableViewCell.self,
            SettingsInfoControlCellViewModel.className(): SettingsInfoControlTableViewCell.self,
            SettingsAccountInfoCellViewModel.className(): SettingsAccountInfoTableViewCell.self,
            RecordPatientTitleCellViewModel.className(): RecordPatientTitleTableViewCell.self,
            SettingsAccountExitCellViewModel.className(): SettingsAccountExitTableViewCell.self,
            EmptyCellViewModel.className(): EmptyTableViewCell.self,
            SettingsTopCellViewModel.className(): SettingsTopTableViewCell.self
        ]
    }
}

extension SettingsViewController {
    private func onCameraTap() {
        let ok = OKAlertAction()
        let settings = SettingsAlertAction()

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            AVCaptureDevice.rx.status(for: AVMediaType.video)
                .map({ [unowned self] status -> UIViewController in
                    if status == .authorized {
                        self.picker.sourceType = .camera
                        return self.picker
                    } else {
                        return AlertController(title: "TTL_PERMISSION_DENIED_TITLE".localized,
                                               message: "TTL_PERMISSION_DENIED_CAMERA_MESSAGE".localized,
                                               style: .alert).withActions([ok, settings])
                    }
                })
                .subscribe(onSuccess: { [weak self] vc in
                    self?.present(vc, animated: true, completion: nil)
                })
                .disposed(by: bag)
        } else {
            let alert = AlertController(title: "TTL_ERROR_TITLE".localized,
                                        message: "TTL_PROFILE_VIEW_CAMERA_UNAVAILABLE".localized,
                                        style: .alert)
                .withActions([OKAlertAction()])

            present(alert, animated: true, completion: nil)
        }
    }

    private func onGalleryTap() {
        let ok = OKAlertAction()
        let settings = SettingsAlertAction()

        PHPhotoLibrary.shared().rx.status
            .map({ [unowned self] status -> UIViewController in
                if status == .authorized {
                    self.picker.sourceType = .photoLibrary
                    return self.picker
                } else {
                    return AlertController(title: "TTL_PERMISSION_DENIED_TITLE".localized,
                                           message: "TTL_PERMISSION_DENIED_PHOTO_MESSAGE".localized,
                                           style: .alert).withActions([ok, settings])
                }
            })
            .subscribe(onSuccess: { [weak self] vc in
                self?.present(vc, animated: true, completion: {
                    // Device specific bug fix
                    self?.picker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
                })
            })
            .disposed(by: bag)
    }
}

extension SettingsViewController: Transitionable {}

extension SettingsViewController: KeyboardAdjustable {}
extension SettingsViewController: ErrorPresentable {}
extension SettingsViewController: AutoBinder {}
extension SettingsViewController: ActivityIndicatorContainer {}

extension SettingsViewController: MDNavigationBarHolder {
    var isLargeTitleMode: Bool {
        false
    }

    var state: MDNavigationBarState {
        .hide
    }
}
