//
//  Language+AlertAction.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 05/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit
import Utils

extension Language: AlertActionType {

    var title: String {
        switch self {
        case .ru:
            return "TTL_LANG_NAME_RU".localized
        case .en:
            return "TTL_LANG_NAME_EN".localized
        }
    }

    internal var style: UIAlertAction.Style {
        return .default
    }
}
