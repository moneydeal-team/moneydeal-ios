//
//  SettingsSelectActionCellViewModel.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import Domain

class SettingsSelectActionCellViewModel: CellViewModel, ActionEmittable {
    var actionType: SettingsAction
    var selectionSubject: PublishSubject<SettingsAction>

    let titleSubject = BehaviorSubject<String?>(value: nil)

    let defaultTitle: String

    let isHiddenSeparator: Bool

    init(defaultTitle: String,
         isHiddenSeparator: Bool = false,
         subject: PublishSubject<String>,
         selectionSubject: PublishSubject<SettingsAction>,
         actionType: SettingsAction) {
        self.selectionSubject = selectionSubject
        self.defaultTitle = defaultTitle
        self.isHiddenSeparator = isHiddenSeparator
        self.actionType = actionType

        super.init()

        subject
        .bind(to: titleSubject)
        .disposed(by: bag)
    }
}

extension SettingsSelectActionCellViewModel: Selectable {
    func onSelect() {
        emitAction()
    }
}
