//
//  SettingsAccountInfoCellViewModel.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import Domain

class SettingsAccountInfoCellViewModel: CellViewModel {
    let subject: BehaviorSubject<String?>

    let valueTitle: String
    let upperTitle: String

    let isSecure: Bool
    let isEnabled: Bool

    init(valueTitle: String,
         upperTitle: String,
         isEnabled: Bool = false,
         isSecure: Bool = false,
         subject: BehaviorSubject<String?>) {
        self.valueTitle = valueTitle
        self.isSecure = isSecure
        self.upperTitle = upperTitle
        self.isEnabled = isEnabled

        self.subject = subject

        super.init()
    }
}
