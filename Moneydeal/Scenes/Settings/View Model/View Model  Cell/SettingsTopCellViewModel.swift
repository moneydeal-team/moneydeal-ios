//
//  SettingsTopCellViewModel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SettingsTopCellViewModel: CellViewModel {
    let closeSubject: PublishSubject<Void>

    init(_ closeSubject: PublishSubject<Void>) {
        self.closeSubject = closeSubject

        super.init()
    }
}
