//
//  SettingsPersonInfoCellViewModel.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import Network
import Domain

class SettingsPersonInfoCellViewModel: CellViewModel, ActionEmittable {
    var actionType: SettingsAction = .avatar
    var selectionSubject: PublishSubject<SettingsAction>

    let userInfo: UserInfo

    var fullName: String {
        return String(format: "%@ %@", userInfo.name ?? "", userInfo.lastName ?? "")
    }

    var avatarURL: URL? {
        guard let avatarLink = userInfo.avatar else { return nil }

        return URL(string: Constants.Dispather.imageServer + avatarLink)
    }

    init(userInfo: UserInfo,
         selectionSubject: PublishSubject<SettingsAction>) {
        self.selectionSubject = selectionSubject

        self.userInfo = userInfo

        super.init()
    }
}

extension SettingsPersonInfoCellViewModel: Selectable {
    func onSelect() {
        emitAction()
    }
}
