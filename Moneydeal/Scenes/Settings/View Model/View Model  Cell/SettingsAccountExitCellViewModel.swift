//
//  SettingsAccountExitCellViewModel.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import Domain

class SettingsAccountExitCellViewModel: CellViewModel, ActionEmittable {
    var actionType: SettingsAction = .exit
    var selectionSubject: PublishSubject<SettingsAction>

    init(selectionSubject: PublishSubject<SettingsAction>) {
        self.selectionSubject = selectionSubject
        super.init()
    }
}

extension SettingsAccountExitCellViewModel: Selectable {
    func onSelect() {
        emitAction()
    }
}
