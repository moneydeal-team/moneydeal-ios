//
//  SettingsInfoControlCellViewModel.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import Domain

class SettingsInfoControlCellViewModel: CellViewModel {
    var stateSubject: BehaviorSubject<Bool>

    let titleSubject = BehaviorSubject<String?>(value: nil)

    let defaultTitle: String

    let isHiddenSeparator: Bool

    init(defaultTitle: String,
         isHiddenSeparator: Bool = false,
         stateSubject: BehaviorSubject<Bool>) {
        self.defaultTitle = defaultTitle
        self.isHiddenSeparator = isHiddenSeparator
        self.stateSubject = stateSubject

        super.init()
    }
}
