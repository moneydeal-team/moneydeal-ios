//
//  SettingsViewModel.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 04/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import Storage
import Utils
import RxSwift
import Network
import RxCocoa

class SettingsViewModel: ViewModel, ActivityTrackable, ErrorTrackable {
    let dataBase = RealmDAO<UserInfo>()

    let errorTracker = ErrorTracker()
    let activityTracker = ActivityTracker()
    let refreshTracker = ActivityTracker()

    let closeSubject = PublishSubject<Void>()

    private var section: Section<CellViewModel> = Section(items: [])

    private let networkingUseCaseProvider: Domain.NetworkingUseCaseProvider
    private let settingsUseCase: Domain.SettingsUseCase

    let nameSubject = BehaviorSubject<String?>(value: nil)
    let lastNameSubject = BehaviorSubject<String?>(value: nil)

    let actionSelection = PublishSubject<SettingsAction>()

    var userInfo: UserInfo? {
        didSet {
            guard let userInfo = userInfo else { return }

            dataBase.persist(object: userInfo)
        }
    }

    override init() {
        let accessToken = AuthorizationService.shared.userInfo?.accessToken

        networkingUseCaseProvider = UseCaseProvider()
        settingsUseCase = networkingUseCaseProvider.makeSettingsUseCase(token: accessToken)

        super.init()
    }
}

extension SettingsViewModel: ViewModelType {
    struct Input {
        let saveButton: Driver<Void>
        let avatar: Driver<Domain.OutgoingFile>
        let refresh: Driver<()>
    }

    struct Output {
        let actionSelection: Driver<SettingsAction>
        let sections: Driver<[Section<CellViewModel>]>
        let closeSubject: Driver<Void>
    }

    func transform(input: SettingsViewModel.Input) -> SettingsViewModel.Output {
        let sections = BehaviorRelay<[Section<CellViewModel>]>(value: [])

        obtainSettings(refresh: true)
        .trackError(errorTracker)
        .trackActivity(activityTracker)
        .drive(sections)
        .disposed(by: bag)

        input.refresh.asDriver()
        .flatMapLatest({ [unowned self] in
            return self.obtainSettings(refresh: true).trackError(self.errorTracker).trackActivity(self.refreshTracker)
        })
        .drive(sections)
        .disposed(by: bag)

        input.avatar
        .flatMapLatest({ [unowned self] in self.upload(file: $0).asDriverOnErrorJustComplete() })
        .flatMapLatest({ [unowned self] avatar -> Driver<Void> in
            self.userInfo?.avatar = avatar.filename

            return self.update().asDriverOnErrorJustComplete()
        })
        .flatMapLatest({ [unowned self] _ in self.obtainSettings(refresh: true).trackError(self.errorTracker).trackActivity(self.refreshTracker) })
        .drive(sections)
        .disposed(by: bag)

        nameSubject.filterNil().asDriverOnErrorJustComplete()
        .flatMapLatest({ [unowned self] name -> Driver<Void> in
            self.userInfo?.name = name

            return self.update().asDriverOnErrorJustComplete()
        })
        .flatMapLatest({ [unowned self] _ in self.obtainSettings(refresh: true).trackError(self.errorTracker).trackActivity(self.refreshTracker) })
        .drive(sections)
        .disposed(by: bag)

        lastNameSubject.filterNil().asDriverOnErrorJustComplete()
        .flatMapLatest({ [unowned self] lastName -> Driver<Void> in
            self.userInfo?.lastName = lastName

            return self.update().asDriverOnErrorJustComplete()
        })
        .flatMapLatest({ [unowned self] _ in self.obtainSettings(refresh: true).trackError(self.errorTracker).trackActivity(self.refreshTracker) })
        .drive(sections)
        .disposed(by: bag)

        return Output(actionSelection: actionSelection.asDriverOnErrorJustComplete(),
                      sections: sections.asDriverOnErrorJustComplete(), closeSubject: closeSubject.asDriverOnErrorJustComplete())
    }
}

extension SettingsViewModel {
    private func obtainSettings(refresh: Bool) -> Driver<[Section<CellViewModel>]> {
        return settingsUseCase.obtainInfo()
            .trackError(errorTracker)
            .asObservable()
            .map({ [weak self] userInfo in
                guard let vm = self else { return [Section<CellViewModel>(items: [])] }

                vm.userInfo = userInfo

                vm.section = Section(items: [SettingsTopCellViewModel(vm.closeSubject),
                                            EmptyCellViewModel(height: 10.0)])

                vm.section.items.append(contentsOf: vm.makeCellViewModels(for: userInfo))

                return [vm.section]
            })
            .asDriverOnErrorJustComplete()
    }

    private func makeCellViewModels(for userInfo: UserInfo) -> [CellViewModel] {
        var cellViewModels: [CellViewModel] = []

        cellViewModels.append(contentsOf: makePersonInfoCellViewModel(for: userInfo))
        cellViewModels.append(contentsOf: makeProfileCellViewModel(for: userInfo))
        cellViewModels.append(contentsOf: makeApplicationCellViewModel())
        cellViewModels.append(contentsOf: makeAccountCellViewModel(for: userInfo))
        cellViewModels.append(contentsOf: makeSpacerCellViewModel())

        return cellViewModels
    }

    private func makePersonInfoCellViewModel(for userInfo: UserInfo) -> [CellViewModel] {
        let persionInfoViewModel = SettingsPersonInfoCellViewModel(userInfo: userInfo,
                                                                   selectionSubject: actionSelection)

        return [persionInfoViewModel]
    }

    private func makeApplicationCellViewModel() -> [CellViewModel] {
        let sectionTitle = "TTL_SETTINGS_SECTION_APP_TITLE".localized
        let sectionTitleViewModel = RecordPatientTitleCellViewModel(title: sectionTitle)

        let languageInfoTitle = "TTL_SETTINGS_SECTION_APP_LANGUAGE_TITLE".localized
        let languageInfoViewModel = SettingsSelectActionCellViewModel(defaultTitle: languageInfoTitle,
                                                                     isHiddenSeparator: true,
                                                                     subject: PublishSubject<String>(),
                                                                     selectionSubject: actionSelection,
                                                                     actionType: .lang)

        return [sectionTitleViewModel, languageInfoViewModel]
    }

    private func makeProfileCellViewModel(for userInfo: UserInfo) -> [CellViewModel] {
        let sectionTitle = "TTL_SETTINGS_SECTION_PROFILE_TITLE".localized
        let sectionTitleViewModel = RecordPatientTitleCellViewModel(title: sectionTitle)

        let nameTitle = "TTL_SETTINGS_SECTION_PROFILE_NAME_TITLE".localized

        let nameViewModel = SettingsAccountInfoCellViewModel(valueTitle: userInfo.name ?? "",
                                                                 upperTitle: nameTitle,
                                                                 isEnabled: true,
                                                                 isSecure: false,
                                                                 subject: nameSubject)

        let lastNameTitle = "TTL_SETTINGS_SECTION_PROFILE_LASTNAME_TITLE".localized

        let lastNameViewModel = SettingsAccountInfoCellViewModel(valueTitle: userInfo.lastName ?? "",
                                                                 upperTitle: lastNameTitle,
                                                                 isEnabled: true,
                                                                 isSecure: false,
                                                                 subject: lastNameSubject)

        return [sectionTitleViewModel, nameViewModel, lastNameViewModel]
    }

    private func makeAccountCellViewModel(for userInfo: UserInfo) -> [CellViewModel] {
        let sectionTitle = "TTL_SETTINGS_SECTION_ACCOUNT_TITLE".localized
        let sectionTitleViewModel = RecordPatientTitleCellViewModel(title: sectionTitle)

        let exitViewModel = SettingsAccountExitCellViewModel(selectionSubject: actionSelection)

        return [sectionTitleViewModel, exitViewModel]
    }

    private func makeSpacerCellViewModel() -> [CellViewModel] {
        return [EmptyCellViewModel(height: 44.0)]
    }
}

extension SettingsViewModel {
    private func upload(file: OutgoingFile) -> Single<ImageInfo> {
        return settingsUseCase.uploadAvatar(file: file)
            .trackActivity(activityTracker)
            .trackError(errorTracker)
    }

    private func update() -> Single<Void> {
        guard let userInfo = userInfo else { return .error(Error.invalidResponse(message: ""))}

        return settingsUseCase.upadte(info: userInfo)
        .trackActivity(activityTracker)
        .trackError(errorTracker)
    }
}

extension SettingsViewModel {
    func logoutAccount() {
        AuthorizationService.shared.resetCredentials()
    }
}

enum SettingsAction {
    case avatar
    case services
    case lang
    case phone
    case exit
}

import Foundation
import RxSwift

class RecordPatientTitleCellViewModel: CellViewModel {
    let title: String

    init(title: String) {
        self.title = title
    }
}
