//
//  ActivityIndicatorView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 24/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class ActivityIndicatorView: View {
    fileprivate let rootFlexContainer = UIView()
    fileprivate let activityIndicator = UIActivityIndicatorView()

    override func didMoveToWindow() {
        super.didMoveToWindow()
    }

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        rootFlexContainer.pin.all()
        rootFlexContainer.flex.layout()
    }

    func setup() {
        self.superview?.sendSubviewToBack(self)
        isHidden = true

        rootFlexContainer.flex.justifyContent(.center).height(100%).define { flex in
            flex.addItem(activityIndicator).width(36).height(36).alignSelf(.center)
        }

        backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.0)
        activityIndicator.alpha = 0

        addSubview(rootFlexContainer)
    }

    func startAnimating() {
        UIView.animate(withDuration: 0.4) { [weak self] in
            self?.backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.7)
            self?.activityIndicator.alpha = 1
        }

        activityIndicator.startAnimating()
    }

    func stopAnimating(completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            self?.backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.0)
            self?.activityIndicator.alpha = 0
        }, completion: { [completion] _ in
            guard let completion = completion else { return }
            completion()
        })
    }
}

extension ActivityIndicatorView: ActivityIndicatorViewType {

    func show() {
        isHidden = false
        startAnimating()
        superview?.bringSubviewToFront(self)
    }

    func hide() {
        stopAnimating { [weak self] in
            guard let self = self else { return }
            self.superview?.sendSubviewToBack(self)
            self.isHidden = true
        }
    }
}
