//
//  TableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift

class TableViewCell: UITableViewCell {

    let bag = DisposeBag()
    var reuseBag = DisposeBag()

    required init?(coder aDecoder: NSCoder) {
        fatalError("storyboards are incompatible with truth and beauty")
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        setupLayout()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        reuseBag = DisposeBag()

        layoutSubviews()
    }

    func setup() {

    }

    func setupLayout() {

    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        // 1) Set the contentView's width to the specified size parameter
        contentView.pin.width(size.width)

        // 2) Layout contentView flex container
        layout()

        // Return the flex container new size
        return contentView.frame.size
    }

    func layout() {

    }

    deinit {
        #if DEBUG
        print("*** \(type(of: self).className()) deinited ***")
        #endif
    }
}
