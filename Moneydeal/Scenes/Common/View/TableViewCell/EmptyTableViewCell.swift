//
//  EmptyTableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit

class EmptyTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    var viewModel: EmptyCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        contentView.flex.height(vm.height)
        contentView.flex.markDirty()

        layoutSubviews()
    }

    override func setup() {
        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    override func setupLayout() {
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}
