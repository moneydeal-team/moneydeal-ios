//
//  View.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxSwift

class View: UIView {
    let bag = DisposeBag()

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("storyboards are incompatible with truth and beauty")
    }

    deinit {
        #if DEBUG
        print("*** \(type(of: self).className()) deinited ***")
        #endif
    }
}
