//
//  CollectionViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift

class CollectionViewCell: UICollectionViewCell {
    let bag = DisposeBag()
    var reuseBag = DisposeBag()

    required init?(coder aDecoder: NSCoder) {
        fatalError("storyboards are incompatible with truth and beauty")
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        setupLayout()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        reuseBag = DisposeBag()

        layoutSubviews()
    }

    func setup() {

    }

    func setupLayout() {

    }

    func layout() {

    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        contentView.pin.width(size.width)

        // Layout contentView flex container
        layout()

        // Return the flex container new size
        return contentView.frame.size
    }

    deinit {
        #if DEBUG
        print("*** \(type(of: self).className()) deinited ***")
        #endif
    }
}
