//
//  TableViewDataSource.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 30/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxDataSources

class TableViewDataSource<S: SectionModelType>: RxTableViewSectionedReloadDataSource<S> where S.Item: CellViewModel {

    init(cellMap: CellMap) {
        super.init(configureCell: { (_, tableView, indexPath, item) -> UITableViewCell in
            let className = item.className

            guard let cellType = cellMap[className] else {
                return UITableViewCell()
            }

            guard let cell = tableView.reusableCell(withCell: cellType,
                                                    for: indexPath) as? TableViewCell & CellAutoBinder else {
                return UITableViewCell()
            }

            cell.bindViewModel(item)

            return cell
        })
    }

    func indexPath(for item: S.Item) -> IndexPath? {
        var indexPath: IndexPath?

        sectionModels.enumerated().forEach({ index, section in
            guard let row = section.items.firstIndex(of: item) else { return }
            indexPath = IndexPath(row: row, section: index)
        })

        return indexPath
    }
}

extension TableViewDataSource {

    var hasItems: Bool {
        guard let first = sectionModels.first else { return false }
        return !first.items.isEmpty
    }
}
