//
//  CollectionViewDataSource.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 04/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxDataSources

class CollectionViewSectionedDataSource<S: SectionModelType>: RxCollectionViewSectionedReloadDataSource<S> where S.Item: CellViewModel {

    init(cellMap: CellMap) {
        super.init(configureCell: { (_, collectionView, indexPath, item) -> UICollectionViewCell in
            let className = item.className

            guard let cellType = cellMap[className] else {
                return UICollectionViewCell()
            }

            guard let cell = collectionView.reusableCell(withCell: cellType,
                                                         for: indexPath) as? CollectionViewCell & CellAutoBinder else {
                return UICollectionViewCell()
            }

            cell.bindViewModel(item)

            return cell
        })
    }
}

extension CollectionViewSectionedDataSource {

    var hasItems: Bool {
        guard let first = sectionModels.first else { return false }
        return !first.items.isEmpty
    }
}
