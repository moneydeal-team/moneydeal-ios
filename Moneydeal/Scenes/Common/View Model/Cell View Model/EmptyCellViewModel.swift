//
//  EmptyCellViewModel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift

class EmptyCellViewModel: CellViewModel {
    let height: CGFloat

    init(height: CGFloat) {
        self.height = height
    }
}
