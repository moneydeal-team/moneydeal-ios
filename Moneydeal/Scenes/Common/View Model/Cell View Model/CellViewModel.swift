//
//  CellViewModel.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

protocol CellViewModelType {
    var className: String { get }
}

extension CellViewModelType {
    var className: String {
        return String(describing: type(of: self))
    }
}

class CellViewModel: ViewModel, CellViewModelType {
}
