//
//  ViewModel.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxViewModel
import RxSwift

protocol ViewModelType {
    associatedtype Input
    associatedtype Output

    var active: Bool { get set }

    func transform(input: Input) -> Output
}

class ViewModel: RxViewModel {

    let bag = DisposeBag()

    deinit {
        #if DEBUG
        print("*** \(type(of: self).className()) deinited ***")
        #endif
    }
}

extension ViewModel: ClassName {}
