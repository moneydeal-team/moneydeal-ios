//
//  ViewController.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxSwift

class ViewController: UIViewController {

    let bag = DisposeBag()

    init() {
        super.init(nibName: nil, bundle: nil)
        navigationController?.navigationBar.barTintColor = .purple
        navigationController?.navigationBar.tintColor = .white

        navigationItem.backBarButtonItem = UIBarButtonItem(title:
            "TTL_COMMMON_NAVIGATIONBAR_BACK_BUTTON_TITLE".localized,
                                                           style: .plain,
                                                           target: nil,
                                                           action: nil)

        guard let navBarFont = UIFont(name: "GothamPro-Bold", size: 17) else {
            fatalError("Can't find the font")
        }
        navigationItem.backBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: navBarFont],
                                                                 for: .normal)

    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        fatalError("storyboards are incompatible with truth and beauty")
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("storyboards are incompatible with truth and beauty")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let viewBinder = self as? ViewBinder {
            viewBinder.configureView()
        }

        if let errorPresentable = self as? ErrorPresentable {
            errorPresentable.subscribeToErrors()
        }

        if let container = self as? IndicatorContainer {
            container.configureIndicator()
        }

        if let binder = self as? AutoBinder {
            binder.bindViewModel()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let bellsHolder = self as? MDNavigationBarHolder {
            bellsHolder.configureMDNavigationBar()
        }

    }

    override func loadView() {
        if let viewBinder = self as? ViewBinder {
            viewBinder.bindView()
        }
    }

    deinit {
        #if DEBUG
        print("*** \(type(of: self).className()) deinited ***")
        #endif
    }
}

extension ViewController: ClassName {}
