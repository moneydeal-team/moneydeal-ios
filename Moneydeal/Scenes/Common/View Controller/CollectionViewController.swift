//
//  CollectionViewController.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 04/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CollectionViewController: ViewController, CellMapper {

    private (set) var cellMap: CellMap = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        cellMap = configureCellMap()
    }

    func configureCellMap() -> CellMap {
        preconditionFailure("\(#function) should be implemented in subclass")
    }
}
