//
//  TableViewController.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 30/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/// [CellViewModel.className: UIView]
typealias CellMap = [String: UIView.Type]

protocol CellMapper {
    var cellMap: CellMap { get }
    func configureCellMap() -> CellMap
}

class TableViewController: ViewController, CellMapper {

    private (set) var cellMap: CellMap = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        cellMap = configureCellMap()
    }

    func configureCellMap() -> CellMap {
        preconditionFailure("\(#function) should be implemented in subclass")
    }
}
