//
//  Section.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 30/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxDataSources

struct Section<T: CellViewModelType>: SectionModelType {

    var title: String
    var items: [T]

    init(original: Section, items: [T]) {
        self = original
        self.title = original.title
        self.items = items
    }

    init(title: String = "", items: [T]) {
        self.title = title
        self.items = items
    }
}
