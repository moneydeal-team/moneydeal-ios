//
//  AutoBinder.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 24/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

protocol AutoBinder {
    func bindViewModel()
}

protocol CellAutoBinder {
    func bindViewModel<T: ViewModel>(_ vm: T)
}

protocol ViewAutoBinder: CellAutoBinder {}

extension AutoBinder where Self: ViewController & ViewModelHolder,
                            Self: ActivityIndicatorContainer,
                            Self.VM: ActivityTrackable {

    func bindViewModel() {
        ViewModelHolderBinder(holder: self).activityTrackable()
    }
}

extension AutoBinder where Self: ViewController & ViewModelHolder {

    func bindViewModel() {
        //ViewModelHolderBinder(holder: self)
    }

    func bindViewModel<T: ViewModel>(_ vm: T) {
        guard let vm = vm as? Self.VM else { return }

        self.viewModel = vm

        bindViewModel()
    }
}

/// Необходим для осуществления реактивного биндинга.
/// Нельзя вызывать одинаковые биндинги несколько раз. Это приведет к ошибкам.
struct ViewModelHolderBinder<T: ViewModelHolder> {
    weak var holder: T?
}

extension ViewModelHolderBinder where T: CellAutoBinder {

    init(holder: T, viewModel: T.VM) {
        holder.viewModel = viewModel
        self.holder = holder
    }
}

// MARK: - CellAutoBinder

extension CellAutoBinder where Self: TableViewCell & ViewModelHolder {

    func bindViewModel<T: ViewModel>(_ vm: T) {
        guard let vm = vm as? Self.VM else { return }

        self.viewModel = vm
    }
}

// MARK: - Collection CellAutoBinder

extension CellAutoBinder where Self: CollectionViewCell & ViewModelHolder {

    func bindViewModel<T: ViewModel>(_ vm: T) {
        guard let vm = vm as? Self.VM else { return }

        self.viewModel = vm
    }
}
