//
//  ViewScrollable.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 08/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

protocol ViewScrollable: class {

    var scrollView: UIScrollView { get }
}
