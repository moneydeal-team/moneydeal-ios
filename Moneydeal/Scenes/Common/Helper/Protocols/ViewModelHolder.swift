//
//  ViewModelHolder.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

protocol ViewModelHolder: class {
    // swiftlint:disable:next type_name
    associatedtype VM: ViewModel

    var viewModel: VM? { get set }
}
