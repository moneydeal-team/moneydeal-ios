//
//  Selectable.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 01/10/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

protocol Selectable {
    func onSelect()
}
