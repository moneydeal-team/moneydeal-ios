//
//  ViewBinder.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

protocol ViewBinder {
    func bindView()
    func configureView()
}

extension ViewHolder where Self: ViewController & ViewBinder {
    func bindView() {
        view = ViewClass()
    }

    func configureView() {
        DispatchQueue.main.async { [unowned self] in
            self.configure(with: self.mainView)
        }
    }
}
