//
//  BaseViewInput.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

protocol BaseViewInput: View {
    associatedtype Input

    func configure(input: Input)
}

protocol BaseView: View {
    func configure()
}
