//
//  ViewHolder.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit

typealias ViewConfigurator = ViewHolder & ViewBinder

protocol ViewHolder: class {

    associatedtype ViewClass: View

    func configure(with view: ViewClass)
}

extension ViewHolder where Self: ViewController {
    var mainView: ViewClass {
        guard let view = self.view as? ViewClass else {
            fatalError("mainView has not equal class \(ViewClass.className())")
        }

        return view
    }
}
