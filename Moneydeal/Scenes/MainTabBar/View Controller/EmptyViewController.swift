//
//  EmptyViewController.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 02/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class EmptyViewController: ViewController, ViewConfigurator {
    func configure(with view: EmptyView) {
        view.configure(input: .init(visibleText: "testView"))

        print("[DEBUG] Clear auth session")

        let alertVC = UIAlertController.init(title: "Session", message: "It may be cleared", style: .alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        let clearAction = UIAlertAction(title: "CLEAR", style: .destructive) { _ in
            AuthorizationService.shared.userInfo = nil
        }

        alertVC.addAction(cancelAction)
        alertVC.addAction(clearAction)

        if AuthorizationService.shared.userInfo != nil {
            present(alertVC, animated: true)
        }
    }
}
