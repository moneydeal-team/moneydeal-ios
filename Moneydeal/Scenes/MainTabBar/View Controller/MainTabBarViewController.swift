//
//  MainTabBarViewController.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 02/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    var previousIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        viewControllers = [personalExpensesViewController,
                           addExpensesViewController,
                           groupExpensesViewController,
                           goalsViewController]

        self.delegate = self

        view.backgroundColor = .white
        tabBar.backgroundColor = .white

        viewControllers?.forEach({ vc in
            vc.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        })

    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }

    private lazy var personalExpensesViewController: UIViewController = {
        return try? CommonNavigationController.createPresenter()
            .configure({ vc in
                vc.tabBarItem = UITabBarItem(title: nil,
                                             image: UIImage(named: "is_personal_expenses"), tag: 0)
                vc.viewControllers = [PersonalExpensesViewController()]
            }).provideEmbeddedSourceController().parent
    }() ?? UIViewController()

    private lazy var addExpensesViewController: UIViewController = {
        return try? CommonNavigationController.createPresenter()
            .configure({ vc in
                vc.tabBarItem = UITabBarItem(title: nil,
                                             image: UIImage(named: "is_add_expenses"), tag: 1)
                vc.viewControllers = [EmptyViewController()]
            }).provideEmbeddedSourceController().parent
        }() ?? UIViewController()

    private lazy var groupExpensesViewController: UIViewController = {
        return try? CommonNavigationController.createPresenter()
            .configure({ vc in
                vc.tabBarItem = UITabBarItem(title: nil,
                                             image: UIImage(named: "is_group_expenses"), tag: 2)
                vc.viewControllers = [GroupExpensesViewController()]
            }).provideEmbeddedSourceController().parent
    }() ?? UIViewController()

    private lazy var goalsViewController: UIViewController = {
        return try? CommonNavigationController.createPresenter()
            .configure({ vc in
                vc.tabBarItem = UITabBarItem(title: nil,
                                             image: UIImage(named: "is_goals"), tag: 3)
                vc.viewControllers = [GoalsViewController()]
            }).provideEmbeddedSourceController().parent
    }() ?? UIViewController()
}

extension MainTabBarViewController: TransitionPresenter, Transitionable { }

extension MainTabBarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController,
                          shouldSelect viewController: UIViewController) -> Bool {
        previousIndex = selectedIndex

        return true
    }
}
