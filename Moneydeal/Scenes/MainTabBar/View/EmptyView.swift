//
//  EmptyView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 02/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit
import Utils

class EmptyView: View {
    fileprivate let rootFlexContainer = UIView()

    let textLabel = UILabel()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        rootFlexContainer.pin.all()
        rootFlexContainer.flex.layout()
    }

    func setup() {
        setupLabel(with: "TTL_ERROR_UNDEFINED".localized)

        backgroundColor = .white
    }

    func setupLabel(with text: String) {
        textLabel.text = text
    }
}

extension EmptyView: BaseViewInput {
    func configure(input: EmptyView.Input) {
        setupLabel(with: input.visibleText)

        rootFlexContainer.flex.define { flex in
            flex.addItem().marginTop(300).direction(.column).alignItems(.center).define({ flex in
                flex.addItem(textLabel)
            })
        }

        addSubview(rootFlexContainer)
    }

    struct Input {
        let visibleText: String
    }
}
