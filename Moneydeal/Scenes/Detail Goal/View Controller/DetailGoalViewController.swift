//
//  DetailGoalViewController.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 09/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift
import Domain
import RxCocoa

class DetailGoalViewController: CollectionViewController, ViewConfigurator, ViewModelHolder {
    var viewModel: DetailGoalViewModel? {
        didSet {
            configureBindings()
        }
    }

    var dataSource: CollectionViewSectionedDataSource<Section<CellViewModel>>?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "TTL_TABBAR_GOALS_TITLE".localized
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func configure(with view: DetailGoalView) {
        view.configure()
    }

    func configureBindings() {
        guard let vm = viewModel else { return }

        let output = vm.transform(input: .init())

        dataSource = CollectionViewSectionedDataSource(cellMap: cellMap)

        output.sections.drive(mainView.collectionView.rx.items(dataSource: dataSource!)).disposed(by: bag)

        print("[DEBUG] Detail goal for \(vm.goal.id)")
    }

    override func configureCellMap() -> CellMap {
        return [
            DetailGoalHeaderCellViewModel.className(): DetailGoalHeaderCollectionViewCell.self,
            DetailGoalHistoryCellViewModel.className(): DetailGoalHistoryCollectionViewCell.self
        ]
    }
}

extension DetailGoalViewController: TransitionableInput {
    struct Input {
        let goal: Goal
    }

    func onTransit(input: DetailGoalViewController.Input) {
        viewModel = DetailGoalViewModel(for: input.goal)
    }
}

extension DetailGoalViewController: MDNavigationBarHolder {
    var isLargeTitleMode: Bool {
        false
    }

    var state: MDNavigationBarState {
        .hide
    }
}
