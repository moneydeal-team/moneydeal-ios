//
//  DetailGoalViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 09/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import Storage
import Utils
import RxSwift
import Network
import RxCocoa

class DetailGoalViewModel: ViewModel {
    let goal: Goal

    init(for goal: Goal) {
        self.goal = goal

        super.init()
    }

    private var section: Section<CellViewModel> = Section(items: [])
}

extension DetailGoalViewModel: ViewModelType {
    struct Input {
    }

    struct Output {
        let sections: Driver<[Section<CellViewModel>]>
    }

    func transform(input: DetailGoalViewModel.Input) -> DetailGoalViewModel.Output {
        let sections = BehaviorRelay<[Section<CellViewModel>]>(value: [])

        obtainDetailGoals(refresh: true)
        .drive(sections)
        .disposed(by: bag)

        return Output(sections: sections.asDriverOnErrorJustComplete())
    }
}

extension DetailGoalViewModel {
    private func obtainDetailGoals(refresh: Bool) -> Driver<[Section<CellViewModel>]> {
        section = Section(items: makeCellViewModels())

        return Driver<[Section<CellViewModel>]>.just([section])
    }

    private func makeCellViewModels() -> [CellViewModel] {
        var cellViewModels: [CellViewModel] = []

        cellViewModels.append(contentsOf: makeHeaderDetailGoalCellViewModel())
        cellViewModels.append(contentsOf: makeDetailGoalHistoryCellViewModel())

        return cellViewModels
    }

    func makeHeaderDetailGoalCellViewModel() -> [CellViewModel] {
        return [DetailGoalHeaderCellViewModel(for: goal)]
    }

    func makeDetailGoalHistoryCellViewModel() -> [CellViewModel] {
        return [DetailGoalHistoryCellViewModel()]
    }
}
