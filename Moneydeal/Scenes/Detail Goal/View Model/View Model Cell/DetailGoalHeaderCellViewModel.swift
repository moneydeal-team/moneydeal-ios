//
//  DetailGoalHeaderCellViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 09/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain

class DetailGoalHeaderCellViewModel: CellViewModel {
    let goal: Goal

    var imageURL: URL? {
        let defaultURL = Constants.Default.goalImageURL

        return URL(string: (goal.imageURL.isEmpty ? defaultURL : (Constants.Dispather.imageServer+goal.imageURL)))
    }

    init(for goal: Goal) {
        self.goal = goal
    }
}
