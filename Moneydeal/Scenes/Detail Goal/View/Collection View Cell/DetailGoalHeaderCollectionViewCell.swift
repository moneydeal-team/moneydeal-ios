//
//  DetailGoalHeaderCollectionViewCell.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 09/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import FlexLayout

class DetailGoalHeaderCollectionViewCell: CollectionViewCell, CellAutoBinder, ViewModelHolder {
    let goalNameLabel = UILabel()
    let yearLabel = UILabel()
    let priceLabel = UILabel()
    let imageView = UIImageView()

    let gradient = CAGradientLayer()
    let gradientPlaceholderView = UIView()
    let historyLabel = UILabel()

    var viewModel: DetailGoalHeaderCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
        setupGradientLayer()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        goalNameLabel.text = vm.goal.name

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.maximumFractionDigits = 0
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current

        let amountString = currencyFormatter.string(from: NSNumber(value: vm.goal.amount)) ?? ""
        let balanceString = currencyFormatter.string(from: NSNumber(value: vm.goal.balance)) ?? ""

        priceLabel.text = String(format: "TTL_GOAL_CURRENT_AMOUNT".localized, balanceString,
                                                                              amountString)

        setupImageView(from: vm.imageURL)
    }

    override func setup() {
        setupCellStyles()

        backgroundColor = .white
    }

    override func setupLayout() {
        contentView.flex.addItem().define { flex in
            flex.addItem().define { flex in
                layoutImageView(for: flex).justifyContent(.end).define { flex in
                    flex.addItem(gradientPlaceholderView).position(.absolute).all(0)
                    flex.addItem().marginLeft(26.0).marginRight(26.0).marginBottom(18.0).define { flex in
                        layoutGoalNameLabel(for: flex)
                        layoutYearLabel(for: flex)
                        layoutPriceLabel(for: flex)
                    }
                }
            }
            flex.addItem().define { flex in
                layoutHistoryLabel(for: flex)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension DetailGoalHeaderCollectionViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()
        setupGoalNameLabel()
        setupYearLabel()
        setupPriceLabel()
        setupGradientPlaceholderView()
        setupHistoryLabel()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width
        let headerHeight: CGFloat = 260.0

        contentView.flex.minWidth(headerWidth)
        contentView.flex.minHeight(headerHeight)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }

    /// Setup goal name label
    func setupGoalNameLabel() {
        goalNameLabel.textColor = .white

        goalNameLabel.font = UIFont(name: "GothamPro-Bold", size: 24)

        goalNameLabel.text = "Audi A4"

        goalNameLabel.numberOfLines = 0
        goalNameLabel.textAlignment = .left
    }

    /// Setup year label
    func setupYearLabel() {
        yearLabel.textColor = .white

        yearLabel.font = UIFont(name: "GothamPro-Medium", size: 11)

        yearLabel.text = "2022"

        yearLabel.numberOfLines = 0
        yearLabel.textAlignment = .left
    }

    /// Setup price label
    func setupPriceLabel() {
        priceLabel.textColor = .white

        priceLabel.font = UIFont(name: "GothamPro-Medium", size: 10)

        priceLabel.text = "28 000$"

        priceLabel.numberOfLines = 0
        priceLabel.textAlignment = .left
    }

    /// Setup image view
    func setupImageView(from url: URL?) {
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true

        guard let url = url else { return }

        imageView.kf.setImage(with: url, options: [.backgroundDecode], auth: true)
    }

    /// Setup Gradient layer
    func setupGradientLayer() {
        gradient.frame = gradientPlaceholderView.bounds
    }

    /// Setup GradientPlaceholderView
    func setupGradientPlaceholderView() {
        gradient.colors = [
          UIColor(red: 0.38, green: 0.243, blue: 0.918, alpha: 0.75).cgColor,
          UIColor(red: 0.353, green: 0.851, blue: 0.702, alpha: 0.48).cgColor
        ]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 0.25, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.75, y: 0.5)
        gradientPlaceholderView.layer.addSublayer(gradient)
        gradientPlaceholderView.backgroundColor = .clear
        gradientPlaceholderView.alpha = 1.0
    }

    /// Setup history label
    func setupHistoryLabel() {
        historyLabel.textColor = UIColor(red: 0.231, green: 0.255, blue: 0.294, alpha: 1)

        historyLabel.font = UIFont(name: "GothamPro-Medium", size: 16)

        historyLabel.text = "TTL_HISTORY_PAYMENTS_TITLE".localized

        historyLabel.numberOfLines = 0
        historyLabel.textAlignment = .left
    }
}

extension DetailGoalHeaderCollectionViewCell {
    /// Layout for image view
    func layoutImageView(for flex: Flex) -> Flex {
        let imageHeight: CGFloat = 233.0

        return flex.addItem(imageView).height(imageHeight).width(100%)
    }

    /// Layout for goal name label
    func layoutGoalNameLabel(for flex: Flex) {
        flex.addItem(goalNameLabel)
    }

    /// Layout for year label
    func layoutYearLabel(for flex: Flex) {
        flex.addItem(yearLabel).marginTop(9.0)
    }

    /// Layout for price label
    func layoutPriceLabel(for flex: Flex) {
        flex.addItem(priceLabel).marginTop(9.0)
    }

    /// Layout for history label
    func layoutHistoryLabel(for flex: Flex) {
        flex.addItem(historyLabel).marginLeft(21.0).marginTop(18.0)
    }
}
