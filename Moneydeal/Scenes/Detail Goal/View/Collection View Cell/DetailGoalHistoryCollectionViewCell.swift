//
//  DetailGoalHistoryCollectionViewCell.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 09/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import FlexLayout

class DetailGoalHistoryCollectionViewCell: CollectionViewCell, CellAutoBinder, ViewModelHolder {
    let imageView = UIImageView()
    let totalAmountLabel = UILabel()
    let sumLabel = UILabel()
    let date = UILabel()
    let nameLabel = UILabel()

    var viewModel: DetailGoalHistoryCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
    }

    override func setup() {
        setupCellStyles()
        setupImageView()
        setupImageView()
        setupNameLabel()
        setupDateLabel()
        setupSumLabel()
        setupTotalAmountLabel()

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.addItem().direction(.row).marginTop(18.0).define { flex in
            flex.addItem().define { flex in
                layoutImageView(for: flex)
            }
            flex.addItem().marginLeft(27.0).define { flex in
                layoutNameLabel(for: flex)
                layoutDateLabel(for: flex)
            }
            flex.addItem().position(.absolute).right(18.0).define { flex in
                layoutSumLabel(for: flex)
                layoutTotalAmountLabel(for: flex)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension DetailGoalHistoryCollectionViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width

        contentView.flex.minWidth(headerWidth)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }

    /// Setup styles for image view
    func setupImageView() {
        imageView.image = UIImage(named: "ic_expenses_apple")
    }

    /// Setup styles for name label
    func setupNameLabel() {
        nameLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        nameLabel.font = UIFont(name: "GothamPro-Medium", size: 14)

        nameLabel.text = "ДОСТОЖРИСТО БАНК"

        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .left
    }

    /// Setup styles for date label
    func setupDateLabel() {
        date.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)

        date.font = UIFont(name: "GothamPro", size: 12)

        date.text = "21.06.19"

        date.numberOfLines = 0
        date.textAlignment = .left
    }

    /// Setup styles for sum label
    func setupSumLabel() {
        sumLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        sumLabel.font = UIFont(name: "GothamPro-Medium", size: 14)

        sumLabel.text = "+100$"

        sumLabel.numberOfLines = 0
        sumLabel.textAlignment = .right
    }

    /// Setup styles fot total amount label
    func setupTotalAmountLabel() {
        totalAmountLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)

        totalAmountLabel.font = UIFont(name: "GothamPro", size: 12)

        totalAmountLabel.text = "1 346$"

        totalAmountLabel.numberOfLines = 0
        totalAmountLabel.textAlignment = .right
    }
}

extension DetailGoalHistoryCollectionViewCell {
    /// Layout for image view
    func layoutImageView(for flex: Flex) {
        let sizes: CGSize = CGSize(width: 20.0, height: 20.0)
        flex.addItem(imageView).marginLeft(36.0).marginTop(8).width(sizes.width).height(sizes.height)
    }

    /// Layout for name label
    func layoutNameLabel(for flex: Flex) {
        flex.addItem(nameLabel)
    }

    /// Layout for date label
    func layoutDateLabel(for flex: Flex) {
        flex.addItem(date).marginTop(15.0)
    }

    /// Layout for sum label
    func layoutSumLabel(for flex: Flex) {
        flex.addItem(sumLabel)
    }

    /// Layout for total amount label
    func layoutTotalAmountLabel(for flex: Flex) {
        flex.addItem(totalAmountLabel).marginTop(15.0)
    }
}
