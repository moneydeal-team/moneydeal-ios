//
//  AddGoalViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 12/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import Storage
import Utils
import RxSwift
import Network
import RxCocoa

class AddGoalViewModel: ViewModel, ActivityTrackable, ErrorTrackable {

    struct GoalDataInput {
        let title: String?
        let balance: String?
        let amount: String?
        let finishDate: String?
    }

    enum AdGoalResult {
        case success(goal: Goal)
        case error(_ error: AddGoalError)
    }

    enum AddGoalError: Swift.Error {
        case emptyData
        case someError
    }

    private let result = PublishSubject<AdGoalResult>()

    private var section: Section<CellViewModel> = Section(items: [])

    let errorTracker = ErrorTracker()
    let activityTracker = ActivityTracker()

    let closeSubject = PublishSubject<Void>()

    let titleSubject = BehaviorSubject<String?>(value: nil)
    let balanceSubject = BehaviorSubject<String?>(value: nil)
    let amountSubject = BehaviorSubject<String?>(value: nil)
    let finishDateSubject = BehaviorSubject<String?>(value: nil)
    let photoSubject = BehaviorSubject<String?>(value: nil)

    let createSubject = PublishSubject<Void>()

    let dateProxySubject = BehaviorSubject<Date?>(value: nil)
    let datePickerEvent = PublishSubject<Void>()

    let photoSelectSubject = PublishSubject<Void>()

    let photoDeleteSubject = PublishSubject<Void>()

    var photoName: String?
    var photo: UIImage?

    private let networkingUseCaseProvider: Domain.NetworkingUseCaseProvider
    private let goalUseCase: Domain.GoalUseCase

    override init() {
        let accessToken = AuthorizationService.shared.userInfo?.accessToken

        networkingUseCaseProvider = UseCaseProvider()
        goalUseCase = networkingUseCaseProvider.makeGoalUseCase(token: accessToken)

        super.init()
    }
}

extension AddGoalViewModel: ViewModelType {
    struct Input {
        let photo: Driver<Domain.OutgoingFile>
    }

    struct Output {
        let sections: Driver<[Section<CellViewModel>]>
        let closeSubject: Driver<Void>
        let datePickerEvent: Driver<Void>
        let photoSelectEvent: Driver<Void>

        let result: Driver<AdGoalResult>
    }

    func transform(input: AddGoalViewModel.Input) -> AddGoalViewModel.Output {
        let sections = BehaviorRelay<[Section<CellViewModel>]>(value: [])

        obtainAddGoal(refresh: true)
        .drive(sections)
        .disposed(by: bag)

        let data = Driver.combineLatest(titleSubject.asDriverOnErrorJustComplete(),
                                        balanceSubject.asDriverOnErrorJustComplete(),
                                        amountSubject.asDriverOnErrorJustComplete(),
                                        finishDateSubject.asDriverOnErrorJustComplete())
        .map(GoalDataInput.init)

        createSubject.asDriverOnErrorJustComplete()
        .withLatestFrom(data)
        .flatMap({ [unowned self] data in
            return self.getResult(for: data)
        })
        .drive(result)
        .disposed(by: bag)

        dateProxySubject.filterNil()
        .asDriverOnErrorJustComplete()
        .map({ $0.asString(format: Utils.Constants.humanLikeDateFormat) })
        .drive(finishDateSubject)
        .disposed(by: bag)

        input.photo.drive(onNext: { [weak self] photo in
            guard let self = self else { return }

            self.photo = UIImage(data: photo.data)
        }).disposed(by: bag)

        input.photo.flatMap({ [unowned self] _ in
            return self.obtainAddGoal(refresh: true)
        })
        .drive(sections)
        .disposed(by: bag)

        photoDeleteSubject.asDriverOnErrorJustComplete()
        .flatMap({ [unowned self] _ in
            self.photoName = nil
            self.photo = nil
             return self.obtainAddGoal(refresh: true)
         })
         .drive(sections)
         .disposed(by: bag)

        input.photo
        .flatMapLatest({ [unowned self] in self.upload(file: $0)
        .asDriverOnErrorJustComplete() })
        .asDriver()
        .drive(onNext: { imageInfo in
            self.photoName = imageInfo.filename
        }).disposed(by: bag)

        return Output(sections: sections.asDriverOnErrorJustComplete(),
                      closeSubject: closeSubject.asDriverOnErrorJustComplete(),
                      datePickerEvent: datePickerEvent.asDriverOnErrorJustComplete(),
                      photoSelectEvent: photoSelectSubject.asDriverOnErrorJustComplete(),
                      result: result.asDriverOnErrorJustComplete())
    }
}

extension AddGoalViewModel {
    private func getResult(for data: GoalDataInput) -> Driver<AdGoalResult> {
        return tryToPublish(with: data)
            .trackError(self.errorTracker)
            .asDriverOnErrorJustComplete()
            .flatMapLatest({ [unowned self] result in
                switch result {
                case .success(let goal):
                    return self.publish(goal: goal).asDriverOnErrorJustComplete()
                case .error:
                    return Driver.just(result)
                }
            })
    }

    private func tryToPublish(with data: GoalDataInput) -> Observable<AdGoalResult> {
        // check error
        guard let title = data.title, title.isNotEmpty else {
            return Observable.error(Error.appError(message: "TTL_ADDG_GOAL_NAME_ERROR".localized))
        }

        guard let amount = stringToInt(for: data.amount) else {
            return Observable.error(Error.appError(message: "TTL_ADDG_GOAL_AMOUNT_ERROR".localized))
        }

        guard let balance = stringToInt(for: data.balance) else {
            return Observable.error(Error.appError(message: "TTL_ADDG_GOAL_BALANCE_ERROR".localized))
        }

        guard let date = try? self.dateProxySubject.value() else {
            return Observable.error(Error.appError(message: "TTL_ADDG_GOAL_DATE_ERROR".localized))
        }

        let serverDate = date.asString(format: Utils.Constants.defaultDateFormat)

        let goal = Goal(id: "",
                        name: title,
                        endDate: serverDate,
                        balance: balance,
                        amount: amount,
                        imageURL: self.photoName ?? "")

        return Observable.just(.success(goal: goal))
    }

    private func publish(goal: Goal) -> Observable<AdGoalResult> {
        return goalUseCase.create(goal: goal)
            .trackError(errorTracker)
            .trackActivity(activityTracker)
            .catchError({ _ in
                .error(Error.appError(message: "TTL_ERROR_TITLE".localized))
            })
            .map({ AdGoalResult.success(goal: goal) })
    }
}

extension AddGoalViewModel {
    private func upload(file: OutgoingFile) -> Single<ImageInfo> {
        return goalUseCase.upload(file: file)
            .trackActivity(activityTracker)
            .trackError(errorTracker)
    }
}

extension AddGoalViewModel {
    func stringToInt(for string: String?) -> UInt64? {
        guard let string = string else { return nil }
        return UInt64(string)
    }
}

extension AddGoalViewModel {
    private func obtainAddGoal(refresh: Bool) -> Driver<[Section<CellViewModel>]> {
        section = Section(items: makeCellViewModels())

        return Driver<[Section<CellViewModel>]>.just([section])
    }

    private func makeCellViewModels() -> [CellViewModel] {
        var cellViewModels: [CellViewModel] = []

        cellViewModels.append(contentsOf: makeTopCellViewModel())
        cellViewModels.append(contentsOf: makeFieldCellViewModels())
        if let photo = self.photo {
            cellViewModels.append(contentsOf: makePhotoControllCellViewModel(for: photo))
        } else {
            cellViewModels.append(contentsOf: makeAddPhotoCellViewModel())
        }
        cellViewModels.append(contentsOf: makeCreateCellViewModel())

        return cellViewModels
    }

    func makeTopCellViewModel() -> [CellViewModel] {
        return [AddGoalTopCellViewModel(closeSubject), EmptyCellViewModel(height: 20.0)]
    }

    func makeFieldCellViewModels() -> [CellViewModel] {
        let titleVM = GoalFieldCellViewModel(type: .text,
                                                title: "TTL_ADD_GOAL_NAME_PLACEHOLDER".localized,
                                                valueSubject: titleSubject)

        let amountVM = GoalFieldCellViewModel(type: .number,
                                                title: "TTL_ADD_GOAL_AMOUNT_PLACEHOLDER".localized,
                                                valueSubject: amountSubject)

        let balanceVM = GoalFieldCellViewModel(type: .number,
                                                title: "TTL_ADD_GOAL_BALANCE_PLACEHOLDER".localized,
                                                valueSubject: balanceSubject)

        let dateVM = GoalFieldCellViewModel(type: .text,
                                                title: "TTL_ADD_GOAL_DATE_PLACEHOLDER".localized,
                                                valueSubject: finishDateSubject,
                                                selectionSubject: datePickerEvent)

        return [titleVM, EmptyCellViewModel(height: 7.0),
                amountVM, EmptyCellViewModel(height: 7.0),
                balanceVM, EmptyCellViewModel(height: 7.0),
                dateVM]

    }

    func makeAddPhotoCellViewModel() -> [CellViewModel] {
        return [AddGoalAddPhotoCellViewModel(photoSelectSubject)]
    }

    func makeCreateCellViewModel() -> [CellViewModel] {
        return [AddGoalCreateCellViewModel(createSubject)]
    }

    func makePhotoControllCellViewModel(for photo: UIImage) -> [CellViewModel] {
        return [AddGoalImageCellViewModel(for: photo, photoDeleteSubject)]
    }
}
