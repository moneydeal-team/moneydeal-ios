//
//  AddGoalTopCellViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 12/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AddGoalTopCellViewModel: CellViewModel {
    let closeSubject: PublishSubject<Void>

    init(_ closeSubject: PublishSubject<Void>) {
        self.closeSubject = closeSubject

        super.init()
    }
}
