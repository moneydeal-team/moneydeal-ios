//
//  GoalFieldCellViewModel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum GoalFieldType {
    case text
    case number
    case date
}

class GoalFieldCellViewModel: CellViewModel {
    let type: GoalFieldType

    let title: String

    let valueSubject: BehaviorSubject<String?>

    let selectionSubject: PublishSubject<Void>?

    init(type: GoalFieldType,
         title: String,
         valueSubject: BehaviorSubject<String?>,
         selectionSubject: PublishSubject<Void>? = nil) {
        self.type = type

        self.title = title

        self.valueSubject = valueSubject

        self.selectionSubject = selectionSubject

        super.init()
    }
}

extension GoalFieldCellViewModel: Selectable {
    func onSelect() {
        selectionSubject?.onNext(())
    }
}
