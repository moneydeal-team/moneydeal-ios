//
//  AddGoalAddPhotoCellViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AddGoalAddPhotoCellViewModel: CellViewModel {
    let selectSubject: PublishSubject<Void>

    init(_ selectSubject: PublishSubject<Void>) {
        self.selectSubject = selectSubject

        super.init()
    }

}

extension AddGoalAddPhotoCellViewModel: Selectable {
    func onSelect() {
        selectSubject.onNext(())
    }
}
