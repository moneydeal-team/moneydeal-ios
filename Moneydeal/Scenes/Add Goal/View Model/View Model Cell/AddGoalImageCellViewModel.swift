//
//  AddGoalImageCellViewModel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import RxSwift
import RxCocoa
import Foundation

class AddGoalImageCellViewModel: CellViewModel {
    let deleteSubject: PublishSubject<Void>

    let image: UIImage

    init(for image: UIImage, _ deleteSubject: PublishSubject<Void>) {
        self.deleteSubject = deleteSubject
        self.image = image

        super.init()
    }
}
