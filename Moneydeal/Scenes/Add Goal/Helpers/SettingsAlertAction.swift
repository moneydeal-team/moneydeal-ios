//
//  SettingsAlertAction.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class SettingsAlertAction: AlertAction {

    override var title: String {
        return "TTL_PERMISSION_DENIED_SETTINGS_BUTTON".localized
    }

    override init(completion: @escaping () -> Void = defaultBlockAction) {
        super.init(completion: completion)
    }

    class func defaultBlockAction() {
        guard let URL = URL(string: UIApplication.openSettingsURLString) else { return }
        UIApplication.shared.open(URL, options: [:], completionHandler: nil)
    }
}
