//
//  SettingsLibraryAction.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class SettingsCameraAction: AlertAction {

    override var title: String {
        return "TTL_PROFILE_VIEW_CAMERA".localized
    }

    override var style: UIAlertAction.Style {
        return .default
    }
}

class SettingsLibraryAction: AlertAction {

    override var title: String {
        return "TTL_PROFILE_VIEW_PHOTO_LIBRARY".localized
    }

    override var style: UIAlertAction.Style {
        return .default
    }
}
