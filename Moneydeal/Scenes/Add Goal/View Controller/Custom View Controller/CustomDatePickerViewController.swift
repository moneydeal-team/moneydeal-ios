//
//  CustomDatePickerViewController.swift
//  Moneydeal
//
//  Created by Arsenij Zorin on 14/01/2020.
//  Copyright © 2020 Arsenij Zorin. All rights reserved.
//

import RxSwift
import Foundation

class CustomDatePickerViewController: ViewController, ViewConfigurator {

    func configure(with view: CustomDatePickerView) {
        view.configure()

        Observable.merge(view.doneButton.rx.tap.throttleTapInMainScheduler(),
                         view.cancelButton.rx.tap.throttleTapInMainScheduler())
        .subscribe(onNext: { [weak self] _ in
            self?.dismiss(animated: true)
        }).disposed(by: bag)
    }
}

extension CustomDatePickerViewController: TransitionableInput {
    struct Input {
        let dateSubject: PublishSubject<Date>
    }

    func onTransit(input: CustomDatePickerViewController.Input) {
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve

        mainView.datePicker.rx.date.skip(1).bind(to: input.dateSubject).disposed(by: bag)
    }
}
