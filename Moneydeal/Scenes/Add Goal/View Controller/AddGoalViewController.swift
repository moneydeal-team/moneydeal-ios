//
//  AddGoalViewController.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 12/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import Kingfisher
import Utils
import MobileCoreServices
import Photos

enum AddGoalRouter {
    static let toDatePicker = PresentPopupTransition(destination: CustomDatePickerViewController.self)
}

class AddGoalViewController: TableViewController, ViewModelHolder, ViewConfigurator {
    var viewModel: AddGoalViewModel? = AddGoalViewModel()

    var indicatorView: ActivityIndicatorView! = ActivityIndicatorView()

    var dataSource: TableViewDataSource<Section<CellViewModel>>?

    lazy var picker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.mediaTypes = [String(kUTTypeImage)]
        return picker
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    func configure(with view: AddGoalView) {
        view.configure()

        adjustBottomWithKeyboard(for: view.scrollView)

        dataSource = TableViewDataSource(cellMap: cellMap)

        guard let vm = viewModel else { return }

        let photo = picker.fileSubject.asDriverOnErrorJustComplete()

        let output = vm.transform(input: .init(photo: photo))

        output.sections.drive(view.tableView.rx.items(dataSource: dataSource!)).disposed(by: bag)

        output.closeSubject.drive(onNext: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)

            self?.dismiss(animated: true, completion: nil)
        })
        .disposed(by: bag)

        output.result.drive(onNext: { [weak self] result in
            guard case AddGoalViewModel.AdGoalResult.success(let goal) = result else { return }

            GoalsViewModel.globalRefresh.onNext(())

            self?.navigationController?.popViewController(animated: true)
            self?.dismiss(animated: true, completion: nil)
        }).disposed(by: bag)

        let proxyDate = PublishSubject<Date>()
        proxyDate.bind(to: vm.dateProxySubject).disposed(by: bag)

        output.datePickerEvent.asDriver().drive(onNext: { [weak self] _ in
            self?.dismissKeyboard()
        }).disposed(by: bag)

        output.datePickerEvent
        .map({ _ in CustomDatePickerViewController.Input(dateSubject: proxyDate) })
        .asTransitionable()
        .transit(to: AddGoalRouter.toDatePicker)
        .disposed(by: bag)

        output.photoSelectEvent
        .drive(onNext: { [weak self] _ in
            guard let self = self else { return }

            let camera = SettingsCameraAction { [unowned self] in
                self.onCameraTap()
            }
            let library = SettingsLibraryAction { [unowned self] in
                self.onGalleryTap()
            }
            let cancel = CancelAlertAction()

            let sheet = AlertController(style: .actionSheet).withActions([camera, library, cancel])
            self.present(sheet, animated: true, completion: nil)

        }).disposed(by: bag)

        view.tableView.rx.itemSelected
        .asDriverOnErrorJustComplete()
        .drive(onNext: { [dataSource] index in
            guard let selectable = try? dataSource?.model(at: index) as? Selectable else {
                return
            }

            Vibration.selection.vibrate()
            selectable.onSelect()
        })
        .disposed(by: bag)
    }

    override func configureCellMap() -> CellMap {
        return [
            AddGoalTopCellViewModel.className(): AddGoalTopTableViewCell.self,
            AddGoalAddPhotoCellViewModel.className(): AddGoalAddPhotoTableViewCell.self,
            AddGoalCreateCellViewModel.className(): AddGoalCreateTableViewCell.self,
            GoalFieldCellViewModel.className(): GoalFieldTableViewCell.self,
            EmptyCellViewModel.className(): EmptyTableViewCell.self,
            AddGoalImageCellViewModel.className(): AddGoalImageTableViewCell.self
        ]
    }
}

extension AddGoalViewController: AutoBinder, KeyboardAdjustable {}

extension AddGoalViewController: Transitionable {}

extension AddGoalViewController: MDNavigationBarHolder {
    var isLargeTitleMode: Bool {
        false
    }

    var state: MDNavigationBarState {
        .hide
    }
}

extension AddGoalViewController: ErrorPresentable {}

extension AddGoalViewController {
    private func onCameraTap() {
        let ok = OKAlertAction()
        let settings = SettingsAlertAction()

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            AVCaptureDevice.rx.status(for: AVMediaType.video)
                .map({ [unowned self] status -> UIViewController in
                    if status == .authorized {
                        self.picker.sourceType = .camera
                        return self.picker
                    } else {
                        return AlertController(title: "TTL_PERMISSION_DENIED_TITLE".localized,
                                               message: "TTL_PERMISSION_DENIED_CAMERA_MESSAGE".localized,
                                               style: .alert).withActions([ok, settings])
                    }
                })
                .subscribe(onSuccess: { [weak self] vc in
                    self?.present(vc, animated: true, completion: nil)
                })
                .disposed(by: bag)
        } else {
            let alert = AlertController(title: "TTL_ERROR_TITLE".localized,
                                        message: "TTL_PROFILE_VIEW_CAMERA_UNAVAILABLE".localized,
                                        style: .alert)
                .withActions([OKAlertAction()])

            present(alert, animated: true, completion: nil)
        }
    }

    private func onGalleryTap() {
        let ok = OKAlertAction()
        let settings = SettingsAlertAction()

        PHPhotoLibrary.shared().rx.status
            .map({ [unowned self] status -> UIViewController in
                if status == .authorized {
                    self.picker.sourceType = .photoLibrary
                    return self.picker
                } else {
                    return AlertController(title: "TTL_PERMISSION_DENIED_TITLE".localized,
                                           message: "TTL_PERMISSION_DENIED_PHOTO_MESSAGE".localized,
                                           style: .alert).withActions([ok, settings])
                }
            })
            .subscribe(onSuccess: { [weak self] vc in
                self?.present(vc, animated: true, completion: {
                    // Device specific bug fix
                    self?.picker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
                })
            })
            .disposed(by: bag)
    }
}

extension AddGoalViewController: ActivityIndicatorContainer {}
