//
//  AddGoalImageTableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class AddGoalImageTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    let customImageView = UIImageView()

    let closeButton = UIButton()

    var viewModel: AddGoalImageCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        customImageView.image = vm.image

        closeButton.rx.tap.throttleTapInMainScheduler()
         .asDriverOnErrorJustComplete()
         .drive(vm.deleteSubject)
         .disposed(by: reuseBag)
    }

    override func setup() {
        setupCellStyles()
        setupImageView()
        setupCloseButton()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.marginHorizontal(14.0).define { flex in
            layoutImageView(for: flex)
            layoutCloseButton(for: flex)
        }
    }
}

extension AddGoalImageTableViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()

        backgroundColor = .white
    }

    /// Setup styles for close button
    func setupCloseButton() {
        closeButton.setImage(UIImage(named: "Close"), for: .normal)
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width

        contentView.flex.minWidth(headerWidth)
        //contentView.flex.minHeight(100)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }

    /// Setup styles fot button
    func setupImageView() {
        customImageView.contentMode = .scaleAspectFill

        customImageView.clipsToBounds = true
        customImageView.layer.masksToBounds = true
        customImageView.layer.cornerRadius = 14.0
    }
}

extension AddGoalImageTableViewCell {
    /// Layout for image view
    func layoutImageView(for flex: Flex) {
        flex.addItem(customImageView).width(UIScreen.main.bounds.width-28).height(133.0).marginVertical(23.0)
    }

    /// Layout fot delete button
    func layoutCloseButton(for flex: Flex) {
        let backgroundView = UIView()

        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.85)
        backgroundView.layer.cornerRadius = 32/2
        backgroundView.clipsToBounds = true

        flex.addItem(backgroundView).position(.absolute).right(35).top(30).size(32.0).define { flex in
            flex.addItem(closeButton).size(32.0)
        }
    }
}
