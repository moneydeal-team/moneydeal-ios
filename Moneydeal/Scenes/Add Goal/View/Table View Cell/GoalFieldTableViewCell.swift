//
//  GoalFieldTableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class GoalFieldTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    let textField = PlaceholderTextField()

    var viewModel: GoalFieldCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        textField.placeholder = vm.title

        switch vm.type {
        case .number: textField.keyboardType = .numberPad
        case .date: textField.keyboardType = .default
        case .text: textField.keyboardType = .default
        }

        textField.rx.value.filterNil()
        .asDriverOnErrorJustComplete()
        .drive(vm.valueSubject)
        .disposed(by: reuseBag)

        vm.valueSubject
        .asDriverOnErrorJustComplete()
        .drive(textField.rx.text)
        .disposed(by: reuseBag)

        if let selectionSubject = vm.selectionSubject {
            textField.rx.controlEvent([.editingDidBegin])
            .asDriverOnErrorJustComplete()
            .drive(selectionSubject)
            .disposed(by: reuseBag)
        }
    }

    override func setup() {
        setupCellStyles()
        setupTextField()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    /// Setup styles for phone text field
    func setupTextField() {
        textField.font = UIFont(name: "GothamPro", size: 17)

        textField.textColor = UIColor(red: 0.09, green: 0.114, blue: 0.2, alpha: 1)
        textField.placeholderColor = UIColor(red: 0.651, green: 0.667, blue: 0.706, alpha: 1)

        textField.activeBorderColor = UIColor(red: 0.38, green: 0.243, blue: 0.918, alpha: 1)
        textField.inactiveBorderColor = UIColor(red: 0.651, green: 0.667, blue: 0.706, alpha: 1)

        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none

        textField.backgroundColor = .white

        textField.cornerRadius = 6.0
        textField.borderSize = 1.0
    }

    override func setupLayout() {
        contentView.flex.define { flex in
            layoutButton(for: flex)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension GoalFieldTableViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()
        setupTextField()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width

        contentView.flex.minWidth(headerWidth)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }
}

extension GoalFieldTableViewCell {
    /// Layout for button
    func layoutButton(for flex: Flex) {
        flex.addItem(textField).height(60.0).marginHorizontal(14.0)
    }
}
