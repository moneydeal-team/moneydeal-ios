//
//  AddGoalCreateTableViewCell.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class AddGoalCreateTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    let button = UIButton()

    var viewModel: AddGoalCreateCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        button.rx.tap.throttleTapInMainScheduler()
        .asDriverOnErrorJustComplete()
        .drive(vm.selectSubject)
        .disposed(by: reuseBag)
    }

    override func setup() {
        setupCellStyles()
        setupButton()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.define { flex in
            layoutButton(for: flex)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension AddGoalCreateTableViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()
        setupButton()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width

        contentView.flex.minWidth(headerWidth)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }

    /// Setup styles for button
    func setupButton() {
        button.setTitle("TTL_ADD_GOAL_DONE_BUTTON".localized, for: .normal)
        button.titleLabel?.font = UIFont(name: "GothamPro-Bold", size: 16.0)

        button.layer.cornerRadius = 6.0
        button.backgroundColor = UIColor(red: 0.353, green: 0.245, blue: 0.775, alpha: 1)
        button.tintColor = .white
        button.setTitleColor(.lightGray, for: .highlighted)

        // Setup shadow
        button.layer.shadowColor = UIColor(red: 0.38, green: 0.243, blue: 0.918, alpha: 0.32).cgColor
        button.layer.shadowOpacity = 1
        button.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        button.layer.shadowRadius = 8
        button.layer.shouldRasterize = true
        button.layer.rasterizationScale = UIScreen.main.scale
    }
}

extension AddGoalCreateTableViewCell {
    /// Layout for button
    func layoutButton(for flex: Flex) {
        flex.addItem(button).height(56.0).marginHorizontal(14.0)
    }
}
