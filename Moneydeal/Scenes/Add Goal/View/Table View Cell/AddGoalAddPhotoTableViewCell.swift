//
//  AddGoalAddPhotoTableViewCell.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class AddGoalAddPhotoTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    let button = UIButton()

    var viewModel: AddGoalAddPhotoCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        button.rx.tap
        .throttleTapInMainScheduler()
        .asDriverOnErrorJustComplete()
        .drive(vm.selectSubject)
        .disposed(by: reuseBag)
    }

    override func setup() {
        setupCellStyles()
        setupButton()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.define { flex in
            layoutButton(for: flex)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension AddGoalAddPhotoTableViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width

        contentView.flex.minWidth(headerWidth)
        //contentView.flex.minHeight(100)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }

    /// Setup styles fot button
    func setupButton() {
        let buttonColor = UIColor(red: 0.38, green: 0.243, blue: 0.918, alpha: 1)

        button.setTitle("TTL_ADD_GOAL_PHOTO_ADD_TITLE".localized, for: .normal)
        button.setTitleColor(buttonColor, for: .normal)
        button.titleLabel?.font = UIFont(name: "GothamPro-Bold", size: 16)

        button.contentHorizontalAlignment = .left
    }
}

extension AddGoalAddPhotoTableViewCell {
    /// Layout for button
    func layoutButton(for flex: Flex) {
        flex.addItem(button).width(100%).marginHorizontal(34.0).marginVertical(23.0)
    }
}
