//
//  CustomDatePickerView.swift
//  Moneydeal
//
//  Created by Arsenij Zorin on 14/01/2020.
//  Copyright © 2020 Arsenij Zorin. All rights reserved.
//

import Foundation
import UIKit
import FlexLayout

class CustomDatePickerView: View {
    fileprivate let rootFlexContainer = UIView()
    fileprivate let contentView = UIView()

    let toolbar = UIToolbar()

    let containerView = UIView()

    let datePicker = UIDatePicker()

    var doneButton = UIBarButtonItem()
    var cancelButton = UIBarButtonItem()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        datePicker.datePickerMode = .date
        datePicker.locale = Bundle.localization.locale

        containerView.backgroundColor = .white

        setupToolBar()
    }

    func setupToolBar() {
        doneButton = UIBarButtonItem(title: "TTL_CUSTOMDATEPICKER_OK_BUTTON".localized,
                                         style: .done,
                                         target: nil,
                                         action: nil)

        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                          target: nil,
                                          action: nil)

        cancelButton = UIBarButtonItem(title: "TTL_CUSTOMDATEPICKER_CANCEL_BUTTON".localized,
                                          style: .plain,
                                          target: nil,
                                          action: nil)

        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)

        toolbar.clipsToBounds = true
        toolbar.barTintColor = .white
        toolbar.layer.borderColor = UIColor.white.cgColor
    }

    func updateContainerLayout() {
        let path = UIBezierPath(roundedRect: containerView.bounds,
                                byRoundingCorners: [.topLeft, .topRight],
                                cornerRadii: CGSize(width: 20, height: 20))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        containerView.layer.mask = mask
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        rootFlexContainer.pin.all()

        contentView.pin.all()
        contentView.flex.layout()

        updateContainerLayout()

        toolbar.sizeToFit()
    }
}

extension CustomDatePickerView: BaseView {
    func configure() {
        contentView.flex.direction(.column).height(100%).justifyContent(.end).define { flex in
            let datePickerPadding = UIEdgeInsets(top: 10,
                                                left: 0,
                                                bottom: pin.safeArea.bottom + 30,
                                                right: 0)

            flex.addItem(containerView).padding(datePickerPadding).define { flex in
                flex.addItem(toolbar).marginBottom(5)
                flex.addItem(datePicker)
            }
        }

        backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)

        rootFlexContainer.addSubview(contentView)
        addSubview(rootFlexContainer)
    }
}
