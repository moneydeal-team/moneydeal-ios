//
//  AddGoalView.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 12/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import PinLayout
import UIKit

class AddGoalView: View, ViewScrollable {
    var scrollView: UIScrollView {
        return tableView
    }

    let tableView = UITableView()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        tableView.pin.all()
    }
}

extension AddGoalView: BaseView {
    func configure() {
        addSubview(tableView)
    }
}
