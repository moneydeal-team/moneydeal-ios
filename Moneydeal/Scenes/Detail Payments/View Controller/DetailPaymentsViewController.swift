//
//  DetailPaymentsViewController.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 22/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

class DetailPaymentsViewController: TableViewController, ViewModelHolder, ViewConfigurator {
    var viewModel: DetailPaymentsViewModel? = DetailPaymentsViewModel()

    var dataSource: TableViewDataSource<Section<CellViewModel>>?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "TTL_TABBAR_PERSONAL_EXPENSES_TITLE".localized
    }

    func configure(with view: PersonalExpensesView) {
        view.configure()

        dataSource = TableViewDataSource(cellMap: cellMap)

        guard let vm = viewModel else { return }

        let refresh = view.refreshControl.rx.controlEvent(.valueChanged)
                        .flatMapLatest({ [unowned self] in
                            return self.mainView.tableView.rx.didEndDragging.take(1).mapToVoid()
                        })
                        .asDriverOnErrorJustComplete()

        let output = vm.transform(input: .init())

        output.sections.drive(view.tableView.rx.items(dataSource: dataSource!)).disposed(by: bag)

        view.tableView.rx.itemSelected
        .asDriverOnErrorJustComplete()
        .drive(onNext: { [dataSource] index in
            guard let selectable = try? dataSource?.model(at: index) as? Selectable else {
                return
            }

            Vibration.selection.vibrate()
            selectable.onSelect()
        })
        .disposed(by: bag)
    }

    override func configureCellMap() -> CellMap {
        return [
            GoalsEmbededViewModel.className(): GoalsEmbededView.self,
            CategorySelectorCellViewModel.className(): CategorySelectorTableViewCell.self,
            PersonalCategoryCellViewModel.className(): PersonalCategoryTableViewCell.self,
            EmptyCellViewModel.className(): EmptyTableViewCell.self,
            AddCategoryButtonCellViewModel.className(): AddCategoryButtonTableViewCell.self
        ]
    }
}

extension DetailPaymentsViewController: Transitionable {}
extension DetailPaymentsViewController: AutoBinder {}

extension DetailPaymentsViewController: MDNavigationBarHolder {
    var isLargeTitleMode: Bool {
        false
    }

    var state: MDNavigationBarState {
        .hide
    }
}
