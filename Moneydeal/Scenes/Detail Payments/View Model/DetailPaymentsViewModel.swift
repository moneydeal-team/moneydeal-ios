//
//  DetailPaymentsViewModel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 22/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import Storage
import Utils
import RxSwift
import Network
import RxCocoa

class DetailPaymentsViewModel: ViewModel {

    private var section: Section<CellViewModel> = Section(items: [])
    private let categorySelected = PublishSubject<MockCategory>()
}

extension DetailPaymentsViewModel: ViewModelType {
    struct Input {
    }

    struct Output {
        let sections: Driver<[Section<CellViewModel>]>
        let categorySelected: Driver<MockCategory>
    }

    func transform(input: DetailPaymentsViewModel.Input) -> DetailPaymentsViewModel.Output {
        let sections = BehaviorRelay<[Section<CellViewModel>]>(value: [])

        obtainSections(refresh: true)
        .drive(sections)
        .disposed(by: bag)

        return Output(sections: sections.asDriverOnErrorJustComplete(),
                      categorySelected: categorySelected.asDriverOnErrorJustComplete())
    }
}

extension DetailPaymentsViewModel {
    private func obtainSections(refresh: Bool) -> Driver<[Section<CellViewModel>]> {
        section = Section(items: makeCellViewModels())

        return Driver<[Section<CellViewModel>]>.just([section])
    }

    private func makeCellViewModels() -> [CellViewModel] {
        var cellViewModels: [CellViewModel] = []

        cellViewModels.append(contentsOf: makeCategoryCellViewModels())

        cellViewModels.append(contentsOf: makeAddButtonCellViewModels(for: .expenses))

        return cellViewModels
    }

    func makeCategoryCellViewModels() -> [CellViewModel] {
        // TODO: FAST JUMP
        return [        PersonalCategoryCellViewModel(for: MockCategory(title: "Продукты в \"Пятерочка\"",
                                        iamge: "ic_expenses_apple",
                                        sum: "1 260 руб",
                                        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Продукты в \"Пятерочка\"",
        iamge: "ic_expenses_apple",
        sum: "1 260 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Combo подписка",
        iamge: "ic_expenses_apple",
        sum: "260 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Продукты в \"Перекресток\"",
        iamge: "ic_expenses_apple",
        sum: "560 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Оплата такси домой",
        iamge: "ic_expenses_apple",
        sum: "100 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Завтрак в ресторане",
        iamge: "ic_expenses_apple",
        sum: "420 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Радости для ромашки",
        iamge: "ic_expenses_apple",
        sum: "5 100 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Конструктор Лего",
        iamge: "ic_expenses_apple",
        sum: "10 160 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Заправка автомобиля",
        iamge: "ic_expenses_apple",
        sum: "15 710 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Покупка за наличные",
        iamge: "ic_expenses_apple",
        sum: "12 853 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Мойка автомобиля",
        iamge: "ic_expenses_apple",
        sum: "2 260 руб",
        lastPayment: "Tinkoff black"), selectSubject: categorySelected)]
    }

    func makeAddButtonCellViewModels(for category: CategoryType) -> [CellViewModel] {
        return [AddCategoryButtonCellViewModel(for: category)]
    }
}
