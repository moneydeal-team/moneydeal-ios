//
//  AddGoalCellViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 04/12/2019.
//  Copyright © 2019 Никита Чужиков. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AddGoalCellViewModel: CellViewModel {
    let selectSubject: PublishSubject<Void>

    init(selectSubject: PublishSubject<Void>) {
        self.selectSubject = selectSubject

        super.init()
    }
}

extension AddGoalCellViewModel: Selectable {
    func onSelect() {
        selectSubject.onNext(())
    }
}
