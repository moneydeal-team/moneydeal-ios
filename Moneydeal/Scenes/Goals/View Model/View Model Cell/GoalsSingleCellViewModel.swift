//
//  GoalsSingleCellViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 04/12/2019.
//  Copyright © 2019 Никита Чужиков. All rights reserved.
//

import RxSwift
import Foundation
import Domain

class GoalsSingleCellViewModel: CellViewModel {
    enum Mode {
        case single
        case multi
    }

    let mode: Mode

    let selectSubject: PublishSubject<Goal>

    let goal: Goal

    var imageURL: URL? {
        let defaultURL = Constants.Default.goalImageURL

        return URL(string: (goal.imageURL.isEmpty ? defaultURL : (Constants.Dispather.imageServer+goal.imageURL)))
    }

    init(for goal: Goal, mode: Mode = .single, selectSubject: PublishSubject<Goal>) {
        self.goal = goal
        self.mode = mode
        self.selectSubject = selectSubject
    }
}

extension GoalsSingleCellViewModel: Selectable {
    func onSelect() {
        selectSubject.onNext(goal)
    }
}
