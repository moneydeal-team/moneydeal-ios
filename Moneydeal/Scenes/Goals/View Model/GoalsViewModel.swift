//
//  GoalsViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 04/12/2019.
//  Copyright © 2019 Никита Чужиков. All rights reserved.
//

import Foundation
import Domain
import Storage
import Utils
import RxSwift
import Network
import RxCocoa

class GoalsViewModel: ViewModel {

    static let globalRefresh = PublishSubject<Void>()

    private var section: Section<CellViewModel> = Section(items: [])
    private let goalSelected = PublishSubject<Goal>()
    private let goalAdd = PublishSubject<Void>()

    let paginationTracker = ActivityTracker()

    let refreshTracker = ActivityTracker()

    let mode: GoalsSingleCellViewModel.Mode

    var skipCount = 6
    var lastDate = Date(timeIntervalSince1970: 0)
    var needLoadMore = true

    private let networkingUseCaseProvider: Domain.NetworkingUseCaseProvider
    private let goalUseCase: Domain.GoalUseCase

    init(for mode: GoalsSingleCellViewModel.Mode = .single) {
        let accessToken = AuthorizationService.shared.userInfo?.accessToken

        networkingUseCaseProvider = UseCaseProvider()
        goalUseCase = networkingUseCaseProvider.makeGoalUseCase(token: accessToken)

        self.mode = mode

        super.init()
    }

    func prepareForRefresh() {
        lastDate = Date(timeIntervalSince1970: 0)
        needLoadMore = true
        section = Section(items: [])
    }
}

extension GoalsViewModel: ViewModelType {
    struct Input {
        let refresh: Driver<()>?
        let nextPage: Driver<Void>?
    }

    struct Output {
        let sections: Driver<[Section<CellViewModel>]>
        let goalSelected: Driver<Goal>
        let addSubject: Driver<Void>
    }

    func transform(input: GoalsViewModel.Input) -> GoalsViewModel.Output {
        // TODO: FIX IT
        if mode == .multi {
            skipCount = 10000
        }

        let sections = BehaviorRelay<[Section<CellViewModel>]>(value: [])

        input.nextPage?
         .filter({ [unowned self] _ in
             return self.needLoadMore
         })
         .flatMap({ [unowned self] in
             return self.obtainGoals(refresh: false).trackActivity(self.paginationTracker)
         })
         .drive(sections)
         .disposed(by: bag)

        input.refresh?.asDriver()
        .flatMapLatest({ [unowned self] in
            return self.obtainGoals(refresh: true).trackActivity(self.refreshTracker)
        })
        .drive(sections)
        .disposed(by: bag)

        GoalsViewModel.globalRefresh.asDriverOnErrorJustComplete()
        .flatMapLatest({ [unowned self] in
            return self.obtainGoals(refresh: true).trackActivity(self.refreshTracker)
        })
        .drive(sections)
        .disposed(by: bag)

        obtainGoals(refresh: true)
        .drive(sections)
        .disposed(by: bag)

        return Output(sections: sections.asDriverOnErrorJustComplete(),
                      goalSelected: goalSelected.asDriverOnErrorJustComplete(),
                      addSubject: goalAdd.asDriverOnErrorJustComplete())
    }
}

extension GoalsViewModel {
    private func obtainGoals(refresh: Bool) -> Driver<[Section<CellViewModel>]> {
        if refresh {
            prepareForRefresh()
        }

        return goalUseCase.goals(from: lastDate, count: skipCount).asObservable()
            .map({ [weak self] goals in
                guard let vm = self else { return [Section<CellViewModel>(items: [])] }

                if vm.needLoadMore && !vm.section.items.isEmpty {
                    vm.section.items.removeLast()
                }

                if goals.isEmpty {
                    vm.needLoadMore = false
                }

                if let date = goals.compactMap({ $0.endDate.asDate() }).max() {
                    vm.lastDate = date
                }

                vm.section.items.append(contentsOf: GoalFactory.makeCellViewModels(for: goals,
                                        with: vm.mode,
                                        selectSubject: vm.goalSelected,
                                        addSubject: vm.goalAdd))

                return [vm.section]
            }).asDriverOnErrorJustComplete()
    }
}

enum GoalFactory {
    static func makeCellViewModels(for goals: GoalList,
                                   with mode: GoalsSingleCellViewModel.Mode,
                                   selectSubject: PublishSubject<Goal>,
                                   addSubject: PublishSubject<Void>) -> [CellViewModel] {
        var cellViewModels: [CellViewModel] = []

        cellViewModels.append(contentsOf: makeSingleGoalsCellViewModel(for: goals, with: mode, selectSubject))

        cellViewModels.append(contentsOf: makeAddGoalCellViewModel(addSubject))

        return cellViewModels
    }

    static func makeSingleGoalsCellViewModel(for goals: GoalList,
                                             with mode: GoalsSingleCellViewModel.Mode,
                                             _ selectSubject: PublishSubject<Goal>) -> [CellViewModel] {
//        var goals: [Goal] = []
//
//        for idx in 0...10 {
//            goals.append(Goal(id: "\(idx)",
//                              name: "Test \(idx)",
//                              endDate: (Date()+(TimeInterval(20000*idx))).asString(format: Utils.Constants.defaultDateFormat),
//                              balance: UInt64(150000 + idx),
//                              amount: UInt64(500000 + idx),
//                              imageURL: "https://upload.wikimedia.org/wikipedia/commons/7/7e/2018_Audi_A4_Sport_TDi_Quattro_S-A_2.0.jpg"))
//        }

        return goals.map({ GoalsSingleCellViewModel(for: $0, mode: mode, selectSubject: selectSubject) })
    }

    static func makeAddGoalCellViewModel(_ addSubject: PublishSubject<Void>) -> [CellViewModel] {
        return [AddGoalCellViewModel(selectSubject: addSubject)]
    }
}
