//
//  GoalsView.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 03/12/2019.
//  Copyright © 2019 Никита Чужиков. All rights reserved.
//

import Foundation
import UIKit

class GoalsView: View, ViewScrollable {
    var scrollView: UIScrollView {
        return self.collectionView
    }

    let flowLayout = UICollectionViewFlowLayout()

    var loadingIndicator = UIActivityIndicatorView()

    lazy var collectionView: UICollectionView = {
        self.flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        self.flowLayout.minimumLineSpacing = 13
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.flowLayout)
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 13, right: 0)

        return collectionView
    }()

    let refreshControl = UIRefreshControl()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        collectionView.refreshControl = refreshControl

        loadingIndicator.tintColor = UIColor(red: 0.25, green: 0.75, blue: 0.85, alpha: 1)

        collectionView.addSubview(loadingIndicator)

        loadingIndicator.isHidden = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        collectionView.pin.all()

        loadingIndicator.frame = CGRect(x: loadingIndicator.frame.minX,
                                        y: loadingIndicator.frame.maxY,
                                        width: collectionView.bounds.width,
                                        height: 44)
    }

}

extension GoalsView: BaseView {
    func configure() {
        addSubview(collectionView)
    }
}
