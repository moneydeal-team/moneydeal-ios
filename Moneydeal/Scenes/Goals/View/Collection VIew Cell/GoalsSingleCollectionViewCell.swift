//
//  GoalsSingleCollectionViewCell.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 04/12/2019.
//  Copyright © 2019 Никита Чужиков. All rights reserved.
//

import UIKit
import FlexLayout

class GoalsSingleCollectionViewCell: CollectionViewCell, CellAutoBinder, ViewModelHolder {
    let goalNameLabel = UILabel()
    let yearLabel = UILabel()
    let priceLabel = UILabel()
    let percentStartLabel = UILabel()
    let percentEndLabel = UILabel()
    let progressView = UIView()
    let progressInputView = UIView()

    var gradient = CAGradientLayer()

    let imageView = UIImageView()

    let imageViewContainer = UIView()

    var viewModel: GoalsSingleCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
        setupGradientToProgressInputView()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        goalNameLabel.text = vm.goal.name

        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.maximumFractionDigits = 0
        // localize to your grouping and decimal separator
        currencyFormatter.locale = Locale.current

        let amountString = currencyFormatter.string(from: NSNumber(value: vm.goal.amount)) ?? ""
        let balanceString = currencyFormatter.string(from: NSNumber(value: vm.goal.balance)) ?? ""

        priceLabel.text = String(format: "TTL_GOAL_CURRENT_AMOUNT".localized, balanceString,
                                                                              amountString)

        percentStartLabel.text = String(format: "%.1f%%", vm.goal.progressPercent*100)

        let modeSpace: CGFloat = vm.mode == .single ? -32.0 : -70.0

        let cellWidth: CGFloat = UIScreen.main.bounds.width + modeSpace

        contentView.flex.minWidth(cellWidth)

        goalNameLabel.flex.markDirty()
        priceLabel.flex.markDirty()
        percentStartLabel.flex.markDirty()
        progressView.flex.markDirty()
        progressInputView.flex.markDirty()

        setupImageView(from: vm.imageURL)

        setupGradientToProgressInputView()
    }

    override func setup() {
        setupCellStyles()

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.addItem().direction(.row).alignContent(.spaceBetween).define { flex in
            flex.addItem().width(50%).shrink(1).direction(.column).marginTop(0.0).marginLeft(0.0).define { flex in
                layoutGoalNameLabel(for: flex)
                layoutYearLabel(for: flex)
                layoutPriceLabel(for: flex)
                layoutPercents(for: flex)
                layoutProgressView(for: flex)
            }
            flex.addItem(imageViewContainer)
                .direction(.row)
                .width(50%)
                .height(177.0)
                .marginLeft(20.0)
                .define { flex in
                    layoutImageView(for: flex)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        CATransaction.begin()
        CATransaction.setDisableActions(true)

        gradient.frame = .zero

        CATransaction.commit()
    }
}

extension GoalsSingleCollectionViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()
        setupGoalNameLabel()
        setupYearLabel()
        setupPriceLabel()
        setupPesentStartLabel()
        setupPesentEndLabel()
        setupProgressView()
        setupProgressInputView()
        setupImageView()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let cellWidth: CGFloat = UIScreen.main.bounds.width - 32.0
        let cellHeight: CGFloat = 177.0
        let cornerRadius: CGFloat = 20.0

        //contentView.flex.width(cellWidth)
        contentView.flex.height(cellHeight)
        contentView.flex.markDirty()

        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = cornerRadius

        // Setup shadow
        contentView.layer.shadowColor = UIColor(red: 0.501, green: 0.501, blue: 0.738, alpha: 0.36).cgColor
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 5.0, height: 10.0)
        contentView.layer.shadowRadius = 20

        contentView.layer.shouldRasterize = true

        contentView.layer.rasterizationScale = UIScreen.main.scale
    }

    /// Setup goal name label
    func setupGoalNameLabel() {
        goalNameLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        goalNameLabel.font = UIFont(name: "GothamPro-Bold", size: 24)

        goalNameLabel.text = "Audi A4"

        goalNameLabel.numberOfLines = 2
        goalNameLabel.textAlignment = .left
    }

    /// Setup year label
    func setupYearLabel() {
        yearLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)

        yearLabel.font = UIFont(name: "GothamPro-Medium", size: 11)

        yearLabel.text = "2022"

        yearLabel.numberOfLines = 0
        yearLabel.textAlignment = .left
    }

    /// Setup price label
    func setupPriceLabel() {
        priceLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        priceLabel.font = UIFont(name: "GothamPro-Medium", size: 10)

        priceLabel.text = "28 000$"

        priceLabel.numberOfLines = 0
        priceLabel.textAlignment = .left
    }

    /// Setup percent start label
    func setupPesentStartLabel() {
        percentStartLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        percentStartLabel.font = UIFont(name: "GothamPro-Medium", size: 10)

        percentStartLabel.text = "42%"

        percentStartLabel.numberOfLines = 0
        percentStartLabel.textAlignment = .left
    }

    /// Setup percent end label
    func setupPesentEndLabel() {
        percentEndLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        percentEndLabel.font = UIFont(name: "GothamPro-Medium", size: 10)

        percentEndLabel.text = "100%"

        percentEndLabel.numberOfLines = 0
        percentEndLabel.textAlignment = .right
    }

    /// Setup progress view
    func setupProgressView() {
        progressView.backgroundColor = .clear

        progressView.layer.masksToBounds = true

        progressView.layer.borderColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1).cgColor
        progressView.layer.borderWidth = 0.3
    }

    /// Setup progress input view
    func setupProgressInputView() {
        let startColor = UIColor(red: 0.357, green: 0.925, blue: 0.62, alpha: 1).cgColor
        let endColor = UIColor(red: 0.439, green: 0.722, blue: 0.569, alpha: 1).cgColor

        gradient.colors = [startColor, endColor]
        gradient.locations = [0, 1]
        gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.5)

        gradient.frame = progressInputView.bounds

        progressInputView.backgroundColor = .white
        progressInputView.layer.addSublayer(gradient)
    }

    /// Setup gradient to progress input view
    func setupGradientToProgressInputView() {
        if let vm = viewModel {
            CATransaction.begin()
            CATransaction.setDisableActions(true)

            gradient.frame = CGRect(x: 0, y: 0,
                                    width: CGFloat(vm.goal.progressPercent)*progressInputView.bounds.width,
                                    height: progressInputView.bounds.height)

            CATransaction.commit()
        }
    }

    /// Setup image view
    func setupImageView(from url: URL? = nil) {
        imageView.contentMode = .scaleAspectFill

        imageViewContainer.layer.masksToBounds = true
        imageViewContainer.clipsToBounds = true
        imageViewContainer.layer.cornerRadius = 20.0

        imageView.layer.masksToBounds = false
        imageView.clipsToBounds = true

        imageView.layer.cornerRadius = 200/2

        if let url = url {
            imageView.kf.setImage(with: url,
                                  placeholder: nil,
                                  options: [.backgroundDecode],
                                  progressBlock: nil,
                                  completionHandler: nil, auth: true)
            return
        }

        imageView.image = UIImage(named: "md_app_logo")
    }
}

extension GoalsSingleCollectionViewCell {
    /// Layout for goal name label
    func layoutGoalNameLabel(for flex: Flex) {
        flex.addItem(goalNameLabel).marginTop(23.0).marginLeft(18.0)
    }

    /// Layout for year label
    func layoutYearLabel(for flex: Flex) {
        flex.addItem(yearLabel).marginTop(9.0).marginLeft(18.0)
    }

    /// Layout for price label
    func layoutPriceLabel(for flex: Flex) {
        flex.addItem(priceLabel).marginTop(9.0).marginLeft(18.0)
    }

    /// Layout for percents
    func layoutPercents(for flex: Flex) {
        flex.addItem().marginTop(39.0).marginLeft(18.0).direction(.row).define { flex in
            flex.addItem(percentStartLabel)
            flex.addItem(percentEndLabel).position(.absolute).right(0.0)
        }
    }

    /// Layout for progress view
    func layoutProgressView(for flex: Flex) {
        flex.addItem(progressView).marginTop(3.0).marginLeft(18.0).height(5.0).define { flex in
            flex.addItem(progressInputView).height(5.0).width(100%)
        }
    }

    /// Layout for image view
    func layoutImageView(for flex: Flex) {
        flex.addItem(imageView).position(.absolute).width(200).height(200).top(6).left(10)
    }
}
