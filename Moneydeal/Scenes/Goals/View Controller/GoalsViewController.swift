//
//  GoalsViewController.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 03/12/2019.
//  Copyright © 2019 Никита Чужиков. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum GoalsRouter {
    static let toDetailGoal = PushTransition(destination: DetailGoalViewController.self)
    static let toAddGoal = PresentPopupTransition(destination: AddGoalViewController.self)
}

class GoalsViewController: CollectionViewController, ViewConfigurator, ViewModelHolder {

    var viewModel: GoalsViewModel? = GoalsViewModel()

    var nextPage = PublishSubject<Void>()

    var dataSource: CollectionViewSectionedDataSource<Section<CellViewModel>>?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "TTL_TABBAR_GOALS_TITLE".localized
    }

    func configure(with view: GoalsView) {
        view.configure()
        configureBindings()
    }

    func configureBindings() {
        guard let vm = viewModel else { return }

        let refresh = mainView.refreshControl.rx.controlEvent(.valueChanged)
            .flatMapLatest({ [unowned self] in
                return self.mainView.collectionView.rx.didEndDragging.take(1).mapToVoid()
            })
            .asDriverOnErrorJustComplete()

        let output = vm.transform(input: .init(refresh: refresh,
                                               nextPage: nextPage.asDriverOnErrorJustComplete()))

        vm.refreshTracker.drive(mainView.refreshControl.rx.isRefreshing).disposed(by: bag)

        dataSource = CollectionViewSectionedDataSource(cellMap: cellMap)

        output.sections.drive(mainView.collectionView.rx.items(dataSource: dataSource!)).disposed(by: bag)

        output.goalSelected
        .map({ DetailGoalViewController.Input(goal: $0) })
        .asTransitionable()
        .transit(to: GoalsRouter.toDetailGoal)
        .disposed(by: bag)

        output.addSubject
        .asTransitionable()
        .transit(to: GoalsRouter.toAddGoal)
        .disposed(by: bag)

        mainView.collectionView.rx.itemSelected
        .asDriverOnErrorJustComplete()
        .drive(onNext: { [dataSource] index in
            guard let selectable = try? dataSource?.model(at: index) as? Selectable else {
                return
            }

            Vibration.selection.vibrate()
            selectable.onSelect()
        })
        .disposed(by: bag)

        Observable.combineLatest(mainView.collectionView.rx.didScroll,
                                 vm.paginationTracker.asObservable())
            .filter({ [unowned vm] _ in
                vm.needLoadMore
            })
            .throttle(0.5, scheduler: MainScheduler.asyncInstance)
            .observeOn(MainScheduler.asyncInstance)
            .filter({ [weak self] _, paginatingNow -> Bool in
                guard !paginatingNow else { return false }

                guard let self = self else { return false }
                let threshold =  self.mainView.collectionView.bounds.height
                let visibleHeight = self.mainView.collectionView.contentSize.height -
                                    self.mainView.collectionView.contentOffset.y

                return threshold > visibleHeight
            })
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] _ in
                self?.nextPage.onNext(())
            })
            .disposed(by: bag)

//        vm.paginationTracker.map({ !$0 }).drive(mainView.loadingIndicator.rx.isHidden).disposed(by: bag)
//        vm.paginationTracker.map({ $0 }).drive(mainView.loadingIndicator.rx.isAnimating).disposed(by: bag)
    }

    override func configureCellMap() -> CellMap {
        return [
            GoalsSingleCellViewModel.className(): GoalsSingleCollectionViewCell.self,
            AddGoalCellViewModel.className(): AddGoalCollectionViewCell.self
        ]
    }
}

extension GoalsViewController: Transitionable {}

extension GoalsViewController: MDNavigationBarHolder {
    var isLargeTitleMode: Bool {
        true
    }

    var state: MDNavigationBarState {
        .display
    }
}
