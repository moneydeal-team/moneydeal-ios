//
//  GoalsEmbededView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 11/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout
import Foundation
import RxSwift
import RxCocoa

class GoalsEmbededView: TableViewCell, CellAutoBinder, ViewModelHolder {

    let itemWidth: CGFloat = UIScreen.main.bounds.width - 70.0

    fileprivate var indexOfCellBeforeDragging = 0
    fileprivate var countCells: Int = 0

    let flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//
        let spacingBetweenCards: CGFloat = 20.0

        layout.sectionInset = UIEdgeInsets(top: 0, left: spacingBetweenCards / 2.0, bottom: 0, right: spacingBetweenCards / 2.0)
        layout.minimumLineSpacing = spacingBetweenCards
        layout.minimumInteritemSpacing = spacingBetweenCards

        layout.scrollDirection = .horizontal

        return layout
    }()

    lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.flowLayout)

        collectionView.backgroundColor = .white

        return collectionView
    }()

    var itemSize = CGSize(width: 0, height: 0)

    var viewModel: GoalsEmbededViewModel? {
        didSet {
            setupBindings()
        }
    }

    var goalsViewModel = GoalsViewModel(for: .multi)

    var dataSource: CollectionViewSectionedDataSource<Section<CellViewModel>>?

    override func setup() {
        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.showsHorizontalScrollIndicator = false

        configureGoalsViewModel()
    }

    override func setupLayout() {
        contentView.flex.define { flex in
            flex.addItem(collectionView).width(100%).height(100%).minHeight(250.0)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
        collectionView.setNeedsDisplay()
        collectionView.setNeedsLayout()
        collectionView.layoutIfNeeded()
//
//        let width = collectionView.bounds.size.width
//        let height = collectionView.bounds.size.height
//        itemSize = CGSize(width: width, height: height)
    }

    func setupBindings() {
        guard let vm = viewModel else { return }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension GoalsEmbededView {
    func configureGoalsViewModel() {
        let cellMap = configureCellMap()

        let output = goalsViewModel.transform(input: .init(refresh: nil, nextPage: nil))

        dataSource = CollectionViewSectionedDataSource(cellMap: cellMap)

        output.sections.drive(collectionView.rx.items(dataSource: dataSource!)).disposed(by: bag)

        output.sections.map({ $0.first?.items.count })
        .drive(onNext: { [weak self] goalsCount  in
            self?.countCells = goalsCount ?? 0
        }).disposed(by: bag)

        output.goalSelected
        .map({ DetailGoalViewController.Input(goal: $0) })
        .asTransitionable()
        .transit(to: GoalsRouter.toDetailGoal)
        .disposed(by: bag)

        collectionView.rx.itemSelected
        .asDriverOnErrorJustComplete()
        .drive(onNext: { [dataSource] index in
            guard let selectable = try? dataSource?.model(at: index) as? Selectable else {
                return
            }

            Vibration.selection.vibrate()
            selectable.onSelect()
        })
        .disposed(by: bag)

        collectionView.rx.willBeginDragging.observeOn(MainScheduler.asyncInstance)
        .asDriverOnErrorJustComplete()
        .drive(onNext: { [weak self] _ in
            guard let self = self else { return }
            self.indexOfCellBeforeDragging = self.indexOfMajorCell()

        }).disposed(by: bag)

        collectionView.rx.willEndDragging.observeOn(MainScheduler.asyncInstance)
        .asDriverOnErrorJustComplete()
        .drive(onNext: { [weak self] (velocity, targetContentOffset) in
            guard let self = self else { return }
            // Stop scrollView sliding:
            targetContentOffset.pointee = self.collectionView.contentOffset

            // calculate where scrollView should snap to:
            let indexOfMajorCell = self.indexOfMajorCell()

            // calculate conditions:
            let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
            let hasEnoughVelocityToSlideToTheNextCell = self.indexOfCellBeforeDragging + 1 < self.countCells &&
                                                        velocity.x > swipeVelocityThreshold

            let hasEnoughVelocityToSlideToThePreviousCell = self.indexOfCellBeforeDragging - 1 >= 0 &&
                                                            velocity.x < -swipeVelocityThreshold

            let majorCellIsTheCellBeforeDragging = indexOfMajorCell == self.indexOfCellBeforeDragging

            let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging &&
                                        (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)

            if didUseSwipeToSkipCell {
                let snapToIndex = self.indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)

                let indexPath = IndexPath(row: snapToIndex, section: 0)

                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            } else {
                // This is a much better way to scroll to a cell:
                let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }

        }).disposed(by: bag)
    }

    private func indexOfMajorCell() -> Int {
        let proportionalOffset = collectionView.contentOffset.x / self.itemWidth
        let index = Int(round(proportionalOffset))
        let safeIndex = max(0, min(countCells - 1, index))

        return safeIndex
    }
}

extension GoalsEmbededView {
    func configureCellMap() -> CellMap {
        return [
            GoalsSingleCellViewModel.className(): GoalsSingleCollectionViewCell.self,
            AddGoalCellViewModel.className(): PersonalGoalAddCollectionViewCell.self
        ]
    }
}
