//
//  PersonalCategoryTableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import FlexLayout

class PersonalCategoryTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    let iconView = UIImageView()
    let sumLabel = UILabel()
    let lastPayLabel = UILabel()
    let nameLabel = UILabel()

    var viewModel: PersonalCategoryCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        nameLabel.text = vm.category.title
        iconView.image = UIImage(named: vm.category.iamge)
        sumLabel.text = vm.category.sum
        lastPayLabel.text = vm.category.lastPayment
    }

    override func setup() {
        setupCellStyles()
        setupIconView()
        setupNameLabel()
        setupLastPayLabel()
        setupSumLabel()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    override func setupLayout() {
        let cellMargin = UIEdgeInsets(top: 14, left: 36, bottom: 14, right: 18)
        contentView.flex.define { flex in
            flex.addItem().margin(cellMargin).direction(.row).define { flex in
                flex.addItem().shrink(0).define { flex in
                    layouticonView(for: flex)
                }
                flex.addItem().grow(1).shrink(1).marginLeft(27.0).define { flex in
                    layoutNameLabel(for: flex)
                    layoutLastPayLabel(for: flex)
                }
                flex.addItem().shrink(0).grow(1).define { flex in
                    layoutSumLabel(for: flex)
                }
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension PersonalCategoryTableViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width

        contentView.flex.minWidth(headerWidth)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }

    /// Setup styles for image view
    func setupIconView() {
        iconView.image = UIImage(named: "ic_expenses_icon")
    }

    /// Setup styles for name label
    func setupNameLabel() {
        nameLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        nameLabel.font = UIFont(name: "GothamPro-Medium", size: 14)

        nameLabel.text = "Тестовые напитки"

        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .left
    }

    /// Setup styles for date label
    func setupLastPayLabel() {
        lastPayLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)

        lastPayLabel.font = UIFont(name: "GothamPro", size: 12)

        lastPayLabel.text = "-325 руб"

        lastPayLabel.numberOfLines = 0
        lastPayLabel.textAlignment = .left
    }

    /// Setup styles for sum label
    func setupSumLabel() {
        sumLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        sumLabel.font = UIFont(name: "GothamPro-Medium", size: 14)

        sumLabel.text = "2 560 руб"

        sumLabel.numberOfLines = 0
        sumLabel.textAlignment = .right
    }
}

extension PersonalCategoryTableViewCell {
    /// Layout for image view
    func layouticonView(for flex: Flex) {
        let sizes: CGSize = CGSize(width: 20.0, height: 20.0)
        flex.addItem(iconView).marginTop(8).width(sizes.width).height(sizes.height)
    }

    /// Layout for name label
    func layoutNameLabel(for flex: Flex) {
        flex.addItem(nameLabel)
    }

    /// Layout for date label
    func layoutLastPayLabel(for flex: Flex) {
        flex.addItem(lastPayLabel).marginTop(15.0)
    }

    /// Layout for sum label
    func layoutSumLabel(for flex: Flex) {
        flex.addItem(sumLabel)
    }
}
