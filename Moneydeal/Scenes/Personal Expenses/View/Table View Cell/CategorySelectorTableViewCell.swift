//
//  CategorySelectorTableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 13/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FlexLayout

class CategorySelectorTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {

    var viewModel: CategorySelectorCellViewModel? {
        didSet {
            setupBindings()
        }
    }

    let expensesView = UIView()
    let revenueView = UIView()

    let expensesLable = UILabel()
    let revenueLabel = UILabel()

    let selectorView = UIView()

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    override func setupLayout() {
        let cellMargin = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        let titleHeight: CGFloat = 28.0

        let marginBottomForSeparator: CGFloat = 10.0

        let separatorColor = UIColor(red: 0.91, green: 0.914, blue: 0.925, alpha: 1)

        contentView.flex.define { flex in
            flex.addItem().margin(cellMargin)
                .alignItems(.center)
                .direction(.column).define { flex in
                    flex.addItem().direction(.row)
                    .alignItems(.center)
                    .marginBottom(marginBottomForSeparator)
                    .width(100%).define { flex in
                        flex.addItem(expensesView).width(50%).define({ flex in
                            flex.addItem(expensesLable).width(100%).minHeight(titleHeight)
                        })

                        flex.addItem(revenueView).width(50%).define({ flex in
                            flex.addItem(revenueLabel).width(100%).minHeight(titleHeight)
                        })
                    }

                    flex.addItem().width(100%).alignItems(.center).define { flex in
                        flex.addItem()
                            .height(0.5)
                            .width(100%)
                            .backgroundColor(separatorColor).define { flex in
                                flex.addItem(selectorView)
                                    .height(2)
                                    .width(30%)
                                    .position(.absolute)
                                    .bottom(-1)
                        }
                    }
            }
        }
    }

    override func setup() {
        setupBlocks(isExpensesActive: true, animated: false)

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    func setupBlocks(isExpensesActive: Bool, animated: Bool) {
        setupExpensesLable(isActive: isExpensesActive, animated: animated)
        setupRevenueLabel(isActive: !isExpensesActive, animated: animated)

        setupSelectorView(isExpensesActive: isExpensesActive,
                          animated: animated)
    }

    func setupExpensesLable(isActive: Bool, animated: Bool) {
        let activeColor = UIColor(red: 0.231, green: 0.255, blue: 0.294, alpha: 1)
        let disableColor = UIColor(red: 0.651, green: 0.667, blue: 0.706, alpha: 1)

        if animated {
            UIView.transition(with: expensesLable,
                              duration: 0.3,
                              options: .transitionCrossDissolve,
                              animations: {  [weak self] in

                self?.expensesLable.textColor = isActive ? activeColor : disableColor
            })
        } else {
            expensesLable.textColor = isActive ? activeColor : disableColor
        }

        expensesLable.font = UIFont(name: "GothamPro-Bold", size: 18)

        expensesLable.text = "TTL_PERSONAL_EXPENNSES_CAT_EXPENSES_LABEL".localized

        expensesLable.textAlignment = .center
    }

    func setupRevenueLabel(isActive: Bool, animated: Bool) {
        let activeColor = UIColor(red: 0.231, green: 0.255, blue: 0.294, alpha: 1)
        let disableColor = UIColor(red: 0.651, green: 0.667, blue: 0.706, alpha: 1)

        if animated {
            UIView.transition(with: revenueLabel,
                              duration: 0.3,
                              options: .transitionCrossDissolve,
                              animations: {  [weak self] in

                self?.revenueLabel.textColor = isActive ? activeColor : disableColor
            })
        } else {
            revenueLabel.textColor = isActive ? activeColor : disableColor
        }

        revenueLabel.font = UIFont(name: "GothamPro-Bold", size: 18)

        revenueLabel.text = "TTL_PERSONAL_EXPENNSES_CAT_REVENUE_LABEL".localized

        revenueLabel.textAlignment = .center
    }

    func setupSelectorView(isExpensesActive: Bool, animated: Bool) {
        let expensesColor = UIColor(red: 0.353, green: 0.245, blue: 0.775, alpha: 1)
        let revenueLabel = UIColor(red: 1, green: 0.49, blue: 0, alpha: 1)

        selectorView.layer.cornerRadius = 2.0

        let leftSpace: FPercent = isExpensesActive ? 10% : 60%

        if animated {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.selectorView.backgroundColor = isExpensesActive ? expensesColor : revenueLabel
                self?.selectorView.flex.left(leftSpace).markDirty()
                self?.setNeedsLayout()
                self?.layoutIfNeeded()
            }
        } else {
            selectorView.backgroundColor = isExpensesActive ? expensesColor : revenueLabel
            selectorView.flex.left(leftSpace).markDirty()
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func setupBindings() {
        //guard let vm = viewModel else { return }

        setupGestureExpensesView()
        setupGestureRevenueView()
    }

    func setupGestureExpensesView() {
        let tapGesture = UITapGestureRecognizer()
        expensesView.addGestureRecognizer(tapGesture)

        let tapInsideLabelDriver = tapGesture.rx.event.map({ _ -> Bool in
            return true
        }).asDriverOnErrorJustComplete()

        tapInsideLabelDriver
        .drive(rx.isExpensesEnable)
        .disposed(by: reuseBag)
    }

    func setupGestureRevenueView() {
        let tapGesture = UITapGestureRecognizer()
        revenueView.addGestureRecognizer(tapGesture)

        let tapInsideLabelDriver = tapGesture.rx.event.map({ _ -> Bool in
            return false
        }).asDriverOnErrorJustComplete()

        tapInsideLabelDriver
        .drive(rx.isExpensesEnable)
        .disposed(by: reuseBag)
    }
}

extension Reactive where Base: CategorySelectorTableViewCell {
    internal var isExpensesEnable: Binder<Bool> {
      return Binder(self.base) { view, isActive in
        view.setupBlocks(isExpensesActive: isActive, animated: true)
      }
    }
}
