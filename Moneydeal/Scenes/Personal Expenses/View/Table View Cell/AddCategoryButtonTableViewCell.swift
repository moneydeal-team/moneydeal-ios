//
//  AddCategoryButtonTableViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 22/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit
import FlexLayout

class AddCategoryButtonTableViewCell: TableViewCell, CellAutoBinder, ViewModelHolder {
    let addIconView = UIView()
    let titleLabel = UILabel()

    let plusImageView = UIImageView()

    var viewModel: AddCategoryButtonCellViewModel? {
        didSet {
            setupBindings()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    override func setup() {
        setupAddIconView()
        setupTitleLabel()
        setupPlusImageView()

        separatorInset = .zero
        selectionStyle = .none

        backgroundColor = .clear
    }

    override func setupLayout() {
        let cellMargin = UIEdgeInsets(top: 14, left: 26, bottom: 14, right: 18)
        contentView.flex.define { flex in
            flex.addItem().margin(cellMargin).direction(.row).define { flex in
                layoutAddIconView(for: flex)
                layoutTitleLabel(for: flex)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    func setupBindings() {
        guard let vm = viewModel else { return }

        titleLabel.text = getTitle(for: vm.categoryType)
    }

    func getTitle(for category: CategoryType) -> String {
        switch category {
        case .expenses:
            return "TTL_ADD_CATEGORY_EXPENSES_TITLE".localized
        case .revenue:
            return "TTL_ADD_CATEGORY_REVENUE_TITLE".localized
        }
    }
}

extension AddCategoryButtonTableViewCell {
    func setupAddIconView() {
        addIconView.backgroundColor = UIColor(red: 0.063, green: 0.788, blue: 0.443, alpha: 1)
        addIconView.layer.cornerRadius = 20.0
    }

    func setupTitleLabel() {
        titleLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)
        titleLabel.font = UIFont(name: "GothamPro-Medium", size: 14)
    }

    func setupPlusImageView() {
        plusImageView.image = UIImage(named: "ic_expenses_plus_icon")
    }
}

extension AddCategoryButtonTableViewCell {
    func layoutAddIconView(for flex: Flex) {
        let size = CGSize(width: 40.0, height: 40.0)

        addIconView.flex.size(size)

        flex.addItem(addIconView).define { flex in
            layoutPlusImageView(for: flex)
        }
    }

    func layoutTitleLabel(for flex: Flex) {
        let leftMargin: CGFloat = ceil(16.96)

        titleLabel.flex.marginLeft(leftMargin)

        flex.addItem(titleLabel)
    }

    func layoutPlusImageView(for flex: Flex) {
        flex.addItem()
        .width(100%).height(100%)
        .direction(.column)
        .justifyContent(.center).define { flex in
            flex.addItem(plusImageView).alignSelf(.center)
        }
    }
}
