//
//  PersonalExpensesView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 10/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import PinLayout
import UIKit

class PersonalExpensesView: View, ViewScrollable {
    var scrollView: UIScrollView {
        return tableView
    }

    let tableView = UITableView()

    let refreshControl = UIRefreshControl()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        tableView.separatorStyle = .none
        tableView.refreshControl = refreshControl
        tableView.showsVerticalScrollIndicator = false
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        tableView.pin.all()
    }

}

extension PersonalExpensesView: BaseView {
    func configure() {
        addSubview(tableView)
    }
}
