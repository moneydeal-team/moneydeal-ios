//
//  PersonalGoalAddCollectionViewCell.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class PersonalGoalAddCollectionViewCell: CollectionViewCell, CellAutoBinder, ViewModelHolder {
    let button = UIButton()
    let nameLabel = UILabel()

    var viewModel: AddGoalCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
//        guard let vm = viewModel else { return }

        layoutSubviews()
    }

    override func setup() {
        setupCellStyles()

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.justifyContent(.center).direction(.column).define { flex in
            flex.addItem().direction(.column).alignItems(.center).justifyContent(.center).define { flex in
                layoutButton(for: flex)
                layoutTextFirld(for: flex)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension PersonalGoalAddCollectionViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()
        setupButton()
        setupTextField()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let cellWidth: CGFloat = 127.0
        let cellHeight: CGFloat = 177.0
        let cornerRadius: CGFloat = 20.0

        contentView.flex.minWidth(cellWidth)
        contentView.flex.minHeight(cellHeight)
        //contentView.flex.markDirty()

        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = cornerRadius

        // Setup shadow
        contentView.layer.shadowColor = UIColor(red: 0.501, green: 0.501, blue: 0.738, alpha: 0.36).cgColor
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 5.0, height: 10.0)
        contentView.layer.shadowRadius = 20

        contentView.layer.shouldRasterize = true

        contentView.layer.rasterizationScale = UIScreen.main.scale
    }

    /// Setup styles for button
    func setupButton() {
        button.setImage(UIImage(named: "AddButton"), for: .normal)
    }

    /// Setup styles for text field
    func setupTextField() {
        let color = UIColor(red: 0.651, green: 0.667, blue: 0.706, alpha: 1)

        nameLabel.textColor = color

        nameLabel.text = "TTL_ADD_GOAL_LABEL_TEXT".localized
        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .center

        nameLabel.font = UIFont(name: "GothamPro", size: 12)
    }
}

extension PersonalGoalAddCollectionViewCell {
    /// Layout for button
    func layoutButton(for flex: Flex) {
        flex.addItem(button).size(40.0)
    }

    /// Layout for text field
    func layoutTextFirld(for flex: Flex) {
        flex.addItem(nameLabel).marginTop(12.0)
    }
}
