//
//  AddCategoryButtonCellViewModel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 22/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

enum CategoryType {
    case expenses
    case revenue
}

class AddCategoryButtonCellViewModel: CellViewModel {
    let categoryType: CategoryType

    init(for category: CategoryType) {
        self.categoryType = category

        super.init()
    }
}
