//
//  PersonalCategoryCellViewModel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MockCategory {
    let title: String
    let iamge: String
    let sum: String
    let lastPayment: String
}

class PersonalCategoryCellViewModel: CellViewModel {
    let category: MockCategory

    let selectSubject: PublishSubject<MockCategory>

    init(for category: MockCategory, selectSubject: PublishSubject<MockCategory>) {
        self.category = category
        self.selectSubject = selectSubject
    }
}

extension PersonalCategoryCellViewModel: Selectable {
    func onSelect() {
        selectSubject.onNext(category)
    }
}
