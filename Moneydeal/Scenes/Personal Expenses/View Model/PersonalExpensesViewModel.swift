//
//  PersonalExpensesViewModel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 10/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import Storage
import Utils
import RxSwift
import Network
import RxCocoa

class PersonalExpensesViewModel: ViewModel {

    private var section: Section<CellViewModel> = Section(items: [])
    private let categorySelected = PublishSubject<MockCategory>()
}

extension PersonalExpensesViewModel: ViewModelType {
    struct Input {
    }

    struct Output {
        let sections: Driver<[Section<CellViewModel>]>
        let categorySelected: Driver<MockCategory>
    }

    func transform(input: PersonalExpensesViewModel.Input) -> PersonalExpensesViewModel.Output {
        let sections = BehaviorRelay<[Section<CellViewModel>]>(value: [])

        obtainSections(refresh: true)
        .drive(sections)
        .disposed(by: bag)

        return Output(sections: sections.asDriverOnErrorJustComplete(),
                      categorySelected: categorySelected.asDriverOnErrorJustComplete())
    }
}

extension PersonalExpensesViewModel {
    private func obtainSections(refresh: Bool) -> Driver<[Section<CellViewModel>]> {
        section = Section(items: makeCellViewModels())

        return Driver<[Section<CellViewModel>]>.just([section])
    }

    private func makeCellViewModels() -> [CellViewModel] {
        var cellViewModels: [CellViewModel] = []

        cellViewModels.append(contentsOf: makeEmbedeGoalsCellViewModel())
        cellViewModels.append(contentsOf: makeSelectorCellViewModel())

        cellViewModels.append(contentsOf: makeCategoryCellViewModels())

        cellViewModels.append(contentsOf: makeAddButtonCellViewModels(for: .expenses))

        return cellViewModels
    }

    func makeEmbedeGoalsCellViewModel() -> [CellViewModel] {
        return [GoalsEmbededViewModel()]
    }

    func makeSelectorCellViewModel() -> [CellViewModel] {
        return [CategorySelectorCellViewModel(),
                EmptyCellViewModel(height: 17.0)]
    }

    func makeCategoryCellViewModels() -> [CellViewModel] {
        return [PersonalCategoryCellViewModel(for: MockCategory(title: "Еда",
                                                                iamge: "ic_expenses_eat",
                                                                sum: "3 560 руб",
                                                                lastPayment: "-325 руб"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Одежда",
                                        iamge: "ic_expenses_wear",
                                        sum: "2 130 руб",
                                        lastPayment: "-1 225 руб"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Алкоголь",
                                        iamge: "ic_expenses_icon",
                                        sum: "500 руб",
                                        lastPayment: "-300 руб"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Квартплата",
                                        iamge: "ic_expenses_home",
                                        sum: "30 560 руб",
                                        lastPayment: "-24 325 руб"), selectSubject: categorySelected),
        PersonalCategoryCellViewModel(for: MockCategory(title: "Займ",
                                        iamge: "ic_expenses_get",
                                        sum: "5 560 руб",
                                        lastPayment: "-3 022 руб"), selectSubject: categorySelected)
        ]
    }

    func makeAddButtonCellViewModels(for category: CategoryType) -> [CellViewModel] {
        return [AddCategoryButtonCellViewModel(for: category)]
    }
}
