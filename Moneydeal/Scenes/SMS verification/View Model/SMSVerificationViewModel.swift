//
//  SMSVerificationViewModel.swift
//  Moneydeal
//
//  Created by Arsenij Zorin on 18/11/2019.
//  Copyright © 2019 Arsenij Zorin. All rights reserved.
//

import Foundation
import Network
import RxSwift
import RxCocoa
import Domain

class SMSVerificationViewModel: ViewModel, ActivityTrackable, ErrorTrackable {
    let activityTracker = ActivityTracker()
    let errorTracker = ErrorTracker()

    private let networkingUseCaseProvider: Domain.NetworkingUseCaseProvider
    private let authUseCase: Domain.AuthorizationUseCase

    var authInfo: SMSAuthorizationInfo

    init(for authInfo: SMSAuthorizationInfo) {
        networkingUseCaseProvider = UseCaseProvider()
        authUseCase = networkingUseCaseProvider.makeAuthorizationUseCase()

        self.authInfo = authInfo

        super.init()
    }

    func checkPhone(for phone: String) -> Bool {
        let maxLendth: Int = Constants.Phone.russianPhoneLendth

        return phone.count >= maxLendth
    }
}

extension SMSVerificationViewModel: ViewModelType {
    struct Input {
        let code: Driver<String>
    }

    struct Output {
        let result: Driver<Bool>
    }

    func transform(input: SMSVerificationViewModel.Input) -> SMSVerificationViewModel.Output {
        let result = PublishSubject<Bool>()

        input.code.flatMap({ [weak self] code -> Driver<Bool> in
            guard let self = self else { return .never() }

            guard let fcmToken = AuthorizationService.shared.fcmToken else { return .never() }

            return self.authorize(phone: "+"+self.authInfo.phone.filter("01234567890".contains),
                                  token: fcmToken,
                                  googleToken: self.authInfo.token,
                                  code: code).asDriverOnErrorJustComplete()
        }).drive(result).disposed(by: bag)

        return .init(result: result.asDriverOnErrorJustComplete())
    }
}

extension SMSVerificationViewModel {
    func authorize(phone: String, token: String, googleToken: String, code: String) -> Observable<Bool> {
        return authUseCase.login(phone: phone,
                                 token: token,
                                 googleToken: googleToken,
                                 code: code)
            .trackActivity(activityTracker)
            .trackError(errorTracker)
            .asObservable()
            .flatMap({ login -> Observable<Bool> in
                let isSuccess = (login.token != nil)

                if let token = login.token {
                    let userInfo = Domain.AuthorizationInfo(with: token)
                    AuthorizationService.shared.userInfo = userInfo
                }

                return Observable.just(isSuccess)
            })
    }
}
