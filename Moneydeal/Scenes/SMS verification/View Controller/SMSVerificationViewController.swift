//
//  SMSVerificationViewController.swift
//  Moneydeal
//
//  Created by Arsenij Zorin on 18/11/2019.
//  Copyright © 2019 Arsenij Zorin. All rights reserved.
//

import RxCocoa
import RxSwift
import Domain
import InputMask

enum SMSVerificationRouter {
    static let toMain = InitialNavigationTransition(destination: MainTabBarViewController.self)
}

class SMSVerificationViewController: ViewController, ViewModelHolder, ViewConfigurator {
    var indicatorView: ActivityIndicatorView! = ActivityIndicatorView()

    var personInfo: SMSAuthorizationInfo?

    var viewModel: SMSVerificationViewModel? {
        didSet {
            bindViewModel()
            subscribeToErrors()
            setupBindings()
        }
    }

    let codeSubject = PublishSubject<String>()

    private var currentTextFieldIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func configure(with view: SMSVerificationView) {
        guard let phone = personInfo?.phone else { return }

        view.configure(input: phone)

        view.codeTextFields.forEach {
            $0.delegate = self
            $0.viewDelegate = self
        }

        view.codeTextFields.first?.becomeFirstResponder()
    }

    func setupBindings() {
        guard let vm = viewModel else { return }

        let output = vm.transform(input: .init(code: codeSubject.asDriverOnErrorJustComplete()))

        output.result
        .filter({ $0 })
        .asTransitionable()
        .transit(to: SMSVerificationRouter.toMain)
        .disposed(by: bag)
    }
}

extension SMSVerificationViewController: TransitionPresenter, TransitionableInput {
    func onTransit(input: SMSAuthorizationInfo) {
        personInfo = input

        configure(with: mainView)

        viewModel = SMSVerificationViewModel(for: input)
    }
}

extension SMSVerificationViewController: ErrorPresentable {}
extension SMSVerificationViewController: ActivityIndicatorContainer {}
extension SMSVerificationViewController: AutoBinder {}

extension SMSVerificationViewController: PhoneVerificationControlDelegate {
    func needBecomeFirstResponder(for tag: Int) -> UITextField? {
        guard tag >= 0 && tag < mainView.codeTextFields.count else { return nil }

        mainView.codeTextFields[tag].becomeFirstResponder()

        return mainView.codeTextFields[tag]
    }

    func onComplete() {
        let code = mainView.codeTextFields.reduce("") { $0 + ($1.text ?? "") }

        codeSubject.onNext(code)
    }
}

protocol PhoneVerificationControlDelegate: class {
    func onComplete()
    func needBecomeFirstResponder(for tag: Int) -> UITextField?
}

class PhoneVerificationTextField: UITextField {
    weak var viewDelegate: PhoneVerificationControlDelegate?

    override func deleteBackward() {
        super.deleteBackward()

        let previousTag = self.tag - 1
        viewDelegate?.needBecomeFirstResponder(for: previousTag)?.text = ""
    }

}

extension SMSVerificationViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {

        let textFieldCount = textField.text?.count ?? 0

        // Сlosure
        let setValueAndMoveForward = {
            textField.text = string
            let nextTag = textField.tag + 1

            _ = self.needBecomeFirstResponder(for: nextTag)

            if textField.tag == 5 {
                self.onComplete()
            }

        }

        // Сlosure
        let clearValueAndMoveBack = {
            textField.text = ""
            let previousTag = textField.tag - 1

            _ = self.needBecomeFirstResponder(for: previousTag)
        }

        if textFieldCount < 1 && string.isNotEmpty {

            setValueAndMoveForward()

            return false

        } else if textFieldCount >= 1 && string.isEmpty {

            clearValueAndMoveBack()
            return false

        } else if textFieldCount >= 1 && string.isNotEmpty {

            let nextTag = textField.tag + 1

            _ = needBecomeFirstResponder(for: nextTag)

            if textField.tag >= 3 {
                onComplete()
            }

            return false
        }

        return true
    }

}
