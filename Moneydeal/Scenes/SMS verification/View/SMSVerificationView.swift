//
//  SMSVerificationView.swift
//  Moneydeal
//
//  Created by Arsenij Zorin on 18/11/2019.
//  Copyright © 2019 Arsenij Zorin. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Utils

class SMSVerificationView: View {
    let contentView = UIScrollView()
    fileprivate let rootFlexContainer = UIView()

    let phoneLabel = UILabel()

    lazy var codeViews: [UIView] = {
        var codeViews = [UIView]()
        for _ in 1...6 {
            codeViews.append(UIView())
        }
        return codeViews
    }()

    lazy var codeTextFields: [PhoneVerificationTextField] = {
        var codeTextFields = [PhoneVerificationTextField]()
        for _ in 1...6 {
            codeTextFields.append(PhoneVerificationTextField())
        }
        return codeTextFields
    }()

    let infoLabel = UILabel()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Behavior for layout
    override func layoutSubviews() {
        super.layoutSubviews()

        // Layout the contentView & rootFlexContainer using PinLayout
        contentView.pin.all(pin.safeArea)
        rootFlexContainer.pin.top().horizontally()

        // Let the flexbox container layout itself and adjust the height
        rootFlexContainer.flex.layout(mode: .adjustHeight)

        // Adjust the scrollview contentSize
        contentView.contentSize = rootFlexContainer.frame.size
    }
}

extension SMSVerificationView {
    /// Setup all styles for UI elements
    func setup() {
        setupPhoneLabel()
        setupCodeViews()
        setupCodeTextFields()
        setupInfoLabel()

        backgroundColor = .white
    }

    /// Setup styles for info label
    func setupInfoLabel() {
        infoLabel.textColor = .black

        infoLabel.font = UIFont(name: "GothamPro-Bold", size: 21)

        infoLabel.numberOfLines = 0
        infoLabel.textAlignment = .center

        infoLabel.text = "TTL_SMSVERIFICATION_INFO_TEXT".localized
    }

    /// Setup styles for code views

    func setupCodeViews() {
        let cornerRadius: CGFloat = 17.5

        codeViews.forEach {
            $0.clipsToBounds = true
            $0.layer.cornerRadius = cornerRadius

            $0.backgroundColor = UIColor(red: 0.904, green: 0.904, blue: 0.904, alpha: 1)
        }
    }

    /// Setup styles for code text fields
    func setupCodeTextFields() {
        for (idx, textField) in codeTextFields.enumerated() {
            textField.textAlignment = .center

            textField.keyboardType = .decimalPad

            textField.backgroundColor = .clear
            textField.tintColor = .clear

            textField.tag = idx
        }
    }

    /// Setup styles for enter phone label
    func setupPhoneLabel() {
        phoneLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)

        phoneLabel.font = UIFont(name: "GothamPro", size: 12)

        phoneLabel.numberOfLines = 0
        phoneLabel.textAlignment = .center
    }

}

extension SMSVerificationView: BaseViewInput {
    /// Configure layout for view
    func configure(input phoneNumber: String) {
        phoneLabel.text = String(format: "TTL_SMSVERIFICATION_PHONE_TEXT".localized, phoneNumber)

        configure()
    }

    func configure() {
        // Configure layout for root container
        rootFlexContainer
        .flex
        .direction(.column)
        .alignItems(.center)
        .define { flex in
            // Configure layout for each UI element
            layoutInfoLabel(for: flex)
            layoutCodeViews(for: flex)
//            layoutCodeTextFields(for: flex)
            layoutPhoneLabel(for: flex)
        }

        contentView.addSubview(rootFlexContainer)

        addSubview(contentView)
    }

    /// Layout for info label
    func layoutInfoLabel(for flex: Flex) {
        let marginTop: CGFloat = 20.0

        let horizontalMargin: CGFloat = 24.0

        flex.addItem(infoLabel).marginTop(marginTop).marginHorizontal(horizontalMargin)
    }

    /// Layout for code views
    func layoutCodeViews(for flex: Flex) {
        let sizes: CGSize = CGSize(width: 35.0, height: 35.0)

        let topMargin: CGFloat = 57.0

        flex.addItem()
            .justifyContent(.spaceBetween)
            .marginTop(topMargin)
            .width(72%)
            .define { flex in
            for (idx, codeView) in codeViews.enumerated() {
                flex.direction(.row).addItem(codeView).minWidth(sizes.width).minHeight(sizes.height).define { flex in
                    flex.addItem(codeTextFields[idx]).minWidth(sizes.width).minHeight(sizes.height)
                }
            }
        }
    }

    /// Layout for enter phone label
    func layoutPhoneLabel(for flex: Flex) {
        let marginTop: CGFloat = 57.0

        let horizontalMargin: CGFloat = 24.0

        flex.addItem(phoneLabel).marginTop(marginTop).marginHorizontal(horizontalMargin)
    }

}
