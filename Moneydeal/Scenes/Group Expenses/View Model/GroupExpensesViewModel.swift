//
//  GroupExpensesViewModel.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import Storage
import Utils
import RxSwift
import Network
import RxCocoa

class GroupExpensesViewModel: ViewModel {

    private var section: Section<CellViewModel> = Section(items: [])

}

extension GroupExpensesViewModel: ViewModelType {
    struct Input {
    }

    struct Output {
        let sections: Driver<[Section<CellViewModel>]>
    }

    func transform(input: GroupExpensesViewModel.Input) -> GroupExpensesViewModel.Output {
        let sections = BehaviorRelay<[Section<CellViewModel>]>(value: [])

        obtainGroupExpenses(refresh: true)
        .drive(sections)
        .disposed(by: bag)

        return Output(sections: sections.asDriverOnErrorJustComplete())
    }
}

extension GroupExpensesViewModel {
    private func obtainGroupExpenses(refresh: Bool) -> Driver<[Section<CellViewModel>]> {
        section = Section(items: makeCellViewModels())

        return Driver<[Section<CellViewModel>]>.just([section])
    }

    private func makeCellViewModels() -> [CellViewModel] {
        var cellViewModels: [CellViewModel] = []

        cellViewModels.append(contentsOf: makeAddGroupExpensesCellViewModel())
        cellViewModels.append(contentsOf: makeGroupExpensesCellViewModel())

        return cellViewModels
    }

    func makeAddGroupExpensesCellViewModel() -> [CellViewModel] {
        return [AddGroupExpensesCellViewModel()]
    }

    func makeGroupExpensesCellViewModel() -> [CellViewModel] {
        return [GroupExpensesCellViewModel()]
    }
}
