//
//  AddGroupExpensesCollectionViewCell.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class AddGroupExpensesCollectionViewCell: CollectionViewCell, CellAutoBinder, ViewModelHolder {
    let button = UIButton()
    let nameLabel = UILabel()

    var viewModel: AddGroupExpensesCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }

        layoutSubviews()
    }

    override func setup() {
        setupCellStyles()

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.addItem().direction(.row).define { flex in
            layoutButton(for: flex)
            layoutTextFirld(for: flex)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension AddGroupExpensesCollectionViewCell {
    /// Layout for goal name label
    func setupCellStyles() {
        setupContentView()
        setupButton()
        setupTextField()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let cellWidth: CGFloat = UIScreen.main.bounds.width - 32.0
        let cellHeight: CGFloat = 62.0
        let cornerRadius: CGFloat = 20.0

        contentView.flex.minWidth(cellWidth)
        contentView.flex.minHeight(cellHeight)
        //contentView.flex.markDirty()

        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = cornerRadius

        // Setup shadow
        contentView.layer.shadowColor = UIColor(red: 0.501, green: 0.501, blue: 0.738, alpha: 0.36).cgColor
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        contentView.layer.shadowRadius = 5

        contentView.layer.shouldRasterize = true

        contentView.layer.rasterizationScale = UIScreen.main.scale
    }

    /// Setup styles for button
    func setupButton() {
        button.setImage(UIImage(named: "AddButton"), for: .normal)
    }

    /// Setup styles for text field
    func setupTextField() {
        let color = UIColor(red: 0.651, green: 0.667, blue: 0.706, alpha: 1)

        nameLabel.textColor = color

        nameLabel.text = "TTL_GROUP_EXP_ADD_TITLE".localized

        nameLabel.font = UIFont(name: "GothamPro", size: 12)
    }
}

extension AddGroupExpensesCollectionViewCell {
    /// Layout for button
    func layoutButton(for flex: Flex) {
        flex.addItem(button).size(40.0).marginTop(12.0).marginLeft(14.0)
    }

    /// Layout for text field
    func layoutTextFirld(for flex: Flex) {
        flex.addItem(nameLabel).marginTop(12.0).marginLeft(7.0).marginRight(14.0)
    }
}
