//
//  GroupExpensesCollectionViewCell.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import FlexLayout

class GroupExpensesCollectionViewCell: CollectionViewCell, CellAutoBinder, ViewModelHolder {
    let imageView = UIImageView()
    let totalAmountLabel = UILabel()
    let friendsCountLabel = UILabel()
    let nameLabel = UILabel()

    var viewModel: GroupExpensesCellViewModel? {
        didSet {
            configure()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layout()
    }

    override func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }

    func configure() {
        guard let vm = viewModel else { return }
    }

    override func setup() {
        setupCellStyles()
        setupImageView()
        setupImageView()
        setupNameLabel()
        setupFriendsCountLabel()
        setupTotalAmountLabel()

        backgroundColor = .clear
    }

    override func setupLayout() {
        contentView.flex.addItem().direction(.row).marginTop(18.0).define { flex in
            flex.addItem().define { flex in
                layoutImageView(for: flex)
            }
            flex.addItem().marginLeft(27.0).define { flex in
                layoutNameLabel(for: flex)
                layoutFriendsCountLabel(for: flex)
            }
            flex.addItem().position(.absolute).right(18.0).define { flex in
                layoutTotalAmountLabel(for: flex)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension GroupExpensesCollectionViewCell {
    /// Setup all styles for UI elements
    func setupCellStyles() {
        setupContentView()

        backgroundColor = .white
    }

    /// Setup styles for content view
    func setupContentView() {
        let headerWidth: CGFloat = UIScreen.main.bounds.width

        contentView.flex.minWidth(headerWidth)
        contentView.flex.markDirty()

        contentView.backgroundColor = .clear
    }

    /// Setup styles for image view
    func setupImageView() {
        imageView.image = UIImage(named: "ic_expenses_apple")
    }

    /// Setup styles for name label
    func setupNameLabel() {
        nameLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        nameLabel.font = UIFont(name: "GothamPro-Medium", size: 14)

        nameLabel.text = "ДОСТОЖРИСТО БАНК"

        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .left
    }

    /// Setup styles for friends count label
    func setupFriendsCountLabel() {
        friendsCountLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)

        friendsCountLabel.font = UIFont(name: "GothamPro", size: 12)

        friendsCountLabel.text = "3 друга"

        friendsCountLabel.numberOfLines = 0
        friendsCountLabel.textAlignment = .left
    }

    /// Setup styles fot total amount label
    func setupTotalAmountLabel() {
        totalAmountLabel.textColor = UIColor(red: 0.133, green: 0.133, blue: 0.133, alpha: 1)

        totalAmountLabel.font = UIFont(name: "GothamPro-Medium", size: 14)

        totalAmountLabel.text = "₽65"

        totalAmountLabel.numberOfLines = 0
        totalAmountLabel.textAlignment = .right
    }
}

extension GroupExpensesCollectionViewCell {
    /// Layout for image view
    func layoutImageView(for flex: Flex) {
        let sizes: CGSize = CGSize(width: 20.0, height: 20.0)
        flex.addItem(imageView).marginLeft(36.0).marginTop(8).width(sizes.width).height(sizes.height)
    }

    /// Layout for name label
    func layoutNameLabel(for flex: Flex) {
        flex.addItem(nameLabel)
    }

    /// Layout for friends count label
    func layoutFriendsCountLabel(for flex: Flex) {
        flex.addItem(friendsCountLabel).marginTop(15.0)
    }

    /// Layout for total amount label
    func layoutTotalAmountLabel(for flex: Flex) {
        flex.addItem(totalAmountLabel).marginTop(15.0)
    }
}
