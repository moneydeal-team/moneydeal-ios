//
//  GroupExpensesView.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class GroupExpensesView: View, ViewScrollable {
    var scrollView: UIScrollView {
        return self.collectionView
    }

    let flowLayout = UICollectionViewFlowLayout()

    lazy var collectionView: UICollectionView = {
        self.flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        self.flowLayout.minimumLineSpacing = 13
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.flowLayout)
        collectionView.backgroundColor = .white
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 13, right: 0)

        return collectionView
    }()

    let refreshControl = UIRefreshControl()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        collectionView.refreshControl = refreshControl
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        collectionView.pin.all()
    }

}

extension GroupExpensesView: BaseView {
    func configure() {
        addSubview(collectionView)
    }
}
