//
//  GroupExpensesViewController.swift
//  Moneydeal
//
//  Created by Никита Чужиков on 15/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GroupExpensesViewController: CollectionViewController, ViewConfigurator, ViewModelHolder {

    var viewModel: GroupExpensesViewModel? = GroupExpensesViewModel()

    var dataSource: CollectionViewSectionedDataSource<Section<CellViewModel>>?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "TTL_GROUP_EXP_TITLE".localized
    }

    func configure(with view: GoalsView) {
        view.configure()
        configureBindings()
    }

    func configureBindings() {
        guard let vm = viewModel else { return }

        let output = vm.transform(input: .init())

        dataSource = CollectionViewSectionedDataSource(cellMap: cellMap)

        output.sections.drive(mainView.collectionView.rx.items(dataSource: dataSource!)).disposed(by: bag)
    }

    override func configureCellMap() -> CellMap {
        return [
            AddGroupExpensesCellViewModel.className(): AddGroupExpensesCollectionViewCell.self,
            GroupExpensesCellViewModel.className(): GroupExpensesCollectionViewCell.self
        ]
    }
}

extension GroupExpensesViewController: MDNavigationBarHolder {
    var isLargeTitleMode: Bool {
        true
    }

    var state: MDNavigationBarState {
        .display
    }
}
