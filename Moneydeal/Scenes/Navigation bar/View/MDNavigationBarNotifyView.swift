//
//  MDNavigationBarNotifyView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 07/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import PinLayout
import FlexLayout
import Utils

class MDNavigationBarNotifyView: View {

    enum Size {
        static let notifyContainerSize = CGSize(width: 35.0, height: 35.0)
        static let notifySize = CGSize(width: 27.0, height: 27.0)
    }

    fileprivate let rootFlexContainer = UIView()

    let notifyImageView = UIImageView()
    let notifyImageViewContainer = UIView()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Behavior for layout
    override func layoutSubviews() {
        super.layoutSubviews()

        rootFlexContainer.pin.all()

        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
}

extension MDNavigationBarNotifyView {
    /// Setup all styles for UI elements
    func setup() {
        setupAvatarImageView()

        backgroundColor = .clear
    }

    func setupAvatarImageView() {
        notifyImageView.image = UIImage(named: "ic_navigationbar_notify")
    }
}

extension MDNavigationBarNotifyView: BaseView {
    /// Configure layout for view
    func configure() {

        // Configure layout for root container
        rootFlexContainer
        .flex
        .direction(.column)
        .alignItems(.center)
        .define { flex in
            // Configure layout for each UI element
            layoutAvatarImageView(for: flex)
        }

        addSubview(rootFlexContainer)
    }

    func layoutAvatarImageView(for flex: Flex) {
        flex.addItem().size(Size.notifyContainerSize).justifyContent(.center).direction(.column).define { flex in
            flex.addItem().width(100%).direction(.row).justifyContent(.center).define { flex in
                flex.addItem(notifyImageView).size(Size.notifySize)
            }
        }
    }
}
