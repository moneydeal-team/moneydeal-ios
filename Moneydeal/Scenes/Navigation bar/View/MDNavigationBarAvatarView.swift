//
//  MDNavigationBarAvatarView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 07/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import PinLayout
import Domain
import FlexLayout
import Utils
import Storage

class MDNavigationBarAvatarView: View {

    let dataBase = RealmDAO<UserInfo>()

    enum Size {
        static let avatarSize = CGSize(width: 35.0, height: 35.0)
    }

    fileprivate let rootFlexContainer = UIView()

    let containerAvatarView = UIView()
    let avatarImageView = UIImageView()

    init() {
        super.init(frame: .zero)

        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Behavior for layout
    override func layoutSubviews() {
        super.layoutSubviews()

        rootFlexContainer.pin.all()

        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
}

extension MDNavigationBarAvatarView {
    /// Setup all styles for UI elements
    func setup() {
        setupContainerAvatarView()
        setupAvatarImageView()
        setupAvatarObserver()

        backgroundColor = .clear
    }

    func setupContainerAvatarView() {

    }

    func setupAvatarObserver() {
        dataBase.collection().asObservable()
        .mapToVoid()
        .asDriverOnErrorJustComplete()
        .drive(onNext: { [weak self] _ in
            guard let self = self else { return }

            guard let avatar = self.dataBase.collection().last?.translate(UserInfo.self)?.avatar else { return }

            guard let imageURL = URL(string: Constants.Dispather.imageServer+avatar) else { return }

            self.avatarImageView.kf.setImage(with: imageURL,
                                        placeholder: UIImage(named: "is_settings_avatar_default"),
                                        options: [.backgroundDecode],
                completionHandler: ({ [weak self] (_, _, _, _) in
                    guard let self = self else { return }

                    self.setupAvatarImageView()
            }), auth: true)

        }).disposed(by: bag)
    }

    func setupAvatarImageView() {
        var avatarImage = avatarImageView.image ?? UIImage(named: "is_settings_avatar_default")

        let minSize = min(avatarImage?.size.height ?? 0,
                          avatarImage?.size.width ?? 0)

        avatarImage = avatarImage?.crop(rect: CGRect(origin: CGPoint(x: 0, y: 0),
                                                     size: CGSize(width: minSize,
                                                                  height: minSize)))

        avatarImage = avatarImage?.roundedImage()

        avatarImageView.image = avatarImage

        avatarImageView.layer.borderWidth = 0
    }
}

extension MDNavigationBarAvatarView: BaseView {
    /// Configure layout for view
    func configure() {

        // Configure layout for root container
        rootFlexContainer
        .flex
        .direction(.column)
        .alignItems(.center)
        .define { flex in
            // Configure layout for each UI element
            layoutAvatarImageView(for: flex)
        }

        addSubview(rootFlexContainer)
    }

    func layoutAvatarImageView(for flex: Flex) {
        flex.addItem(avatarImageView).size(Size.avatarSize)
    }
}

extension UIImage {
    func crop(rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x *= self.scale
        rect.origin.y *= self.scale
        rect.size.width *= self.scale
        rect.size.height *= self.scale

        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return image
    }

    func roundedImage() -> UIImage {
        let imageView: UIImageView = UIImageView(image: self)

        let layer = imageView.layer

        layer.masksToBounds = true
        layer.cornerRadius = imageView.frame.width / 2

        UIGraphicsBeginImageContext(imageView.bounds.size)

        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return roundedImage ?? UIImage()
    }
}
