//
//  MDNavigationBarHolder.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 07/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import PinLayout
import RxCocoa

enum GlobalRouter {
    static let toProfile = PresentPopupTransition(destination: SettingsViewController.self)
}

private struct MDNavigationBarHolderConst {
    struct Notify {
        /// Image height/width for Large NavBar state
        static let ImageSizeForLargeState: CGFloat = 27

        /// Image height/width for Small NavBar state
        static let ImageSizeForSmallState: CGFloat = 20
    }

    struct Avatar {
        /// Image height/width for Large NavBar state
        static let ImageSizeForLargeState: CGFloat = 35
        /// Image height/width for Small NavBar state
        static let ImageSizeForSmallState: CGFloat = 28
    }

    /// Margin from right anchor of safe area to right anchor of Image
    static let ImageRightMargin: CGFloat = 16
    /// Margin from bottom anchor of NavBar to bottom anchor of Image for Large NavBar state
    static let ImageBottomMarginForLargeState: CGFloat = 12
    /// Margin from bottom anchor of NavBar to bottom anchor of Image for Small NavBar state
    static let ImageBottomMarginForSmallState: CGFloat = 6
    /// Height of NavBar for Small state.
    static let NavBarHeightSmallState: CGFloat = 44
    /// Height of NavBar for Large state.
    static let NavBarHeightLargeState: CGFloat = 96.5
}

enum MDNavigationBarState {
    case display
    case hide
}

protocol MDNavigationBarHolder {
    var state: MDNavigationBarState { get }
    var isLargeTitleMode: Bool { get }

    func configureMDNavigationBar()
}

//extension ViewController {
//    struct MDNavigationBarStateHolder {
//        static var _state: MDNavigationBarState = .display
//    }
//}

class CombineView: UIView {
   // let tapGesture = UITapGestureRecognizer()
}

extension MDNavigationBarHolder where Self: ViewController & ViewConfigurator {
    func configureMDNavigationBar() {
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        navigationBar.barTintColor = .white
        navigationBar.shadowImage = UIColor.white.as1ptImage()

        setLargeTitleDisplayMode(isLargeTitleMode ? .always : .never)

        navigationBar.subviews.forEach { subView in
            guard let combineView = subView as? CombineView else { return }

            combineView.removeFromSuperview()
        }

        guard state == .display else { return }

        let avatarView = MDNavigationBarAvatarView()
        avatarView.configure()

        avatarView.translatesAutoresizingMaskIntoConstraints = false

        let notifyView = MDNavigationBarNotifyView()
        notifyView.configure()

        notifyView.translatesAutoresizingMaskIntoConstraints = false

        let combineView = CombineView()
        combineView.translatesAutoresizingMaskIntoConstraints = false

        navigationBar.addSubview(combineView)

//        avatarView.addGestureRecognizer(combineView.tapGesture)
//        notifyView.addGestureRecognizer(combineView.tapGesture)

        let tapGesture = UITapGestureRecognizer()

        navigationBar.addGestureRecognizer(tapGesture)

        let tapInsideProfile = tapGesture.rx.event.throttleTapInMainScheduler().asDriverOnErrorJustComplete()

        tapInsideProfile.drive(onNext: { _ in
            print("[DEBUG] TAP")
        }).disposed(by: bag)

        tapInsideProfile.asTransitionable().transit(to: GlobalRouter.toProfile).disposed(by: bag)

        NSLayoutConstraint.activate([
            combineView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor,
                                              constant: -MDNavigationBarHolderConst.ImageRightMargin),
            combineView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor,
                                               constant: -MDNavigationBarHolderConst.ImageBottomMarginForLargeState),
            combineView.heightAnchor.constraint(equalToConstant: MDNavigationBarHolderConst.Avatar.ImageSizeForLargeState)
        ])

        combineView.addSubview(avatarView)
        combineView.addSubview(notifyView)

        NSLayoutConstraint.activate([
            notifyView.rightAnchor.constraint(equalTo: combineView.rightAnchor),
            notifyView.bottomAnchor.constraint(equalTo: combineView.bottomAnchor),
            notifyView.topAnchor.constraint(equalTo: combineView.topAnchor),
            notifyView.widthAnchor.constraint(equalTo: combineView.heightAnchor)
        ])

        NSLayoutConstraint.activate([
            avatarView.rightAnchor.constraint(equalTo: notifyView.leftAnchor,
                                              constant: -MDNavigationBarHolderConst.ImageRightMargin),
            avatarView.bottomAnchor.constraint(equalTo: combineView.bottomAnchor),
            avatarView.topAnchor.constraint(equalTo: combineView.topAnchor),
            avatarView.widthAnchor.constraint(equalTo: combineView.heightAnchor)
        ])

        if let mainView = self.mainView as? ViewScrollable & View {
            mainView.scrollView.rx.contentOffset.observeOn(MainScheduler.asyncInstance)
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] _ in
                guard let self = self else { return }

                combineView.transform = self.getTransformForAvatar(for: navigationBar.frame.height)

                combineView.setNeedsLayout()
                combineView.layoutSubviews()

            }).disposed(by: mainView.bag)
        }
    }
}

extension MDNavigationBarHolder where Self: ViewController {
    private func getTransformForAvatar(for height: CGFloat) -> CGAffineTransform {
        let coeff: CGFloat = {
            let delta = height - MDNavigationBarHolderConst.NavBarHeightSmallState
            let heightDifferenceBetweenStates = (MDNavigationBarHolderConst.NavBarHeightLargeState - MDNavigationBarHolderConst.NavBarHeightSmallState)
            return delta / heightDifferenceBetweenStates
        }()

        let factor = MDNavigationBarHolderConst.Avatar.ImageSizeForSmallState / MDNavigationBarHolderConst.Avatar.ImageSizeForLargeState

        let scale: CGFloat = {
            let sizeAddendumFactor = coeff * (1.0 - factor)
            return min(1.0, sizeAddendumFactor + factor)
        }()

        // Value of difference between icons for large and small states
        let sizeDiff = MDNavigationBarHolderConst.Avatar.ImageSizeForLargeState * (1.0 - factor) // 8.0
        let yTranslation: CGFloat = {
            /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = MDNavigationBarHolderConst.ImageBottomMarginForLargeState - MDNavigationBarHolderConst.ImageBottomMarginForSmallState + sizeDiff
            return max(0, min(maxYTranslation, (maxYTranslation - coeff * (MDNavigationBarHolderConst.ImageBottomMarginForSmallState + sizeDiff))))
        }()

        let xTranslation = max(0, sizeDiff - coeff * sizeDiff)

        return CGAffineTransform.identity.scaledBy(x: scale, y: scale).translatedBy(x: xTranslation, y: yTranslation)
    }
}

extension UIColor {

    /// Converts this `UIColor` instance to a 1x1 `UIImage` instance and returns it.
    ///
    /// - Returns: `self` as a 1x1 `UIImage`.
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIViewController {

    func setLargeTitleDisplayMode(_ largeTitleDisplayMode: UINavigationItem.LargeTitleDisplayMode) {
        switch largeTitleDisplayMode {
        case .automatic:
              guard let navigationController = navigationController else { break }
            if let index = navigationController.children.firstIndex(of: self) {
                setLargeTitleDisplayMode(index == 0 ? .always : .never)
            } else {
                setLargeTitleDisplayMode(.always)
            }
        case .always, .never:
            navigationItem.largeTitleDisplayMode = largeTitleDisplayMode
            // Even when .never, needs to be true otherwise animation will be broken on iOS11, 12, 13
            navigationController?.navigationBar.prefersLargeTitles = true
        @unknown default:
            assertionFailure("\(#function): Missing handler for \(largeTitleDisplayMode)")
        }
    }
}
