//
//  Constants.swift
//  Cosmo
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

enum KeychainKeys {
    public static let token = "MDAuthorizationToken"
    public static let fcmToken = "MDFCMToken"
}

enum Constants {
    enum Phone {
        public static let russianPhoneLendth = 18
    }

    enum SMS {
        public static let maxCodeLendth = 6
    }

    enum Pattern {
        public static let phoneMask = "{+7} ([000]) [000]-[00]-[00]"
    }

    enum Links {
        public static let term = "https://mail.ru"
        public static let privacyPolicy = "https://hh.ru"
    }

    enum Default {
        public static let goalImageURL = "https://upload.wikimedia.org/wikipedia/ru/thumb/f/fa/MailLogo.svg/1200px-MailLogo.svg.png"
    }

    enum Dispather {
        public static let imageServer = "https://balancer.moneydeal.kkapp.ru/api/v1/file/download/"
    }
}
