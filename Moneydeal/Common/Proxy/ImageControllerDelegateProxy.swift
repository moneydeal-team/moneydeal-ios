//
//  ImageControllerDelegateProxy.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import RxSwift
import RxCocoa
import Domain
import MobileCoreServices
import AVFoundation
import Photos
import AssetsLibrary

// swiftlint:disable:next line_length
public class ImageControllerDelegateProxy: DelegateProxy<UIImagePickerController, (UIImagePickerControllerDelegate & UINavigationControllerDelegate)>, DelegateProxyType, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let fileSubject: PublishSubject<OutgoingFile> = PublishSubject()
    let bag = DisposeBag()

    /// Typed parent object.
    weak private(set) var pickerController: UIImagePickerController?

    public init(pickerController: ParentObject) {
        self.pickerController = pickerController
        super.init(parentObject: pickerController, delegateProxy: ImageControllerDelegateProxy.self)
    }

    public static func currentDelegate(for object: UIImagePickerController) -> (UIImagePickerControllerDelegate & UINavigationControllerDelegate)? {
        return object.delegate
    }

    public static func setCurrentDelegate(_ delegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate)?, to object: UIImagePickerController) {
        object.delegate = delegate
    }

    public static func registerKnownImplementations() {
        self.register { ImageControllerDelegateProxy(pickerController: $0) }
    }

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard picker.sourceType != .camera else {
            didFinishPickingMediaFromCamera(picker, info: info)
            return
        }

        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String
        let type = (mediaType == String(kUTTypeImage) ? FileType.image : .video)
        let URLkey = (type == .image ? UIImagePickerController.InfoKey.imageURL : UIImagePickerController.InfoKey.mediaURL)

        var thumbnail: UIImage?

        guard let mediaURL = info[URLkey] as? URL else {
            fileSubject.onError(Domain.Error.appError(message: "TTL_ERROR_FILE_NOT_LOADED".localized))
            picker.dismiss(animated: true)
            return
        }

        if type != .image {
            thumbnail = AVAssetImageGenerator.thumbnail(for: mediaURL)
        }

        if let file = OutgoingFile(mediaURL: mediaURL, type: type, thumbnail: thumbnail) {
            fileSubject.onNext(file)
        }

        picker.dismiss(animated: true, completion: nil)
    }

    private func didFinishPickingMediaFromCamera(_ picker: UIImagePickerController, info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            didFinishPickingWithError()
            return
        }

        PHPhotoLibrary.requestAuthorization({ [weak self] status in
            switch status {
            case .authorized:
                self?.giveImageWithSaveInGallery(image)
            default:
                self?.giveImageWithoutSave(image)
            }
        })
    }

    private func didFinishPickingWithError() {
        fileSubject.onError(Domain.Error.appError(message: "TTL_ERROR_FILE_NOT_LOADED".localized))

        DispatchQueue.main.async {
            self.pickerController?.dismiss(animated: true, completion: nil)
        }
    }

    private func giveImageWithSaveInGallery(_ image: UIImage) {
        PHPhotoLibrary.shared().rx.saveImage(image).map({ $1 }).subscribe { [weak self] event in
            switch event {
            case .success(let url):
                guard let file = OutgoingFile(mediaURL: url, type: .image) else {
                    self?.fileSubject.onError(Domain.Error.appError(message: "TTL_UPLOAD_FILE_CREATE_FAILED".localized))
                    break
                }
                self?.fileSubject.onNext(file)
            case .error(let error):
                self?.fileSubject.onError(Domain.Error.appError(message: String(format: "TTL_PHOTO_LIBRARY_SAVE_FAILED_FORMAT".localized, error.localizedDescription)))
                break
            }
            self?.pickerController?.dismiss(animated: true, completion: nil)
            }.disposed(by: bag)
    }

    private func giveImageWithoutSave(_ image: UIImage) {
        guard let file = OutgoingFile(image: image, type: .image) else {
            fileSubject.onError(Domain.Error.appError(message: "TTL_UPLOAD_FILE_CREATE_FAILED".localized))
            close()
            return
        }
        fileSubject.onNext(file)
        close()
    }

    private func close() {
        DispatchQueue.main.async {
            self.pickerController?.dismiss(animated: true, completion: nil)
        }
    }
}
