//
//  UIVerticalTopLabel.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 05/10/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class UIVerticalTopLabel: UILabel {

    override func drawText(in rect: CGRect) {
        guard let labelText = text else {  return super.drawText(in: rect) }

        // force unwarp because uilabel always has font
        let attributedText = NSAttributedString(string: labelText,
                                                attributes: [NSAttributedString.Key.font: font!])

        var newRect = rect

        newRect.size.height = attributedText.boundingRect(with: rect.size,
                                                          options: .usesLineFragmentOrigin,
                                                          context: nil).size.height

        if numberOfLines != 0 {
            newRect.size.height = min(newRect.size.height, CGFloat(numberOfLines) * font.lineHeight)
        }

        super.drawText(in: newRect)
    }

}

class BottomAlignedLabel: UILabel {
    override func drawText(in rect: CGRect) {
        guard let labelText = text else {  return super.drawText(in: rect) }

        let stringTextAsNSString = labelText as NSString
        let labelStringSize = stringTextAsNSString.boundingRect(with: rect.size,
                                                                options: .usesLineFragmentOrigin,
                                                                attributes: [NSAttributedString.Key.font: font!],
                                                                context: nil).size
        super.drawText(in: CGRect(x: 0,
                                  y: rect.size.height - labelStringSize.height,
                                  width: self.frame.width,
                                  height: ceil(labelStringSize.height)))
    }
}
