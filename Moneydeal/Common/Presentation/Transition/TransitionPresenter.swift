//
//  TransitionPresenter.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import AppRouter
import UIKit

protocol TransitionPresenter {}

extension TransitionPresenter where Self: UIViewController {

    static func createPresenter() -> AppRouter.Presenter.Configuration<Self> {
        return presenter().from {
            return Self.init()
        }
    }
}

extension TransitionPresenter where Self: UINavigationController {

    static func createPresenter() -> AppRouter.Presenter.Configuration<Self> {
        return presenter().from {
            return Self.init()
        }
    }
}
