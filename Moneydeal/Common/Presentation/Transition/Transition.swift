//
//  Transition.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import AppRouter

typealias ViewControllerTransitionType = UIViewController & TransitionPresenter

protocol Transitionable: TransitionPresenter {
    func onTransit()
}

protocol TransitionableInput: TransitionPresenter {
    associatedtype InputParameters

    func onTransit(input: InputParameters)
}

protocol Transition {
    associatedtype Destination

    var destination: Destination.Type { get }
}

extension Transitionable where Self: UIViewController {

    func onTransit() { }
}

private extension Transition where Destination: ViewControllerTransitionType {

    func asConcreteTransition() -> ViewControllerTransitionBase<Destination> {
        // swiftlint:disable:next force_cast
        return self as! ViewControllerTransitionBase<Destination>
    }
}

extension Transition where Destination: TransitionableInput & ViewControllerTransitionType {

    func go(_ params: Destination.InputParameters) {
        asConcreteTransition().transit(with: destination.createPresenter().configure({ (dest) in
            dest.onTransit(input: params)
        }))
    }
}

extension Transition where Destination: Transitionable & ViewControllerTransitionType {

    func go() {
        asConcreteTransition().transit(with: destination.createPresenter().configure({ (dest) in
            dest.onTransit()
        }))
    }
}

// MARK: - Transitions

class ViewControllerTransitionBase<T: ViewControllerTransitionType>: Transition {
    typealias Destination = T

    let destination: T.Type
    fileprivate let configuration: ((AppRouter.Presenter.Configuration<T>) -> Void)?

    init(destination: T.Type, configuration: ((AppRouter.Presenter.Configuration<T>) -> Void)? = nil) {
        self.destination = destination
        self.configuration = configuration
    }

    fileprivate func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        guard self.configuration != nil else {
            return
        }

        self.configuration!(configuration)
    }
}

class CustomTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

}

class PushTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override fileprivate func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)

        do {
            try configuration.push()
        } catch let error {
            print("Cannot make push transition, reason: \(error.localizedDescription)")
        }
    }
}

class PushWithoutAnimationTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override fileprivate func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)

        do {
            try configuration.push(animated: false)
        } catch let error {
            print("Cannot make push transition, reason: \(error.localizedDescription)")
        }
    }
}

class InsteadRootTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override fileprivate func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)

        guard let topViewController = AppRouter.topViewController else {
            return
        }

        guard let navigationController = topViewController.navigationController ??
                                            topViewController as? UINavigationController else {
            return
        }

        do {
            try configuration.push(animated: true, completion: {
                navigationController.viewControllers = [navigationController.viewControllers.last!]
            })
        } catch let error {
            print("Cannot make instead root transition, reason: \(error.localizedDescription)")
        }
    }
}

class RootTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override fileprivate func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)
        let oldRoot = AppRouter.rootViewController

        do {
            try configuration.setAsRoot(animation:
                        .window(options: .transitionCrossDissolve, duration: 0.5)) { [weak self] _ in
                if let vc = oldRoot {
                    self?.clearStack(previousViewController: vc)
                }
            }
        } catch let error {
            print("Cannot make root transition, reason: \(error.localizedDescription)")
        }
    }

    /// Method needs as completion for AppRouter setAsRoot only for
    /// removing old rootViewController after rootVC changing
    private func clearStack(previousViewController: UIViewController) {
        previousViewController.dismiss(animated: false) {
            previousViewController.view.removeFromSuperview()
        }
    }
}

class PresentTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override fileprivate func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)
        do {
            try configuration.present()
        } catch let error {
            print("Cannot make present transition, reason: \(error.localizedDescription)")
        }
    }
}

class PresentWithoutAnimationTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)
        do {
            try configuration.present(animated: false)
        } catch let error {
            print("Cannot make present transition, reason: \(error.localizedDescription)")
        }
    }
}

class PresentPopupTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override fileprivate func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)

        do {
            try configuration.on { () -> UIViewController in
                return AppRouter.topViewController ?? AppRouter.rootViewController!
                }.present()
        } catch let error {
            print("Cannot make present popup transition, reason: \(error.localizedDescription)")
        }
    }
}

class PresentNavigationTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override fileprivate func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)
        do {
            try configuration.embedInNavigation().present()
        } catch let error {
            print("Cannot make present navigation transition, reason: \(error.localizedDescription)")
        }
    }
}

class InitialNavigationTransition<T: ViewControllerTransitionType>: ViewControllerTransitionBase<T> {

    override func transit(with configuration: AppRouter.Presenter.Configuration<T>) {
        super.transit(with: configuration)

        do {
            try configuration.embedInNavigation().setAsRoot()
        } catch let error {
            print("Cannot make initial navigation transition, reason: \(error.localizedDescription)")
        }
    }
}
