//
//  AlertActionWrapper.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 12/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

class AlertActionWrapper {
    let action: AlertActionType

    init<T: AlertActionType>(_ action: T) {
        self.action = action
    }
}
