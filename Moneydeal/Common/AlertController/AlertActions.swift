//
//  AlertActions.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 12/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

class AlertAction: AlertActionType {

    var title: String {
        return ""
    }

    var style: UIAlertAction.Style {
        return .default
    }

    let block: () -> Void

    init(completion: @escaping (() -> Void) = {}) {
        self.block = completion
    }
}

class OKAlertAction: AlertAction {

    override var title: String {
        return "TTL_OK".localized
    }
}

class CancelAlertAction: AlertAction {

    override var title: String {
        return "TTL_POST_CANCEL".localized
    }
}
