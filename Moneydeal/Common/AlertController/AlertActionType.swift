//
//  AlertActionType.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 12/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift

protocol AlertActionType {
    var title: String { get }
    var style: UIAlertAction.Style { get }
    var block: () -> Void { get }
}

extension AlertActionType {

    var title: String {
        return ""
    }

    var style: UIAlertAction.Style {
        return .default
    }

    var block: () -> Void {
        return {}
    }
}

extension UIAlertController {

    convenience init(title: String? = nil, message: String? = nil, style: UIAlertController.Style = .alert) {
        self.init(title: title, message: message, preferredStyle: style)
    }
}

extension UIAlertController {

    func withActions<T: AlertAction>(_ actions: [T],
                                     completion: @escaping (T) -> Void = { _ in }) -> UIAlertController {
        actions.map({ actionType -> UIAlertAction in
            return UIAlertAction(title: actionType.title, style: actionType.style, handler: { _ in
                completion(actionType)
            })
        }).forEach({ self.addAction($0) })

        return self
    }

    func withActions<T: AlertAction>(_ actions: [T]) -> UIAlertController {
        return withActions(actions, completion: { action in
            action.block()
        })
    }
}

extension UIAlertAction {

    class func cancel() -> UIAlertAction {
        return UIAlertAction(title: "TTL_POST_CANCEL".localized, style: .cancel)
    }
}

extension Reactive where Base: UIAlertController {

    func withActions<T: AlertActionType>(_ actions: [T]) -> Observable<T> {
        return Maybe.create { [weak base] event -> Disposable in
            guard let base = base else {
                event(.completed)
                return Disposables.create()
            }

            actions.map({ actionType -> UIAlertAction in
                return UIAlertAction(title: actionType.title, style: actionType.style, handler: { _ in
                    event(.success(actionType))
                })
            }).forEach({ base.addAction($0) })

            return Disposables.create()
            }
            .asObservable()
            .share()
    }

    func withActionWrappers<T: AlertActionWrapper>(_ wrappers: [T]) -> Observable<T> {
        return Maybe.create { [weak base] event -> Disposable in
            guard let base = base else {
                event(.completed)
                return Disposables.create()
            }

            wrappers.map({ wrapper -> UIAlertAction in
                let actionType = wrapper.action
                return UIAlertAction(title: actionType.title, style: actionType.style, handler: { _ in
                    event(.success(wrapper))
                })
            }).forEach({ base.addAction($0) })

            return Disposables.create()
            }
            .asObservable()
            .share()
    }
}

extension ObservableType where E: AlertActionWrapper {

    func acceptOnly<T>(_ type: T.Type) -> Observable<T> {
        return flatMap({ element -> Observable<T> in
            guard let element = element as? T else { return .empty() }
            return .just(element)
        })
    }

}
