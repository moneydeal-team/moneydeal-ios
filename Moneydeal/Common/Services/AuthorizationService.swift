//
//  AuthorizationService.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 17/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Domain
import KeychainSwift

class AuthorizationService {
    static let shared = AuthorizationService()

    private let keychain = KeychainSwift()

    var fcmToken: String? {
        get { return keychain[KeychainKeys.fcmToken]  }
        set { keychain[KeychainKeys.fcmToken] = newValue }
    }

    var userInfo: Domain.AuthorizationInfo? {
        get { return keychain[KeychainKeys.token]  }
        set { keychain[KeychainKeys.token] = newValue }
    }

    func resetCredentials() {
        userInfo = nil
        erase()
    }

    private func erase() {
    }
}
