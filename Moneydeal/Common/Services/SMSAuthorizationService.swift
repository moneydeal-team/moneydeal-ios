//
//  SMSAuthorizationService.swift
//  Moneydeal
//
//  Created by Aresenij Zorin on 18/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import FirebaseAuth

struct SMSAuthorizationInfo {
    let phone: String
    let token: String
}

enum SMSAuthorizationError: Swift.Error {
    case requestLimit
    case lossToken
    case reject
}

class SMSAuthorizationService {
    static func send(for phone: String) -> Single<SMSAuthorizationInfo> {
        return Single.create { [phone] single in
            PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
                  if let error = error {
                    print("[SMSAuthorizationService] Error: ", error.localizedDescription)

                    return single(.error(SMSAuthorizationError.reject))
                  }

                guard let token = verificationID else {
                    return single(.error(SMSAuthorizationError.lossToken))
                }

                return single(.success(SMSAuthorizationInfo(phone: phone,
                                                            token: token)))
            }

            return Disposables.create {
            }
        }
    }
}
