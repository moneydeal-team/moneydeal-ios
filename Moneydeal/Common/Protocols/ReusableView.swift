//
//  ReusableView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 29/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

/// Переиспользуемое View, например в UITableView или UICollectionView.
protocol ReusableView: class, ClassName {}

extension ReusableView {

    /// Идентификатор для реиспользования. Совпадает с названием класса.
    public static var reuseIdentifier: String {
        return className()
    }
}

extension UIView: ReusableView {}
