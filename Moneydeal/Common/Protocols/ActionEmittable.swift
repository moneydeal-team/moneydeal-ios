//
//  ActionEmittable.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift

protocol ActionEmittable {
    associatedtype ActionType

    var actionType: ActionType { get }
    var selectionSubject: PublishSubject<ActionType> { get set }
}

extension ActionEmittable where Self: CellViewModel {
    func emitAction() {
        selectionSubject.onNext(actionType)
    }
}
