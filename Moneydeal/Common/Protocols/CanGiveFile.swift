//
//  CanGiveFile.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import RxSwift
import Domain

protocol CanGiveFile {
    var fileSubject: PublishSubject<OutgoingFile> { get }
}
