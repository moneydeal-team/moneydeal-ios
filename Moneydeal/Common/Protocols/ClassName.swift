//
//  ClassName.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 03/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

protocol ClassName {
    static func className() -> String
}

extension ClassName {

    static func className() -> String {
        return String(describing: self)
    }
}
