//
//  ReusableTableView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 29/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {

    func reusableCell(withCell cell: UIView.Type, for indexPath: IndexPath) -> UITableViewCell? {
        let reuseIdentifire = cell.className()

        self.register(cell, forCellReuseIdentifier: reuseIdentifire)
        return dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath)
    }
}
