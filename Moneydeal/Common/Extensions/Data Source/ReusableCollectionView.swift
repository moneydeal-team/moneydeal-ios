//
//  ReusableCollectionView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 04/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {

    func reusableCell(withCell cell: UIView.Type, for indexPath: IndexPath) -> UICollectionViewCell? {
        let reuseIdentifire = cell.className()

        self.register(cell, forCellWithReuseIdentifier: reuseIdentifire)
        return dequeueReusableCell(withReuseIdentifier: reuseIdentifire, for: indexPath)
    }
}
