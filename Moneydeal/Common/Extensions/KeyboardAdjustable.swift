//
//  KeyboardAdjustable.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 12/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit
import FlexLayout
import PinLayout
import RxKeyboard

protocol KeyboardAdjustable {
    func adjustBottomWithKeyboard(for scrollView: UIScrollView)
}

extension KeyboardAdjustable where Self: ViewController {

    func adjustBottomWithKeyboard(for scrollView: UIScrollView) {
        RxKeyboard.instance.visibleHeight
        .drive(onNext: { [weak self, scrollView] keyboardVisibleHeight in
            guard let self = self else { return }

            UIView.animate(withDuration: 0) {
                scrollView.contentInset.bottom = (keyboardVisibleHeight - self.view.safeAreaInsets.bottom)

                if scrollView.contentInset.bottom < 0 {
                    scrollView.contentInset.bottom = 0
                }

                scrollView.scrollIndicatorInsets.bottom = scrollView.contentInset.bottom

                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }
        })
        .disposed(by: bag)

        RxKeyboard.instance.willShowVisibleHeight
        .drive(onNext: { [weak self, scrollView] keyboardVisibleHeight in
            guard let self = self else { return }

            guard scrollView.contentSize.height > scrollView.bounds.height else { return }

            var value = (keyboardVisibleHeight - self.view.safeAreaInsets.bottom)

            if value < 0 {
                value = 0
            }

            scrollView.contentOffset.y += value
        })
        .disposed(by: bag)
    }
}
