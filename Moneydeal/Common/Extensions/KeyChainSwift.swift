//
//  KeyChainSwift.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 17/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import KeychainSwift

extension KeychainSwift {

    // Workaround for error: Top-level Int encoded as number JSON fragment
    subscript(key: String) -> Int? {
        get {
            let decoder = JSONDecoder()
            guard let data = getData(key) else { return nil }

            do {
                let array = try decoder.decode([Int].self, from: data)
                return array.first
            } catch let error {
                log(operation: "get", error: error, for: key)
            }

            return nil
        }
        set {
            guard let newValue = newValue else {
                delete(key)
                return
            }

            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode([newValue])

                set(data, forKey: key)
            } catch let error {
                log(operation: "set", error: error, for: key)
            }
        }
    }

    subscript<T: Codable>(key: String) -> T? {
        get {
            let decoder = JSONDecoder()
            guard let data = getData(key) else { return nil }

            do {
                return try decoder.decode(T.self, from: data)
            } catch let error {
                log(operation: "get", error: error, for: key)
            }

            return nil
        }
        set {
            guard let newValue = newValue else {
                delete(key)
                return
            }

            do {
                let encoder = JSONEncoder()
                let data = try encoder.encode(newValue)

                set(data, forKey: key)
            } catch let error {
                log(operation: "set", error: error, for: key)
            }
        }
    }

    subscript(key: String) -> Bool {
        get { return getBool(key) ?? false }
        set { set(newValue, forKey: key) }
    }

    subscript(key: String) -> String {
        get { return get(key) ?? "" }
        set { set(newValue, forKey: key) }
    }

    private func log(operation: String, error: Error, for key: String) {
        print("Cannot \(operation) for key: \"\(key)\" from keychain, reason: \(error.localizedDescription)")
    }
}
