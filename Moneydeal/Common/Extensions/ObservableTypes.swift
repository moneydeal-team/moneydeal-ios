//
//  ObservableTypes.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 12/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: - ThrottleValue

public class ThrottleValues {
    public static let buttonThrottleDelay: Double = 0.3
    public static let retryDelay: Double = 1.0
}

public extension ObservableType {

    func throttleTapInMainScheduler() -> Observable<Self.E> {
        return self.throttle(ThrottleValues.buttonThrottleDelay, scheduler: MainScheduler.instance)
    }

    func throttleTapInBackgroundScheduler() -> Observable<Self.E> {
        return self.throttle(ThrottleValues.buttonThrottleDelay,
                             scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
    }
}

public extension PrimitiveSequence {

    func asDriverOnErrorJustComplete() -> Driver<E> {
        return asDriver { _ in
            return Driver.empty()
        }
    }
}

public extension ObservableType {

    func catchErrorJustComplete() -> Observable<E> {
        return catchError { _ in
            return Observable.empty()
        }
    }

    func asDriverOnErrorJustComplete() -> Driver<E> {
        return asDriver { _ in
            return Driver.empty()
        }
    }

    func mapToVoid() -> Observable<Void> {
        return map { _ in }
    }
}

public extension ObservableType where E == Bool {

    func not() -> Observable<E> {
        return self.map({ !$0 })
    }
}
