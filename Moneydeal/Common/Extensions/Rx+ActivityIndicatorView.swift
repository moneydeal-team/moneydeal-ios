//
//  Rx+ActivityIndicatorView.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 24/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxCocoa
import RxSwift

extension Reactive where Base: ActivityIndicatorViewType {

    internal var isShowing: Binder<Bool> {
        return Binder(base) { object, active in
            if active {
                object.show()
            } else {
                object.hide()
            }
        }
    }
}
