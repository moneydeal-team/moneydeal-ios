//
//  String+Empty.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 09/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

extension String {
    /**
     true if self contains characters.
     */
    var isNotEmpty: Bool {
        return !isEmpty
    }
}
