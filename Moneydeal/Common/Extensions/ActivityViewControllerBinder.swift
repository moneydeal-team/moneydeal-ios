//
//  ActivityViewControllerBinder.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 24/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift

extension ViewModelHolderBinder where T: ActivityIndicatorContainer & ViewController, T.VM: ActivityTrackable {

    @discardableResult
    func activityTrackable() -> ViewModelHolderBinder {
        guard let holder = holder else { return self }

        holder.viewModel?.activityTracker.asObservable()
            .skipWhile({ $0 == false })
            .asDriverOnErrorJustComplete()
            .drive(holder.indicatorView.rx.isShowing).disposed(by: holder.bag)

        return self
    }

}
