//
//  UIImagePickerController.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift
import MobileCoreServices
import Domain

extension UIImagePickerController {

    class func pickerController() -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.mediaTypes = [String(kUTTypeImage), String(kUTTypeMovie)]

        return picker
    }
}

extension UIImagePickerController {

    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.navigationBar.topItem?.rightBarButtonItem?.tintColor = UIColor.black
        self.navigationBar.topItem?.rightBarButtonItem?.isEnabled = true
    }
}

extension UIImagePickerController: CanGiveFile {

    var fileSubject: PublishSubject<OutgoingFile> {
        let proxy = ImageControllerDelegateProxy.proxy(for: self)
        return proxy.fileSubject
    }
}
