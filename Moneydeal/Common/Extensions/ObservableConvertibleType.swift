//
//  ObservableConvertibleType.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 05/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxSwift

extension ObservableConvertibleType {

    func asTransitionable() -> TransitionableSequence<E> {
        return TransitionableSequence(source: self.asObservable())
    }
}

protocol TransitionableSequenceType {
    // swiftlint:disable:next type_name
    associatedtype E

    var transitionableSequence: TransitionableSequence<E> { get }
}

extension TransitionableSequenceType {
    // swiftlint:disable:next line_length
    func transit<T: Transition>(to: T) -> Disposable where T.Destination: Transitionable & ViewControllerTransitionType {
        return transitionableSequence.source.subscribe({ _ in
            to.go()
        })
    }
    // swiftlint:disable:next line_length
    func transit<T: Transition>(to: T) -> Disposable where T.Destination: TransitionableInput & ViewControllerTransitionType,
        T.Destination.InputParameters == E {
            return transitionableSequence.source.subscribe(onNext: to.go)
    }
}

struct TransitionableSequence<Element>: TransitionableSequenceType {
    // swiftlint:disable:next type_name
    typealias E = Element

    fileprivate let source: Observable<Element>
    public var transitionableSequence: TransitionableSequence<E> {
        return self
    }

    public init(source: Observable<Element>) {
        self.source = source
    }
}
