//
//  PR+Photos.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import RxCocoa
import RxSwift
import Photos
import Domain

extension Reactive where Base: PHPhotoLibrary {

    func saveImage(_ image: UIImage) -> Single<(PHAsset, URL)> {
        return Single<(PHAsset, URL)>
            .create { event -> Disposable in
                guard PHPhotoLibrary.authorizationStatus() == .authorized else {
                    event(.error(Domain.Error.appError(message: "TTL_PERMISSION_DENIED_PHOTO_MESSAGE".localized)))
                    return Disposables.create()
                }

                var disposable: Disposable?

                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAsset(from: image)
                }, completionHandler: { (_, error) in
                    let fetchOptions = PHFetchOptions()
                    fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]

                    let result = PHAsset.fetchAssets(with: .image, options: fetchOptions)

                    guard let asset = result.firstObject else {
                        event(.error(Domain.Error.appError(message: "TTL_PHOTO_LIBRARY_NOT_FOUND".localized)))
                        return
                    }

                    disposable = asset.rx.url.map({ (asset, $0) }).subscribe(event)
                })

                return Disposables.create {
                    disposable?.dispose()
                }
            }
            .observeOn(MainScheduler.instance)
    }
}

extension Reactive where Base: PHAsset {

    var url: Single<URL> {
        return Single.create(subscribe: { [weak base] event -> Disposable in
            guard PHPhotoLibrary.authorizationStatus() == .authorized else {
                event(.error(Domain.Error.appError(message: "TTL_PERMISSION_DENIED_PHOTO_MESSAGE".localized)))
                return Disposables.create()
            }

            guard let base = base else {
                event(.error(Domain.Error.appError(message: "TTL_PHOTO_LIBRARY_LINK_NOT_FOUND".localized)))
                return Disposables.create()
            }

            if base.mediaType == .image {
                let options = PHContentEditingInputRequestOptions()
                options.canHandleAdjustmentData = { (adjustmeta: PHAdjustmentData) -> Bool in
                    return true
                }
                base.requestContentEditingInput(with: options, completionHandler: { (contentEditingInput, _) in
                    guard let URL = contentEditingInput?.fullSizeImageURL else {
                        event(.error(Domain.Error.appError(message: "TTL_PHOTO_LIBRARY_LINK_NOT_FOUND".localized)))
                        return
                    }
                    event(.success(URL))

                })
            } else if base.mediaType == .video {
                let options = PHVideoRequestOptions()
                options.version = .original

                PHImageManager.default().requestAVAsset(forVideo: base, options: options, resultHandler: { (asset, _, _) in
                    guard let assetURL = asset as? AVURLAsset else {
                        event(.error(Domain.Error.appError(message: "TTL_PHOTO_LIBRARY_LINK_NOT_FOUND".localized)))
                        return
                    }
                    event(.success(assetURL.url))
                })
            }

            return Disposables.create()
        })
    }
}
