//
//  UIViewController+Keyboard.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 12/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        DispatchQueue.main.async { [weak view] in
            view?.endEditing(true)
        }
    }
}
