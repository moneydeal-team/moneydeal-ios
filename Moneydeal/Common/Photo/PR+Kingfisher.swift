//
//  PR+Kingfisher.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Kingfisher
import RxSwift
import RxKingfisher
import Network

extension Kingfisher where Base: UIImageView {

    @discardableResult
    func setImage(with resource: Resource?,
                  placeholder: Placeholder? = nil,
                  options: KingfisherOptionsInfo? = nil,
                  progressBlock: DownloadProgressBlock? = nil,
                  completionHandler: CompletionHandler? = nil,
                  auth: Bool) -> RetrieveImageTask {
        var options = options

        if options == nil {
            options = []
        }

        options?.withAuth()

        return setImage(with: resource, placeholder: placeholder, options: options, progressBlock: progressBlock, completionHandler: completionHandler)
    }
}

extension Reactive where Base == Kingfisher<UIImageView> {

    func pr_setImage(with resource: Resource?, placeholder: Placeholder? = nil, options: KingfisherOptionsInfo? = nil) -> RxSwift.PrimitiveSequence<RxSwift.SingleTrait, Image> {
        var options = options

        if options == nil {
            options = []
        }

        options?.withAuth()

        return setImage(with: resource, placeholder: placeholder, options: options)
            .catchError(MoyaErrorMapper.handleError)
    }
}

extension Array where Element == KingfisherOptionsInfoItem {

    mutating func withAuth() {
        let option = KingfisherOptionsInfoItem.requestModifier(AnyModifier(modify: { request   in
            guard let token = AuthorizationService.shared.userInfo else { return request }

            var request = request
            request.setValue(token.accessToken, forHTTPHeaderField: "X-Auth")
            request.setValue(Bundle.localization.serverLocal, forHTTPHeaderField: "x-lang")

            return request
        }))
        append(option)
    }
}
