//
//  PR+AVAssetImageGenerator.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import AVFoundation
import UIKit

extension AVAssetImageGenerator {

    class func thumbnail(for url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform = true

        var time = asset.duration
        time.value = min(time.value, 2)

        guard let imageRef = try? assetImageGenerator.copyCGImage(at: time, actualTime: nil) else { return nil }
        return UIImage(cgImage: imageRef)
    }
}
