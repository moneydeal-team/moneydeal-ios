//
//  ActivityIndicatorContainer.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 24/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import RxSwift

protocol IndicatorContainer {
    func configureIndicator()
}

protocol ActivityIndicatorContainer: IndicatorContainer {
    associatedtype IndicatorView: ActivityIndicatorViewType

    var indicatorView: IndicatorView! { get }

    func configureAppearance(for indicator: IndicatorView)
}

extension ActivityIndicatorContainer {

    func configureAppearance(for indicator: IndicatorView) {
        // Do nothing
    }
}

extension IndicatorContainer where Self: ActivityIndicatorContainer & ViewController {

    func configureIndicator() {
        view.addSubview(indicatorView)

        view.rx.methodInvoked(#selector(UIView.layoutSubviews))
            .subscribe(onNext: { [weak self] _ in
                self?.indicatorView?.frame = self?.view.frame ?? .zero
            }).disposed(by: bag)

        configureAppearance(for: indicatorView)
    }
}

extension IndicatorContainer where Self: ActivityIndicatorContainer & TableViewCell {

    func configureIndicator() {
        contentView.addSubview(indicatorView)
        contentView.bringSubviewToFront(indicatorView)
        indicatorView.isHidden = false

        contentView.rx.methodInvoked(#selector(UIView.layoutSubviews))
            .subscribe(onNext: { [weak self] _ in
                self?.indicatorView?.frame = self?.contentView.frame ?? .zero
            }).disposed(by: bag)

        configureAppearance(for: indicatorView)
    }
}

extension IndicatorContainer where Self: ActivityIndicatorContainer & CollectionViewCell {

    func configureIndicator() {
        contentView.addSubview(indicatorView)

        contentView.rx.methodInvoked(#selector(UIView.layoutSubviews))
            .subscribe(onNext: { [weak self] _ in
                self?.indicatorView?.frame = self?.contentView.frame ?? .zero
            }).disposed(by: bag)

        configureAppearance(for: indicatorView)
    }
}
