//
//  ErrorTracker.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 12/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxSwift
import RxCocoa

final class ErrorTracker: SharedSequenceConvertibleType {

    typealias SharingStrategy = DriverSharingStrategy
    private let _subject = PublishSubject<Error>()

    func trackError<O: ObservableConvertibleType>(from source: O) -> Observable<O.E> {
        return source.asObservable().do(onError: onError)
    }

    func asSharedSequence() -> SharedSequence<SharingStrategy, Error> {
        return _subject.asObservable().asDriverOnErrorJustComplete()
    }

    func asObservable() -> Observable<Error> {
        return _subject.asObservable()
    }

    func onError(_ error: Swift.Error) {
        _subject.onNext(error)
    }

    deinit {
        _subject.onCompleted()
    }
}

extension ObservableConvertibleType {

    func trackError(_ errorTracker: ErrorTracker) -> Observable<E> {
        return errorTracker.trackError(from: self)
    }
}

extension SharedSequenceConvertibleType {

    func trackError(_ errorTracker: ErrorTracker) -> Driver<E> {
        return errorTracker.trackError(from: self).asDriverOnErrorJustComplete()
    }
}

extension PrimitiveSequence {

    func trackError(_ errorTracker: ErrorTracker) -> Single<ElementType> {
        return errorTracker.trackError(from: self).asSingle()
    }
}
