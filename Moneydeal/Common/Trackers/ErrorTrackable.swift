//
//  ErrorTrackable.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 12/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import Domain

protocol ErrorTrackable {
    var errorTracker: ErrorTracker { get }
}

protocol ErrorPresentable {

    var shouldShowError: Bool { get }

    func subscribeToErrors()

    func showError(error: Swift.Error)
}

extension ErrorPresentable where Self: ViewModelHolder & ViewController, Self.VM: ErrorTrackable {

    var shouldShowError: Bool {
        return true
    }

    func subscribeToErrors() {
        viewModel?.errorTracker
            .asDriver()
            .drive(onNext: { [weak self] error in
                guard let vc = self else { return }
                vc.showError(error: error)
            }).disposed(by: bag)
    }

    func showError(error: Swift.Error) {
        guard shouldShowError else { return }

        let controller = AlertController(title: "TTL_ERROR_TITLE".localized,
                                         style: .alert)
            .withActions([OKAlertAction()])
        controller.message = error.localizedDescription

//        if let error = error as? ServerStatusResponse {
//            controller.message = error.description
//        }
        if let error = error as? Domain.Error {
            controller.message = error.localizedDescription
        }

        if presentedViewController == nil {
            present(controller, animated: true, completion: nil)
        }
    }
}
