//
//  ActivityIndicatorViewType.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 24/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit

protocol ActivityIndicatorViewType where Self: UIView {
    func show()
    func hide()
}
