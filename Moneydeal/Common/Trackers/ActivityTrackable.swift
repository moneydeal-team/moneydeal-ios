//
//  ActivityTrackable.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 24/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

protocol ActivityTrackable {
    associatedtype T: ActivityTracker

    var activityTracker: T { get }
}
