//
//  Date.swift
//  Utils
//
//  Created by Konstantin Kulakov on 06/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation

public extension Date {

    func asString(format: String = "", timeZone: TimeZone = .current, locale: Locale = .current) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = timeZone
        formatter.locale = locale

        /// Workaround for 12-hour format
        if !locale.identifier.contains("POSIX") {
            formatter.locale = Locale(identifier: "\(locale.identifier)_POSIX")
        }

        return formatter.string(from: self)
    }

    func today() -> Bool {
        return Calendar.current.isDateInToday(self)
    }

    func yesterday() -> Bool {
        return Calendar.current.isDateInYesterday(self)
    }
}
