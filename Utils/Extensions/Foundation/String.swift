//
//  String.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

public extension String {

    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle, value: "", comment: "")
    }
}

public extension String {
    func asDate(format: String = Constants.defaultDateFormat,
                timeZone: TimeZone = .current,
                locale: Locale = .current) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = timeZone
        formatter.locale = locale

        /// Workaround for 12-hour format
        if !locale.identifier.contains("POSIX") {
            formatter.locale = Locale(identifier: "\(locale.identifier)_POSIX")
        }

        return formatter.date(from: self)
    }
}
