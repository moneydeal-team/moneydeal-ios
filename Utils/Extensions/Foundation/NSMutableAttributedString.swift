//
//  NSMutableAttributedString.swift
//  Utils
//
//  Created by Никита Чужиков on 14/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import UIKit
import Foundation

extension NSMutableAttributedString {

    /// Устанавливает ссылку для первого вхождения подстроки в тексте.
    ///
    /// - Parameters:
    ///   - for: Искомая строка в тексте.
    ///   - with: Ссылка на ресурс.
    public func setLink(for textToFind: String, with linkURL: String) {
        let foundRange = self.mutableString.range(of: textToFind)

        guard foundRange.location != NSNotFound else { return }

        self.addAttribute(.link, value: linkURL, range: foundRange)
    }

}
