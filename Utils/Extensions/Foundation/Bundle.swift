//
//  Bundle.swift
//  Moneydeal
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

public enum Language: String {
    case ru = "ru_RU"
    case en = "en_US"

    public init(from rawValue: String?) {
        let strLocale: String? = rawValue ?? Locale.current.languageCode

        switch strLocale {
        case "ru_RU":
            self = .ru
        case "ru":
            self = .ru

        case "en_US":
            self = .en
        case "en":
            self = .en

        default:
            self = .ru
        }
    }
}

public extension Language {
    var path: String {
        switch self {
        case .ru:
            return "ru"
        case .en:
            return "en"
        }
    }

    var locale: Locale {
        return Locale.init(identifier: self.rawValue)
    }

    var serverLocal: String {
        switch self {
        case .ru:
            return "ru"
        case .en:
            return "en"
        }
    }
}

public extension Bundle {
    private static var bundle: Bundle!

    static var localizedBundle: Bundle {
        if bundle == nil {
            let path = Bundle.main.path(forResource: localization.path, ofType: "lproj")

            bundle = Bundle(path: path!)
        }

        return bundle
    }

    static var localization: Language {
        get {
            let appLang = UserDefaults.standard.string(forKey: "app_lang")

            return Language(from: appLang)
        }

        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: "app_lang")
            UserDefaults.standard.synchronize()
            let path = Bundle.main.path(forResource: newValue.path, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
    }
}
