//
//  Constants.swift
//  Utils
//
//  Created by Konstantin Kulakov on 06/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation

public enum Constants {
    public static let defaultDateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    public static let humanLikeDateFormat = "dd.MM.yyyy"

    public enum Camera {
        public static let defaultNameWithExt = "photo.jpg"
        public static let defaultNameWithExtPNG = "photo.png"
        public static let defaultName = "photo"
    }
}
