//
//  Reactive+AVFoundation.swift
//  Utils
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import AVFoundation
import RxSwift

public extension Reactive where Base: AVCaptureDevice {

    static func status(for type: AVMediaType) -> Single<AVAuthorizationStatus> {
        let status = AVCaptureDevice.authorizationStatus(for: type)
        guard status == .notDetermined else {
            return Single.just(status)
        }

        return Single.create(subscribe: { single -> Disposable in
            AVCaptureDevice.requestAccess(for: type, completionHandler: { status in
                single(.success(status ? AVAuthorizationStatus.authorized : .denied))
            })

            return Disposables.create()
        }).observeOn(MainScheduler.instance)
    }

}
