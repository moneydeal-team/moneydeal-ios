//
//  Rx+TextField.swift
//  Utils
//
//  Created by Konstantin Kulakov on 18/11/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit

public extension Reactive where Base: UITextField {

    var textChange: Observable<String?> {
        return Observable.merge(self.base.rx.observe(String.self, "text"),
                                self.base.rx.controlEvent(.editingChanged).withLatestFrom(self.base.rx.text))
    }

}
