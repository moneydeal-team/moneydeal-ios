//
//  Reactive+PHPhotoLibrary.swift
//  Utils
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import RxSwift
import Photos

public extension Reactive where Base: PHPhotoLibrary {

    public var status: Single<PHAuthorizationStatus> {
        let status = PHPhotoLibrary.authorizationStatus()
        guard status == .notDetermined else {
            return Single.just(status)
        }

        return Single.create(subscribe: { single -> Disposable in
            PHPhotoLibrary.requestAuthorization({ status in
                single(.success(status))
            })

            return Disposables.create()
        }).observeOn(MainScheduler.instance)
    }

}
