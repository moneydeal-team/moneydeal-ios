//
//  PR+ObservableType.swift
//  RealmStorage
//
//  Created by Ivan Tsaryov on 06/12/2018.
//  Copyright © 2018 65apps. All rights reserved.
//
// swiftlint:disable all
import RxSwift

public extension ObservableType where E: PersistableObject {

    func translate<P: Persistable>(_ type: P.Type) -> Observable<P> {
        return flatMap({ element -> Observable<P> in
            guard let element = element.translate(type) else {
                return Observable.empty()
            }
            return Observable.just(element)
        })
    }
}

public extension ObservableType {

    func translate<P: Persistable>(_ type: P.Type) -> Observable<([P], DAOChangeset?)> where E == ([P.ManagedObject], DAOChangeset?) {
        return flatMap({ Observable.just(($0.asPersistableArray(type), $1)) })
    }
}
