//
//  PR+Array.swift
//  RealmStorage
//
//  Created by Ivan Tsaryov on 01/08/2018.
//  Copyright © 2018 65apps. All rights reserved.
//

import RealmSwift

extension Array where Element: Persistable {

    public func asList() -> List<Element.ManagedObject> {
        return self.reduce(List<Element.ManagedObject>(), { (list, persistable) -> List<Element.ManagedObject> in
            list.append(persistable.managedObject())
            return list
        })
    }
}

extension Array {

    public func asPersistableArray<P: Persistable>(_ type: P.Type) -> [P] where P.ManagedObject == Element {
        return self.map({ P(managedObject: $0) })
    }
}
