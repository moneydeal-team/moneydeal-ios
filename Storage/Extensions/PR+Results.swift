//
//  PR+Results.swift
//  RealmStorage
//
//  Created by Ivan Tsaryov on 01/08/2018.
//  Copyright © 2018 65apps. All rights reserved.
//
// swiftlint:disable all
import RxSwift
import RealmSwift
import RxRealm

// MARK: - DB Translation

extension Results where Element: PersistableObject {

    public func translate<P: Persistable>(_ type: P.Type) -> [P] where Element == P.ManagedObject {
        return self.toArray().asPersistableArray(type)
    }
}

// MARK: - Observable results

extension Results: ObservableConvertibleType {

    public typealias E = ([Element], DAOChangeset?)

    public func asObservable() -> Observable<E> {
        return Observable.deferred({
            Observable.changeset(from: self)
                .map({ (tuple) in
                    let collection = tuple.0.toArray()
                    let changeset = DAOChangeset(deleted: (tuple.1?.deleted) ?? [],
                                                 inserted: (tuple.1?.inserted) ?? [],
                                                 updated: (tuple.1?.updated) ?? [])
                    return (collection, changeset)
                })
        })
    }
}
