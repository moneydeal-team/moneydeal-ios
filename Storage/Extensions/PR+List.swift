//
//  PR+List.swift
//  RealmStorage
//
//  Created by Ivan Tsaryov on 01/08/2018.
//  Copyright © 2018 65apps. All rights reserved.
//

import RealmSwift

extension List {

    public func asPersistableArray<P: Persistable>() -> [P] where P.ManagedObject == Element {
        return self.map({ P(managedObject: $0) })
    }
}
