//
//  Persistable.swift
//  RealmStorage
//
//  Created by Ivan Tsaryov on 01/08/2018.
//  Copyright © 2018 65apps. All rights reserved.
//

import RealmSwift

public protocol Persistable {

    associatedtype ManagedObject: PersistableObject

    init(managedObject: ManagedObject)

    func managedObject() -> ManagedObject
}

public extension Persistable {

    func translate() -> Self.ManagedObject {
        return self.managedObject()
    }
}
