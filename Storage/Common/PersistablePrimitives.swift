//
//  PersistablePrimitives.swift
//  RealmStorage
//
//  Created by Ivan Tsaryov on 01/08/2018.
//  Copyright © 2018 65apps. All rights reserved.
//

import RealmSwift

public final class StringObject: PersistableObject {

    @objc dynamic var value: String = "" {
        didSet {
            id = value
        }
    }
}

extension String: Persistable {

    public typealias ManagedObject = StringObject

    public init(managedObject: ManagedObject) {
        self = managedObject.value
    }

    public func managedObject() -> StringObject {
        let object = StringObject()
        object.value = self

        return object
    }
}
