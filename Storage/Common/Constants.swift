//
//  Constants.swift
//  RealmStorage
//
//  Created by Ivan Tsaryov on 01/08/2018.
//  Copyright © 2018 65apps. All rights reserved.
//

import Foundation

enum Constants {

    enum Database {
        static let DatabaseFilename = "Cosmo.realm"
        static let DatabaseVersion: UInt64 = 2
    }
}
