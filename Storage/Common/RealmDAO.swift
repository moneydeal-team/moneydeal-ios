//
//  RealmDAO.swift
//  RealmStorage
//
//  Created by Ivan Tsaryov on 01/08/2018.
//  Copyright © 2018 65apps. All rights reserved.
//

// swiftlint:disable all
import RealmSwift

public struct DAOChangeset {
    /// the indexes in the collection that were deleted
    public let deleted: [Int]

    /// the indexes in the collection that were inserted
    public let inserted: [Int]

    /// the indexes in the collection that were modified
    public let updated: [Int]
}

protocol DAO {

    associatedtype T: Persistable

    var databaseFilename: String { get }
    var databaseVersion: UInt64 { get set }

    func object(entityID: String) -> T.ManagedObject?
    func collection() -> Results<T.ManagedObject>

    func persist(object: T)
    func persistAll(objects: [T])

    func erase(object: T, cascading: Bool)
    func erase(objects: [T], cascading: Bool)
    func erase(cascading: Bool)
}

public class RealmDAO<P: Persistable>: DAO {

    typealias T = P

    var databaseFilename: String = Constants.Database.DatabaseFilename
    var databaseVersion: UInt64 = Constants.Database.DatabaseVersion

    public init() {}

    func realm() -> Realm {
        var config = Realm.Configuration.defaultConfiguration

        config.schemaVersion = databaseVersion
        config.migrationBlock = { [unowned self] migration, oldSchemaVersion in
            if (oldSchemaVersion < self.databaseVersion) {
                // Nothing to do!
                // Realm will automatically detect new properties and removed properties
                // And will update the schema on disk automatically
            }
        }
        config.fileURL = URL(fileURLWithPath: absolutePath(filename: databaseFilename))

        Realm.Configuration.defaultConfiguration = config

        return try! Realm()
    }

    public func object(entityID: String) -> P.ManagedObject? {
        return realm().object(ofType: P.ManagedObject.self, forPrimaryKey: entityID)
    }

    public func collection() -> Results<P.ManagedObject> {
        return realm().objects(P.ManagedObject.self)
    }

    public func persist(object: P) {
        try? realm().write {
            realm().add(object.translate(), update: .all)
        }
    }

    public func persistAll(objects: [P]) {
        try? realm().write {
            realm().add(objects.asList(), update: .all)
        }
    }
    
    public func update(object: P) {
        try? realm().write {
            realm().add(object.translate(), update: .modified)
        }
    }
    
    public func updateAll(objects: [P]) {
        try? realm().write {
            realm().add(objects.asList(), update: .modified)
        }
    }
    
    public func erase(entityID: String, cascading: Bool) {
        guard let existing = realm().object(ofType: P.ManagedObject.self, forPrimaryKey: entityID) else {
            return
        }

        try? realm().write {
            realm().delete(existing, cascading: cascading)
        }
    }

    public func erase(object: P, cascading: Bool) {
        guard let existing = realm().object(ofType: P.ManagedObject.self, forPrimaryKey: object.translate().id) else {
            return
        }

        try? realm().write {
            realm().delete(existing, cascading: cascading)
        }
    }

    public func erase(objects: [P], cascading: Bool) {
        for object in objects {
            erase(object: object, cascading: cascading)
        }
    }

    public func erase(cascading: Bool) {
        try? realm().write {
            let objects = realm().objects(P.ManagedObject.self)
            realm().delete(objects, cascading: cascading)
        }
    }
}

extension RealmDAO {

    func absolutePath(filename: String) -> String {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)

        guard let path = documentsDirectory.first else { return filename }

        return String(path + "/" + filename)
    }

    public func object(first: Bool = true) -> P.ManagedObject? {
        return first ? collection().first : collection().last
    }

    public func object(by id: String) -> P? {
        return object(entityID: id)?.translate(P.self)
    }
}
