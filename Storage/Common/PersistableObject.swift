//
//  PersistableObject.swift
//  RealmStorage
//
//  Created by Konstantin Kulakov on 19/11/2019.
//  Copyright © 2019 Konstanin Kulakov. All rights reserved.
//
// swiftlint:disable all
import RxSwift
import RxRealm
import RealmSwift

public class PersistableObject: Object {

    @objc public dynamic var id: String = NSUUID().uuidString

    override public static func primaryKey() -> String? {
        return "id"
    }

    public func translate<P: Persistable>(_ type: P.Type) -> P? {
        guard let object = self as? P.ManagedObject else {
            return nil
        }
        return P(managedObject: object)
    }
}

extension PersistableObject: ObservableConvertibleType {

    public typealias E = PersistableObject

    public func asObservable() -> Observable<PersistableObject> {
        return Observable.deferred({
            return Observable.from(object: self)
        })
    }
}
