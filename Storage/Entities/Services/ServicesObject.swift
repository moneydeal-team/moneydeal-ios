//
//  ServicesObject.swift
//  Storage
//
//  Created by Konstantin Kulakov on 20/10/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

// swiftlint:disable all
import RealmSwift

public final class UserInfoObject: PersistableObject {
   // public let id: Int
    
    @objc public dynamic var name: String  = ""
    @objc public dynamic var lastName: String  = ""

    @objc public dynamic var avatar: String  = ""
}
