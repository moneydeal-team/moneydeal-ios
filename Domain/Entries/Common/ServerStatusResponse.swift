//
//  ServerStatusResponse.swift
//  Domain
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation
import Utils

public final class ServerStatusResponse: Swift.Error {
    public private(set) var message: String = ""
    public private(set) var serverMessage: String?

    public var status: ServerStatus {
        return ServerStatus(serverResponse: message)
    }
}

extension ServerStatusResponse: Codable {
    enum CodingKeys: String, CodingKey {
        case message = "status"
        case serverMessage = "error"
    }
}

extension ServerStatusResponse {
    private var codeMap: [ServerStatus: String] {
        return [
            .success: "TTL_ERROR_OK".localized,
            .error: "TTL_ERROR_COMMON".localized
        ]
    }

    public var description: String {
        return codeMap[status] ?? "TTL_ERROR_UNDEFINED".localized
    }
}

public enum ServerStatus: String {
    case success = "OK"
    case error = "error"
    case undefined

    init(serverResponse: String) {
        switch serverResponse {
        case "OK":      self = .success
        case "error":   self = .error
        default:        self = .undefined
        }
    }
}
