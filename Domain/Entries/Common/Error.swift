//
//  Error.swift
//  Domain
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

public enum Error: Swift.Error {
    case authError(message: String)
    case systemError(message: String)
    case serverError(message: String)
    case networkError(message: String)
    case invalidResponse(message: String)
    case appError(message: String)
}

extension Domain.Error {

    public var localizedDescription: String {
        switch self {
        case .authError(let message): return message
        case .systemError(let message): return message
        case .serverError(let message): return message
        case .networkError(let message): return message
        case .appError(let message): return message
        case .invalidResponse(let message): return message
        }
    }
}
