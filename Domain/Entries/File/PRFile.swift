//
//  PRFile.swift
//  Domain
//
//  Created by Konstantin Kulakov on 14/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import UIKit
import Utils

public struct PRFile {

    public let localURL: URL

    public init(localURL: URL) {
        self.localURL = localURL
    }
}

public enum FileType {
    case image
    case video
    case file
}

public struct OutgoingFile {
    public let data: Data
    public let name: String
    public let nameWithExt: String
    public let type: FileType
    public let thumbnail: UIImage?

    public init?(mediaURL: URL, type: FileType, thumbnail: UIImage? = nil) {
        guard let data = try? Data(contentsOf: mediaURL) else {
            return nil
        }

        self.data = data
        self.type = type
        self.thumbnail = thumbnail

        nameWithExt = mediaURL.lastPathComponent
        name = nameWithExt.components(separatedBy: ".").dropLast().reduce(into: "", { (result, value) in
            result += value
        })
    }

    public init?(image: UIImage, type: FileType, thumbnail: UIImage? = nil, isPNG: Bool = false) {
        guard let data: Data = isPNG ?
            image.pngData() :
            image.jpegData(compressionQuality: 1) else { return nil }

        self.data = data
        self.type = type
        self.thumbnail = thumbnail

        nameWithExt = isPNG ? Constants.Camera.defaultNameWithExtPNG : Constants.Camera.defaultNameWithExt
        name = Constants.Camera.defaultName
    }
}

public extension OutgoingFile {

    var `extension`: String {
        return nameWithExt.components(separatedBy: ".").last ?? ""
    }
}
