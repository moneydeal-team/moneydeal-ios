//
//  OutgoingFile+MimeType.swift
//  Domain
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import UIKit
import MobileCoreServices

public extension OutgoingFile {

    var mimeType: String {
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,
                                                              self.extension as NSString,
                                                              nil)?.takeRetainedValue(),
        let mimetype = UTTypeCopyPreferredTagWithClass(uti,
                                                       kUTTagClassMIMEType)?.takeRetainedValue()
            else { return "application/octet-stream" }

        return mimetype as String
    }
}
