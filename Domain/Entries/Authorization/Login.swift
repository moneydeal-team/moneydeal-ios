//
//  Login.swift
//  Domain
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

public final class Login {
    public var token: String?
}

extension Login: Codable {
    enum CodingKeys: String, CodingKey {
        case token = "token"
    }
}
