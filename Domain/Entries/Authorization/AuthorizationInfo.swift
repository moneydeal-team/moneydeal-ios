//
//  AuthorizationInfo.swift
//  Domain
//
//  Created by Konstantin Kulakov on 17/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

public struct AuthorizationInfo: Codable {
    public var accessToken: String = ""

    public init(with token: String) {
        self.accessToken = token
    }
}
