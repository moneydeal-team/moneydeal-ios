//
//  UserInfo.swift
//  Domain
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation

public struct UserInfo: Codable {
    public var name: String?
    public var lastName: String?
    public var avatar: String?

    public init(name: String, lastName: String, phone: String, avatar: String, email: String) {
        self.name = name
        self.lastName = lastName
        self.avatar = avatar
    }

    enum CodingKeys: String, CodingKey {
        case name = "first_name"
        case lastName = "last_name"
        case avatar = "photo_path"
    }
}
