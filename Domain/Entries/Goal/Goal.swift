//
//  Goal.swift
//  Domain
//
//  Created by Никита Чужиков on 09/12/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

public typealias GoalList = [Goal]

public class Goal: Codable {
    public let id: String
    public let name: String
    public let endDate: String
    public let balance: UInt64
    public let amount: UInt64
    public let imageURL: String

    public var progressPercent: Float {
        return Float(balance)/Float(amount)
    }

    public init(id: String, name: String, endDate: String, balance: UInt64, amount: UInt64, imageURL: String) {
        self.id = id
        self.name = name
        self.endDate = endDate
        self.balance = balance
        self.amount = amount
        self.imageURL = imageURL
    }

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "title"
        case endDate = "finish_time"
        case balance = "balance"
        case amount = "amount"
        case imageURL = "photo"
    }
}
