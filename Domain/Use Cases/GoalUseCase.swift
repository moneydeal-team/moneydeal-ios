//
//  GoalUseCase.swift
//  Domain
//
//  Created by Konstantin Kulakov on 06/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift

public protocol GoalUseCase {
    func goals(from date: Date, count: Int) -> Single<[Goal]>
    func create(goal: Goal) -> Single<Void>
    func upload(file: OutgoingFile) -> Single<ImageInfo>
}
