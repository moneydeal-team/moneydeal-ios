//
//  AuthorizationUseCase.swift
//  Domain
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import RxSwift

public protocol AuthorizationUseCase {
    func login(phone: String, token: String, googleToken: String, code: String) -> Single<Login>
}
