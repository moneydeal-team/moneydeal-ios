//
//  SettingsUseCase.swift
//  Domain
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import Foundation
import RxSwift

public protocol SettingsUseCase {
    func obtainInfo() -> Single<UserInfo>
    func uploadAvatar(file: OutgoingFile) -> Single<ImageInfo>
    func upadte(info: UserInfo) -> Single<Void>
}
