//
//  UseCaseProvider.swift
//  Domain
//
//  Created by Konstantin Kulakov on 04/09/2019.
//  Copyright © 2019 Konstantin Kulakov. All rights reserved.
//

import Foundation

public protocol NetworkingUseCaseProvider {

    func makeAuthorizationUseCase() -> AuthorizationUseCase
    func makeGoalUseCase(token: String?) -> GoalUseCase
    func makeSettingsUseCase(token: String?) -> SettingsUseCase
}
