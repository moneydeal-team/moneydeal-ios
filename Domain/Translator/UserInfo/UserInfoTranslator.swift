//
//  UserInfoTranslator.swift
//  Domain
//
//  Created by Konstantin Kulakov on 15/01/2020.
//  Copyright © 2020 Konstantin Kulakov. All rights reserved.
//

import RealmSwift
import Storage

extension UserInfo: Persistable {

  public typealias ManagedObject = UserInfoObject

    public init(managedObject: UserInfoObject) {
        name = managedObject.name
        lastName = managedObject.lastName
        avatar = managedObject.avatar
    }

    public func managedObject() -> UserInfoObject {
        let managedObject = UserInfoObject()

        managedObject.name = name ?? ""
        managedObject.lastName = lastName ?? ""
        managedObject.avatar = avatar ?? ""

        return managedObject
    }
}
